export default {
  roots: ['<rootDir>/src'],
  testMatch: ['**/__tests__/**/*.+(ts|tsx|js|mjs)', '**/?(*.)+(spec|test).+(ts|tsx|js|mjs)'],
  transform: {
    '^.+\\.(ts|tsx|mjs)$': 'babel-jest',
  },
  testEnvironment: 'node',
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  moduleNameMapper: {
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
  },
};
