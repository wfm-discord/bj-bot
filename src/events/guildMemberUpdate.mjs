const botnoiseId = process.env.BOTNOISE_CHANNEL_ID;
const shameRoleId = process.env.SHAME_ROLE_ID;
const punishMeRoleId = process.env.PUNISH_ME_NOW_ROLE_ID;
const dndPlayerRoleId = process.env.DND_PLAYER_ROLE_ID;
const dndDiscussionChannelId = process.env.DND_DISCUSSION_CHANNEL_ID;

export default (() => {
  return {
    name: 'guildMemberUpdate',
    async execute(oldMember, newMember, bot) {
      // Check if the user's roles were updated
      if (oldMember.roles.cache.size !== newMember.roles.cache.size) {
        // Check if user had the shame role
        const hasShameRole = oldMember.roles.cache.has(shameRoleId);
        // Check if the user no longer has the shame role
        const removeShameRole = hasShameRole && !newMember.roles.cache.has(shameRoleId);

        if (removeShameRole) {
          const shame = await bot.shame.forUser(newMember.id);
          if (shame.isShamed()) {
            await shame.clear("role removed");
          }
        }

        // Check if "punish me now" was turned on
        const hadPunishMeRole = oldMember.roles.cache.has(punishMeRoleId);
        const hasPunishMeRoleNow = newMember.roles.cache.has(punishMeRoleId);

        // if user now has the "punish me" role on, but didn't have it before
        if (!hadPunishMeRole && hasPunishMeRoleNow) {
          const botNoise = await newMember.guild.channels.fetch(botnoiseId);
          let punishMessage = `<@${newMember.id}> has turned on the "punish me now" role `;
          const consent = await bot.consent.forUser(newMember.id);
          if (consent.hasIndividualConsent({allFlag: true})) {
            punishMessage += 'and has consented to all! :partying_face:';
          } else if (consent.hasConsentedToSomeone()) {
            punishMessage += 'but has only consented to limited people.';
          } else {
            punishMessage += 'but has not consented to anyone.\n\n';
            punishMessage += 'If you would like anyone to actually assign you anything you need ' +
              'to consent to people. You can consent to all, or individuals.' +
              '\nSee https://discord.com/channels/928037167677181952/928298290783600690/997994922378534984' +
              '\n\nUse `/consent` to give consent to people to mess with you.';
          }
          botNoise.send(punishMessage);
        }

        // Check if the D&D player role was added
        const hadDndPlayerRole = oldMember.roles.cache.has(dndPlayerRoleId);
        const hasDndPlayerRoleNow = newMember.roles.cache.has(dndPlayerRoleId);

        if (!hadDndPlayerRole && hasDndPlayerRoleNow) {
          const dndDiscussionChannel = await newMember.guild.channels.fetch(dndDiscussionChannelId);
          const dndMessage = `<@${newMember.id}> Welcome to the D&D discussion channel! ` +
            'This is where all the D&D discussions happen for players. \n\n' +
            'If you\'d like to play you need a character. ' +
            'You can talk to people in here to help you build one, or you can message ' +
            '<@417060247186964510> directly. This group is generally pretty happy to help you ' +
            'flesh out ideas, but Sam has the final call as the DM.\n' +
            ' \n' +
            'If you want to be really hands-on and detailed on the character creation, ' +
            'you can, or if you want to just have a basic idea and let Sam fill in the necessary ' +
            'details and hand you a completed character, then that\'s also fine\n' +
            '\n' +
            'We do have a lot of players, but we also have people in-and-out. ' +
            'Generally we don\'t want more than like 7 players in a session, so be aware that if a particular session is too ' +
            'full you may not be able to play.\n\n' +
            '' +
            'Please ask questions here if you have any, and make sure you read <#1207166015683690506>\n\n' +
            '' +
            'If you do not play, you will have your player role removed. ' +
            'You may rejoin as an observer, or as a player when you have time to play.' +
            '';
          dndDiscussionChannel.send(dndMessage);
        }
      }
    },
  };
})();
