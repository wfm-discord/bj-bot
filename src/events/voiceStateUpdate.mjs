const textChannelId = process.env.STREAM_ALERT_CHANNEL;
const alertRoleId = process.env.STREAM_ALERT_ROLE;
const dndVoiceChannelId = process.env.DND_VOICE_CHANNEL_ID;

// a map to store the timestamps of the last stream for each user
const lastStreamTimestamps = new Map();

/**
 * Alerts a role when a user starts streaming.
 * If the user started streaming within the last 5 minutes,
 * it posts a message without tagging the role.
 * Posts a message when the user stops streaming.
 */
const voiceStateUpdate = {
  name: 'voiceStateUpdate',
  async execute(oldState, newState) {
    const userId = newState.member.id;
    const userLastStreamTimestamp = lastStreamTimestamps.get(userId);

    // Check if the user is in the DND (Do Not Disturb) voice channel
    if (newState.channelId === dndVoiceChannelId) {
      return; // Exit the function early if the user is in the DND voice channel
    }

    // check if the user is streaming
    if (newState.channelId // is in a voice channel
      && (newState.streaming || newState.selfVideo) // is streaming something
      && !(oldState.streaming || oldState.selfVideo) // wasn't streaming before
    ) {
      // get the voice channel name
      const voiceChannel = newState.channel.name;
      // get the user's name
      const displayName = newState.member.displayName;
      // send a message to the text channel
      const channel = newState.guild.channels.cache.get(textChannelId);

      // only alert the @role if they haven't streamed in the last 5 minutes
      if (!userLastStreamTimestamp || (Date.now() - userLastStreamTimestamp) >= 5 * 60 * 1000) {
        // get the role to ping
        const role = await newState.guild.roles.fetch(alertRoleId);
        channel.send(`${role}: ${displayName} is now streaming in ${voiceChannel}`);
      } else {
        // they started a stream too recently to bother alerting people
        channel.send(`${displayName} is back on in ${voiceChannel}`);
      }
      // update the user's last stream timestamp
      lastStreamTimestamps.set(userId, Date.now());
    } else if ((oldState.streaming || oldState.selfVideo) // was streaming
      && (!(newState.streaming || newState.selfVideo) || !newState.channelId) // and is no longer streaming
    ) {
      // get the user's name
      const displayName = newState.member.displayName;
      // send a message to the text channel
      const channel = newState.guild.channels.cache.get(textChannelId);
      channel.send(`${displayName} is no longer streaming.`);

      // update the user's last timestamp so if they rejoin it doesn't ping again
      lastStreamTimestamps.set(userId, Date.now());
    }
  },
};

export default voiceStateUpdate;
