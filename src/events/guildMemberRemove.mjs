import {forceLeaveLottery} from '../utils/lottery.mjs';

const guildMemberRemove = {
  name: 'guildMemberRemove',
  async execute(member, bot) {
    // Remove player from the lottery if they had joined
    await forceLeaveLottery(member)

    try {
      // Handle if the user had a lines channel.
      const lines = await bot.lines.forUser(member.id);
      if (lines.hasLines()) {
        await lines.channelWhereAssigned.send(`${lines.username} left the server before they could finish their lines :(`);
        await lines.cancelLines();
      }
    } catch (error) {
      console.log(error)
    }
  }
};

export default guildMemberRemove;
