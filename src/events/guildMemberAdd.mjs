const role1Id = process.env.REQUIRED_ROLE_1;
const role2Id = process.env.REQUIRED_ROLE_2;
const role3Id = process.env.REQUIRED_ROLE_3;
const channelId = process.env.WELCOME_CHANNEL_ID;

const guildMemberAdd = {
  name: 'guildMemberAdd',
  async execute(member, bot) {

    // If you want anything to apply to bots, to it above this line!
    // This prevents this bot from doing the welcome things to bots
    if (member.user.bot) return;

    const welcomeChannel = member.guild.channels.cache.get(channelId);

    // Wait for the member to fully load before proceeding
    await member.fetch();

    let welcome_message = `Welcome to the WFM Community Discord server <@${member.id}>!\n`;
    welcome_message += 'Agree to our rules and choose your role in <#928037167677181955>. ' +
      'Let us know here if you have any questions or need any help! \n' +
      'After you\'ve got your role, please introduce yourself in <#928042507152007208> (SFW). \n' +
      'Please put bot commands in <#980574787187396748>: \n' +
      '* Please tell us your WFM profile using `/set_wfm`. \n' +
      '* Learn more about the server by typing `orientation`.';
    await welcomeChannel.send(welcome_message);

    try {
      // 15 minutes to poke at someone about their role
      const firstAlertTimer = 900000;

      // Schedule a timeout to check if the member has chosen one of the roles within 1 hour
      const timeout = setTimeout(async () => {
        try {

          if (member.roles.cache.has(role1Id) || member.roles.cache.has(role2Id) || member.roles.cache.has(role3Id)) {
            // shrug I don't feel like rewriting the conditional
          } else {
            // Send a reminder message to the server welcome channel
            try {
              await member.guild.members.fetch(member);
              await welcomeChannel.send(`Hi ${member}, we noticed you haven't chosen `
              + 'a role yet from the <#928037167677181955> channel to agree to '
              + 'our rules and join the rest of the server. You need to choose '
              + 'a role to agree to the server rules and to stay on the server. '
              + 'Do you need any help? ');
            } catch (error) {
              if (error.code === 10007) {
                // Member is no longer in the guild, do nothing
                return;
              }
              console.error(`Error while fetching member: ${error.message}`);
            }
          }
        } catch (error) {
          console.error(`Error while sending reminder message: ${error.message}`);
        }
      }, firstAlertTimer);
      // Store the reminder timeout in the member object for later reference
      member.reminderTimeout = timeout;
    } catch (error) {
      console.error(`Error while scheduling timeout: ${error.message}`);
    }

    // regag someone
    const gag = await bot.gag.forUser(member.id);
    // Use the saved gag duration
    let remainingTime = gag.duration;
    if (remainingTime > 0) {
      // Send a message about the remaining gag time
      const channel = member.guild.channels.cache.get(channelId);
      channel.send(`Welcome back to the server ${member}!\n` +
        `When you left you were gagged. You still need to serve ${remainingTime} minutes. ` +
        'Please wait until your gag time is over then choose a ' +
        'role to join the rest of server again.');
      await gag.startGag();
    }
  },
};

export default guildMemberAdd;
