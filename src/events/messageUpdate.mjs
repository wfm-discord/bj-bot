import { brain } from 'bot-commons-utils/src/utils/brain.mjs';
import {
  applyPenaltyAndGetPenaltyReplyFromType,
  buildRegex,
  skipThisWord,
} from '../utils/restricted_words.mjs';

const counting_room = process.env.COUNTING_ROOM;

const messageUpdate = {
  name: 'messageUpdate',
  async execute(oldMessage, newMessage) {
    // Ignore if the message content hasn't changed or the message is from a bot
    if (oldMessage.content === newMessage.content || newMessage.author.bot) return;

    await handleRestrictions(oldMessage, newMessage);
  }
};

async function handleCountWarning(oldMessage, newMessage) {
  if (newMessage.channel.id === counting_room // counting room
    && !newMessage.author.bot // not a bot (USE NEW MESSAGE)
    && oldMessage.content // old message has to have content (not cached?)
  ) {
    if (
      (oldMessage.content.match(/^[\d(]/) && oldMessage.content.match(/\d+/))
      || (newMessage.content.match(/^[\d(]/) && newMessage.content.match(/\d+/))
    ) {
      newMessage.react('✏️');
      newMessage.react('⚠️');
      newMessage.reply(
        'Message edited. The old message was `' +
        oldMessage.content.replace(/`/g, '\\`') +
        '`, changed to `' +
        newMessage.content.replace(/`/g, '\\`') +
        '`'
      );
    }
  }
}

async function handleRestrictions(oldMessage, newMessage) {
  const restrictions = brain.data.restricted_words[newMessage.author.id];
  if (!restrictions) return; // Exit if there are no restrictions for this user


  for (const [word, settings] of Object.entries(restrictions)) {
    if (await skipThisWord(word, newMessage.author, newMessage.channel)) continue;

    // Build the regex
    let regex = buildRegex(word, settings);

    // Check if the old message did not match but the new message does
    if (!regex.test(oldMessage.content) && regex.test(newMessage.content)) {
      // Issue a warning or take action based on the penalty setting
      const result = await applyPenaltyAndGetPenaltyReplyFromType(word, settings, newMessage.author, newMessage.channel, newMessage.guild);
      await newMessage.reply({
        content: result.reply,
        embeds: result.embeds
      });

      await brain.write();
      break; // Break after applying the first matching penalty
    }
  }
}

export default messageUpdate;
