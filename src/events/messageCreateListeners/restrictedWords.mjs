// src/events/messageCreateListeners/restrictedWords.mjs
import {brain} from 'bot-commons-utils/src/utils/brain.mjs';
import {
  applyPenaltyAndGetPenaltyReplyFromType,
  buildRegex,
  skipThisWord
} from '../../utils/restricted_words.mjs';

const dndVoiceChannelId = process.env.DND_VOICE_CHANNEL_ID;


export const restrictedWords = {
  name: 'restrictedWords',
  async execute(message) {
    if (
      message.author.bot || // sent by a bot
      message.channel.id === dndVoiceChannelId
    ) {
      return;
    }

    // Check if the user has a restricted word list
    const restrictions = brain.data.restricted_words[message.author.id];
    if (!restrictions) return; // Exit if there are no restrictions for this user

    // Check each restricted word against the message
    for (const [word, settings] of Object.entries(restrictions)) {
      if (await skipThisWord(word, message.author, message.channel)) continue;

      // Build the regex based on settings
      let regex = buildRegex(word, settings);

      // Check for a match
      if (regex.test(message.content)) {
        // Issue a warning or take action based on the penalty setting
        const result = await applyPenaltyAndGetPenaltyReplyFromType(word, settings, message.author, message.channel, message.guild);
        await message.reply({
          content: result.reply,
          embeds: result.embeds
        });
        await brain.write();

        // Stop checking after the first match to avoid multiple penalties for one message
        break;
      }
    }
  }
}
