import {timeUtils} from 'bot-commons-utils/src/utils/time.mjs';
import {bot} from '../../services/bot.mjs';

export const handleMathProblems = {
  name: 'handleMathProblems',
  async execute(message) {
    if (message.author.bot) {
      //ignore bot messages
      return;
    }
    const mathObject = await bot.math.forUser(message.author.id);

    // Make sure the user has problems assigned and this is the right channel
    if (mathObject.hasProblems() && mathObject.inAssignmentChannel(message.channel.id) && !mathObject.isComplete()) {
      // Before anything else, check for cancellation
      if (message.content.toLowerCase() === 'cancel') {
        // Cancel command given, cancel this session
        message.reply('Problems cancelled. This channel will be deleted in 10 seconds!');
        await mathObject.channelWhereAssigned.send(`<@${message.author.id}> cancelled their math.`);
        await mathObject.sendCancellationMessage;
        await mathObject.cancelMath();
        return;
      }

      // Before much else, is content 'start'?
      if (message.content.toLowerCase() === 'start' && !mathObject.isStarted()) {
        // Start the assignment
        await mathObject.startMathAssignment();
        // Give the first problem
        const newProblem = await mathObject.getNewProblem();
        // Reply
        message.reply(`**#${mathObject.problemNumber}.**\n  ${newProblem.problem}`);
        if (mathObject.shouldShowDivisionMessage()) {
          message.channel.send(mathObject.divisionMessage);
        }
        return;
      }

      // If not started, and content isn't "start" - bother them to start.
      if (!mathObject.isStarted()) {
        message.reply('When you\'re ready you can turn your test over by sending \n> start');
        return;
      }


      // Always save last time something was entered
      mathObject.updateTimestamp(); //TODO: maybe just handle this in all the functions in the object, not here

      // Check to see if the reply is the answer to the problem
      const userAnswerStr = message.content.replace(/,/g, ''); // Remove any commas
      const userAnswer = parseFloat(userAnswerStr);

      const correctAnswer = mathObject.currentAnswer;
      const isAnswerCorrect = userAnswer === correctAnswer;
      if (isAnswerCorrect && !mathObject.isTest()) {
        await message.react('✅');
      }

      // Update all the things
      await mathObject.handleAnswer(userAnswerStr);

      // Check if they are done
      if (mathObject.isComplete()) {
        await mathObject.handleCompletion();
        message.reply('Congratulations! You have completed your math assignment after ' +
          `${timeUtils.msToTime(mathObject.timeTaken)}! This channel will be deleted in 10 seconds.`);
        await mathObject.sendCompletionMessage();
        setTimeout(function() {
          mathObject.cancelMathNow();
        }, 10000);
        return;
      }

      // Not done
      if (isAnswerCorrect || mathObject.isTest()) {
        // Give a new problem if the answer was correct OR we're in test mode
        const newProblem = await mathObject.getNewProblem();
        // Reply
        message.reply(`**#${mathObject.problemNumber}.**\n  ${newProblem.problem}`);
        const tracking = await bot.stats.mathForUser(message.author.id);
        await tracking.startProblem(newProblem.operation, mathObject.testMode);
        // Give the first problem
        if (mathObject.shouldShowDivisionMessage()) {
          message.channel.send(mathObject.divisionMessage);
        }
      } else {
        message.react('❌');
        // If it's not, tell the user it's wrong and to keep trying
        message.reply('That is not the correct answer. Please try again.');
      }
    }
  },
}
