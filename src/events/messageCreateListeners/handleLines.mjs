import {timeUtils} from 'bot-commons-utils/src/utils/time.mjs';
import Diff from 'text-diff';
import {bot} from "../../services/bot.mjs";

export const handleLines = {
  name: 'handleLines',
  async execute(message) {
    if (message.author.bot) {
      //ignore bot messages
      return;
    }
    const lines = await bot.lines.forUser(message.author.id);

    // Check if the author of the message has any "lines" data stored in the brain
    // and if the message was sent in the same channel as the lines were started in
    if (lines.hasLines() && lines.inAssignmentChannel(message.channel.id)) {
      // before anything else, check for cancelled lines
      if (message.content.toLowerCase() === 'cancel') {
        // Cancel command given, cancel this session
        message.reply('Lines cancelled. This channel will be deleted in 10 seconds!');
        await lines.channelWhereAssigned.send(`<@${message.author.id}> cancelled their lines.`);
        await lines.cancelLines();
        return;
      }

      // Before much else, is content 'start'?
      if (message.content.toLowerCase() === 'start' && !lines.hasStarted()) {
        await lines.startLines();
        message.reply(await nextLine(lines));
        return;
      } else if (!lines.hasStarted()) {
        message.reply('Did you read the directions?');
        return;
      }

      // Check for copy/pasting cheating
      if (lines.isCopyPasting(message.content)) {
        let alert = `SHAME on <@${message.author.id}> for trying to copy/paste their lines`;
        alert += lines.setByUserId !== message.author.id ? ` while doing lines for <@${lines.setByUserId}>!` : '!!';
        message.reply('CHEATER! Shame on you!\nYou were caught copy/pasting your lines!!!');
        await lines.handleCheating(alert);
        message.react('❌');
        return;
      }

      // Check for typing unusually fast
      if (lines.isTypingUnusuallyFast(message.content)) {
        let alert = `SHAME on <@${message.author.id}> for typing impossibly fast`;
        alert += lines.setByUserId !== message.author.id ? ` while doing lines for <@${lines.setByUserId}>!` : '!!';
        message.reply('CHEATER! Shame on you!\\nYou were caught typing impossibly fast!!!');
        await lines.handleCheating(alert);
        message.react('❌');
        return;
      }

      // Check if the line was typed correctly
      if (lines.isTypedCorrectly(message.content)) {
        // Line typed is correct
        message.react('✅');

        // Increment the number of lines completed by the user and save to the brain
        await lines.completeOneLine();

        // Check to see if user is finished with the lines
        if (!lines.hasLinesToStillDo()) {
          // Calculate how long it took for the lines to be completed
          let msTaken = Date.now() - lines.startTime;
          // The user has successfully completed their lines

          message.reply('Congratulations! You have successfully completed your lines after ' +
            timeUtils.msToTime(msTaken) +
            '! This channel will be deleted in 10 seconds.');

          let reply_msg = '';
          if (lines.setByUserId !== message.author.id) {
            reply_msg += `<@${message.author.id}> completed their lines for <@${lines.setByUserId}>. `
          } else {
            reply_msg += `<@${message.author.id}> completed their lines.`;
          }

          reply_msg += ` They made **${lines.totalMistakes}** ${
            lines.totalMistakes === 1 ? 'mistake' : 'mistakes'
          } and it took them **${
            timeUtils.msToTime(msTaken)
          }** ${lines.qtyToDo > 1 ? `with a typing speed of **${lines.finalWPM} WPM!**` : '.'}`;
          if (lines.hasCheated()) {
            reply_msg += '\n\n**However, they also cheated during this session!**'
          }

          try {
            const chan = await lines.channelWhereAssigned;
            await chan.send(reply_msg);
          } catch (error) {
            let botNoiseChannel = message.client.channels.cache.get(process.env.BOTNOISE_CHANNEL_ID);
            botNoiseChannel.send("An error occurred while sending a message to the intended channel. Here's the message content:\n\n" + reply_msg);
          }

          await lines.completeLines();
          return;
        }

        // Lines aren't finished
        message.reply(await nextLine(lines));
      } else {
        // Line was incorrect
        message.react('❌');
        // set all the data for mistakes
        await lines.madeMistakeOnLine();
        // We want both random and buggy to trigger this.
        if (lines.isRandomCase() || lines.isRandomCaseBuggy()) {
          message.reply(`Don't mess up. Now the line is \n**${lines.displayLine}**`);
        }
        if (lines.mistakesOnLine > 2 && lines.isNotRandomCase()) {
          let hint = lines.antiCheatFilter(generateHint(lines.line, message.content));
          await message.channel.send(hint);
        }
      }
    }
  }
}

async function nextLine(lines) {
  await lines.setUpNextLine();
  let reply_line = `Your new line is: \n **${lines.displayLine}**`;
  if (!lines.hideNumberCompleted()) {
    return `**Line ${(lines.linesCompleted + 1)} / ${lines.qtyToDo}**\n` + reply_line;
  } else {
    return reply_line;
  }
}

function generateHint(expectedLine, givenLine) {
  if (expectedLine === null ||
    givenLine === null ||
    expectedLine === '' ||
    givenLine === '' ||
    expectedLine === undefined ||
    givenLine === undefined
  ) {
    throw new Error('Expected line and given line must not be empty.');
  }

  let hint = 'Hint: ';
  // Find differences in the lines
  let diff = new Diff();
  let textDiff = diff.main(givenLine, expectedLine);
  for (let i = 0; i < textDiff.length; i++) {
    let op = textDiff[i][0]; // Operation: -1 = delete, 1 = add, 0 = no change
    let data = textDiff[i][1];
    switch (op) {
      // Nothing needs to change in this section
      case 0: {
        hint += data;
        break;
      }
      // Extra character / space
      case -1: {
        hint += '~~**' + data + '**~~';
        break;
      }
      // Character or space missing.
      case 1: {
        const newdata = replaceAll(data, ' ', '_');
        hint += '**' + newdata + '**';
        break;
      }
      default:
        break;
    }
  }
  return hint;
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
