const category = process.env.BOOTH_CATEGORY_ID;
const skip = process.env.BOOTH_SKIP_CHANNEL_ID;

/**
 * Bumps booth channels to the top of the list when they have activity
 */
export const boothBump = {
  name: 'boothBump',
  async execute(message) {
    if (
      message.channel.parentId === category && // booth category
      message.channel.id !== skip && // not info channel
      !message.author.bot // not sent by a bot
    ) {
      // bump this channel to the top category
      message.channel.setPosition(0);
    }
  }
}
