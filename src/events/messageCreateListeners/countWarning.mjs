// Description:
//    Warn users about bad counting

// limit the evaluation per mathjs docs
// https://mathjs.org/examples/advanced/more_secure_eval.js.html
import { create, all } from 'mathjs';
const math = create(all);

const limitedEvaluate = math.evaluate;

// Limit what the math evaluate can do
math.import({
  import: function () { throw new Error('Function import is disabled'); },
  createUnit: function () { throw new Error('Function createUnit is disabled'); },
  evaluate: function () { throw new Error('Function evaluate is disabled'); },
  parse: function () { throw new Error('Function parse is disabled'); },
  simplify: function () { throw new Error('Function simplify is disabled'); },
  derivative: function () { throw new Error('Function derivative is disabled'); }
}, { override: true });

// Regex -
// ^ Match on the start of a message
// \d+[\#\d]*\.?[\#\d]*
//    any number of digits,
//    # or a digit, 0 or more times
//    ., 0 or one time
//    # or a digit, 0 or more times
//    3#####.####### is valid
//    3#####.####4## is valid
//    3#####.####4##5 is valid
// (\s+|$) Either zero or more space, or end of line

const counting_room = process.env.COUNTING_ROOM;

export const courtWarning = {
  name: 'courtWarning',
  async execute(message) {
    // only in counting room
    if (message.channel.id === counting_room // counting room
      && !message.author.bot // not a bot
      // This seems to be the regex for counting bot
      && !message.content.match(/^\d+[#\d]*\.?[#\d]*(\s+|$)/) // Digits and either EOL or space
    ) {
      // This isn't a straight count message
      // But apparently, things like 1+1 count

      // First, does it start with either a ( or a digit?
      if (message.content.match(/^[\d(]/) && message.content.match(/\d+/)) {
        try {
          // Try evaluating the string as-is
          let to_evaluate = message.content.match(/^.*(\s|$)/);
          let e = limitedEvaluate(to_evaluate[0])
          if (!e) {
            // Still didn't evaluate
            await reactAppropriately(message, '🤔', 'I couldn\'t react! I couldn\'t evaluate the string.');
            await reactAppropriately(message, '❓');
          } else {
            await reactAppropriately(message, '#️⃣', "I couldn't react! String mathmatically evaluated.");
          }
        } catch {
          // Did not evaluate as is.
          // Is there a space?
          if (message.content.match(/\s/)) {
            let first_part = message.content.match(/.*\s/);
            try {
              let e = limitedEvaluate(first_part[0]);
              if (!e) {
                // Still didn't evaluate
                await reactAppropriately(message, '🤔', 'I couldn\'t react! I couldn\'t evaluate the string.');
                await reactAppropriately(message, '❓');
              } else {
                await reactAppropriately(message, '#️⃣', "I couldn't react! String mathmatically evaluated.");
              }
            } catch {
              // Still didn't evaluate
              await reactAppropriately(message, '🤔', 'I couldn\'t react! I couldn\'t evaluate the string.');
              await reactAppropriately(message, '❓');
            }
          } else {
            await reactAppropriately(message, '❌', 'I couldn\'t react! Warning! Be careful about this message!');
            await reactAppropriately(message, '⚠️');
          }
        }
      } else {
        // Doesn't start with ( or a digit or it has no digits
        await reactAppropriately(message, '❌', 'I couldn\'t react! Warning! Be careful about this message!');
        await reactAppropriately(message, '⚠️');
      }
    } else if (message.channel.id === counting_room // counting room
      && !message.author.bot // not a bot
      && message.reference
    ) {
      // This is a reply
      await reactAppropriately(message, '⭕', 'I couldn\'t react! Warning! This was a reply and not considered by the bot.');
      await reactAppropriately(message, '⚠️');
    }
  }
}


async function reactAppropriately(message, react, message_on_err) {
  try {
    await message.react(react)
  } catch (error) {
    if (message_on_err) {
      message.reply(message_on_err);
    }
    console.log(error);
  }
}
