// src/events/messageCreateListeners/problematicIntroductions.mjs

const bannedTerms = [
  'scat'
];

const suspectWords = [
  'kink',
  'limit',
  'kinks',
  'limits',
  'orgasm',
];

export const problematicIntroductions = {
  name: 'problematicIntroductions',
  async execute(message) {
    // This needs to be the introductions channel
    if (message.channel.id !== process.env.INTRODUCTIONS_CHANNEL_ID) {
      // Not introductions channel
      return;
    }

    const messageContent = message.content.toLowerCase();

    // Check if regex "DMs?\s?(are)?\s?open" matches
    const dmRegex = /dms?\s?(are)?\s?open/i;
    const hasDMOpen = dmRegex.test(messageContent);

    // Check if message includes one of the banned terms (whole words only)
    const hasBannedTerm = bannedTerms.some(term => {
      const termRegex = new RegExp(`\\b${term}\\b`, 'gi');
      return termRegex.test(messageContent);
    });

    // Check if message includes two or more suspect words (whole words)
    const wordCount = suspectWords.reduce((count, word) => {
      const wordRegex = new RegExp(`\\b${word}\\b`, 'gi');
      return count + (messageContent.match(wordRegex) || []).length;
    }, 0);
    const hasMultipleSuspectWords = wordCount >= 2;

    if (hasDMOpen || hasBannedTerm || hasMultipleSuspectWords) {
      // Save the message text to a constant
      const originalMessage = message.content;

      // DM the user
      // First Message: The Informational Part
      const templatePart1 = `Hello. 
I'm removing your introduction from the WFM server because it triggered some of my filters trying to stop people from posting personal ads. 

This is a discussion server. While it's possible for you to develop a relationship with someone on this server, it isn't the goal of the server. We'd like to get to know you as a person.

If you don't want to socialize or at least lurk on the conversations, and all you're interested in is finding a dom/domme or play partner of some sort (or just want everyone to "punish" you without other social interactions) then this probably isn't the server for you. However if you'd like to talk with other kinksters, or play with our many bot toys to torment yourself or others (usually part of the social experience), this is a very fun server. 

We're a friendly bunch and this is a safe space. 

You used the following words that signal that this may not be appropriate for your introduction: 
${hasDMOpen ? '\n* "DMs open" (mostly used by scammers)' : ''} 
${hasBannedTerm ? `\n* banned term(s): ${bannedTerms.filter(term => new RegExp(`\\b${term}\\b`, 'gi').test(messageContent)).join(', ')}` : ''} 
${hasMultipleSuspectWords ? `\n* suspect word(s): ${suspectWords.filter(word => new RegExp(`\\b${word}\\b`, 'gi').test(messageContent)).join(', ')}` : ''}

**Please note that I am a bot and cannot understand any reply.**
If you need anything, please ping @Moderator or @Admin on the server.`;

      // Second Message: Original Introduction with Quotation
      const templatePart2 = `If you'd like to edit it, here's your original introduction:
\`\`\`
${originalMessage}
\`\`\``;

      // Send DM to the user
      try {
        await message.author.send(templatePart1);
        await message.author.send(templatePart2);
      } catch (error) {
        console.error(`Could not send DM to ${message.author.tag}:`, error);
      }

      // Notify the moderation room
      const modMessagePart1 = `<@${message.author.id}> had their introduction removed for the following reason(s): 
${hasDMOpen ? '\n* "DMs open"' : ''} 
${hasBannedTerm ? `\n* Banned term(s): ${bannedTerms.filter(term => new RegExp(`\\b${term}\\b`, 'gi').test(messageContent)).join(', ')}` : ''} 
${hasMultipleSuspectWords ? `\n* Suspect word(s): ${suspectWords.filter(word => new RegExp(`\\b${word}\\b`, 'gi').test(messageContent)).join(', ')}` : ''}`;

      const modMessagePart2 = `Here is the original introduction:\n\`\`\`\n${originalMessage}\n\`\`\``;

      try {
        const moderationChannel = await message.client.channels.fetch(process.env.MODERATION_ROOM_ID);
        if (moderationChannel) {
          await moderationChannel.send(modMessagePart1);
          await moderationChannel.send(modMessagePart2);
        } else {
          console.error('Could not find the moderation channel.');
        }
      } catch (error) {
        console.error('Failed to send message to moderation room:', error);
      }

      // Delete the message from the introductions channel
      try {
        await message.delete();
      } catch (error) {
        console.error(`Could not delete message from ${message.channel.id}:`, error);
      }
    }
  }
};
