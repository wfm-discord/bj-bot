import {handleCommandInteraction} from 'bot-commons-utils/src/utils/interactionCreateUtil.mjs';

const interactionCreate = {
  name: 'interactionCreate',
  async execute(interaction, bot) {
    // console.log(`${interaction.user.tag} in #${interaction.channel.name} triggered an interaction.`);
    await interact(interaction, bot);
  },
};

async function interact(interaction, bot) {
  // Handle command interactions
  await handleCommandInteraction(interaction, bot);

  // Handle button and select menu interactions
  if (interaction.isMessageComponent()) {
    if (interaction.isButton()) {
      const parts = interaction.customId.split('_'); // Split customId into parts
      const action = parts[0]; // 'next' or 'previous'
      const page = parseInt(parts[1]); // page number
      const userId = parts[2]; // user ID

      const command = interaction.client.commands.get(interaction.message.interaction.commandName);
      if (!command || !command.buttons || !command.buttons[action]) return;

      try {
        // Call the handler and pass the page number as a parameter
        await command.buttons[action](interaction, page, userId);
      } catch (error) {
        console.error(error);
        await interaction.reply({ content: 'There was an error while executing this button interaction!', ephemeral: true });
      }
    }
    else if (interaction.isStringSelectMenu()) {
      const command = interaction.client.commands.get(interaction.message.interaction.commandName);
      if (!command || !command.handleSelectMenu) return;

      try {
        await command.handleSelectMenu(interaction);
      } catch (error) {
        console.error(error);
        await interaction.reply({ content: 'There was an error while handling the select menu!', ephemeral: true });
      }
    }
  }
}

export default {
  name: 'interactionCreate',
  execute: interact
};
