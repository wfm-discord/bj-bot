import { SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('time')
  .setDescription('Time utilities')
  .addSubcommand(subcommand =>
    subcommand
      .setName('convert')
      .setDescription('Convert a UTC time to a universal time format')
      .addStringOption(option =>
        option.setName('datetime')
          .setDescription('Enter the date and time in UTC (YYYY-MM-DD HH:MM format)')
          .setRequired(true)))
  .addSubcommand(subcommand =>
    subcommand
      .setName('current')
      .setDescription('Get the current time in UTC'))
  .addSubcommand(subcommand =>
    subcommand
      .setName('future-time')
      .setDescription('Display time in X days/hours/minutes in a universal time format')
      .addIntegerOption(option =>
        option.setName('days')
          .setDescription('Number of days')
          .setRequired(false))
      .addIntegerOption(option =>
        option.setName('hours')
          .setDescription('Number of hours')
          .setRequired(false))
      .addIntegerOption(option =>
        option.setName('minutes')
          .setDescription('Number of minutes')
          .setRequired(false)));

export async function execute(interaction) {
  const subCommand = interaction.options.getSubcommand();

  switch (subCommand) {
    case 'convert':
      const userInput = interaction.options.getString('datetime');
      const userDate = new Date(userInput + 'Z');

      if (isNaN(userDate.getTime())) {
        await interaction.reply({ content: 'Invalid date format. Please use YYYY-MM-DD HH:MM format.', ephemeral: true });
        return;
      }

      const timestamp = Math.floor(userDate.getTime() / 1000);
      await interaction.reply({ content: `<t:${timestamp}>`, ephemeral: false });
      break;

    case 'current':
      const now = new Date();
      const year = now.getUTCFullYear();
      const month = now.getUTCMonth() + 1; // getUTCMonth() returns months from 0-11
      const day = now.getUTCDate();
      const utcHours = now.getUTCHours();
      const utcMinutes = now.getUTCMinutes();
      const seconds = now.getUTCSeconds();

      // Pad single digits with leading zeros for a uniform format
      const formattedDate = `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`;
      const formattedTime = `${utcHours.toString().padStart(2, '0')}:${utcMinutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')} UTC`;

      await interaction.reply({ content: `Current UTC time: ${formattedDate} ${formattedTime}`, ephemeral: false });
      break;


    case 'future-time':
      const days = interaction.options.getInteger('days') || 0;
      const hours = interaction.options.getInteger('hours') || 0;
      const minutes = interaction.options.getInteger('minutes') || 0;
      const futureTime = new Date();
      futureTime.setUTCDate(futureTime.getUTCDate() + days);
      futureTime.setUTCHours(futureTime.getUTCHours() + hours);
      futureTime.setUTCMinutes(futureTime.getUTCMinutes() + minutes);
      await interaction.reply({ content: `Time in ${days} days, ${hours} hours, and ${minutes} minutes from now: <t:${Math.floor(futureTime.getTime() / 1000)}:F>`, ephemeral: false });
      break;


    default:
      await interaction.reply({ content: 'Invalid command used.', ephemeral: true });
  }
}
