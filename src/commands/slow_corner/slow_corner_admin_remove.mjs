import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import {ensureBrainKeyExists, brain} from 'bot-commons-utils/src/utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('slow_corner_admin_remove')
  .setDescription('Remove slowdowns from a user.')
  .addUserOption(option =>
    option.setName('sub_user')
      .setDescription('The user the slowdowns were set on')
      .setRequired(true))
  .addUserOption(option =>
    option.setName('setter_user')
      .setDescription('The user who set the slowdowns (optional)')
      .setRequired(false))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  // Get users
  let subUser = interaction.options.getUser('sub_user');
  let setterUser = interaction.options.getUser('setter_user');

  // Check if slowdowns exist for the user
  if (ensureBrainKeyExists('slowdown', subUser.id)) {
    if (setterUser) {
      // Remove only the slowdowns set by a specific user
      if (brain.data.slowdown[subUser.id][setterUser.id]) {
        delete brain.data.slowdown[subUser.id][setterUser.id];
        await interaction.reply({ content: `Slowdowns set by ${setterUser.username} on ${subUser.username} have been removed.`, ephemeral: true });
      } else {
        await interaction.reply({ content: `${setterUser.username} did not set any slowdowns on ${subUser.username}.`, ephemeral: true });
      }
    } else {
      // Remove all slowdowns
      delete brain.data.slowdown[subUser.id];
      await interaction.reply({ content: `All slowdowns added on ${subUser.username} have been removed.`, ephemeral: true });
    }
    await brain.write();
  } else {
    await interaction.reply({ content: `${subUser.username} doesn't have any slowdowns set.`, ephemeral: true });
  }
}
