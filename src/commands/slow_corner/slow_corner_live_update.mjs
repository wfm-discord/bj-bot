import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';

const shame_room = process.env.SHAME_ROOM;
const bot_noise_room_id = process.env.BOTNOISE_CHANNEL_ID;

export const data = new SlashCommandBuilder()
  .setName('slow_corner_live_update')
  .setDescription('Update the corner live while someone is typing in it!')
  .addSubcommand(subcommand =>
    subcommand
      .setName('direct')
      .setDescription('Directly set the slowdown time')
      .addUserOption(option =>
        option.setName('user')
          .setDescription('The user to set slowdown')
          .setRequired(true))
      .addIntegerOption(option =>
        option.setName('seconds')
          .setDescription('The number of seconds to slow down')
          .setRequired(true)))
  .addSubcommand(subcommand =>
    subcommand
      .setName('random')
      .setDescription('Randomly set the slowdown time between the limits')
      .addUserOption(option =>
        option.setName('user')
          .setDescription('The user to set slowdown')
          .setRequired(true))
      .addIntegerOption(option =>
        option.setName('lower_limit')
          .setDescription('The lower limit of the random slowdown time')
          .setRequired(false))
      .addIntegerOption(option =>
        option.setName('upper_limit')
          .setDescription('The upper limit of the random slowdown time')
          .setRequired(false)));

export async function execute(interaction, bot) {
  // Get commandCaller and client
  let commandCaller = interaction.user;
  let client = interaction.client;
  let shameRoomChannel = client.channels.cache.get(shame_room);

  // Get subcommand name
  const subcommand = interaction.options.getSubcommand();

  // Get user and calculate seconds based on the subcommand
  let user = interaction.options.getUser('user');
  let seconds;
  if (subcommand === 'direct') {
    seconds = interaction.options.getInteger('seconds');
  } else if (subcommand === 'random') {
    let lower_limit = interaction.options.getInteger('lower_limit') || 0;
    let upper_limit = interaction.options.getInteger('upper_limit') || 20;

    // Swap if lower_limit is greater than upper_limit
    if (lower_limit > upper_limit) {
      [lower_limit, upper_limit] = [upper_limit, lower_limit];
    }

    seconds = Math.floor(Math.random() * (upper_limit - lower_limit + 1)) + lower_limit;
  }

  // Check consent before continuing
  if (!await topHasConsent(bot, commandCaller, user)) {
    await interaction.reply({
      content: 'You do not have consent to rate limit that user.',
      ephemeral: true
    });
    return;
  }

  const shame = await bot.shame.forUser(user.id);
  if (!shame.isCounting()) {
    await interaction.reply({
      content: `The user ${user.username} is not currently under punishment and thus the slow mode cannot be updated.`,
      ephemeral: true
    });
    return;
  }

  const callerShame = await bot.shame.forUser(commandCaller.id);
  if (user === commandCaller || callerShame.isCounting()) {
    let botNoiseChannel = client.channels.cache.get(bot_noise_room_id);
    // user tried to change their own rate limit
    let slowdown = shameRoomChannel.rateLimitPerUser;
    if (seconds === 0) {
      seconds = 5;
    }
    const new_slowdown = slowdown + seconds;

    await shameRoomChannel.setRateLimitPerUser(
      new_slowdown,
      `Slow down for ${commandCaller.username} based on the command by ${commandCaller.username}.`
    );

    callerShame.countingTarget += 50;
    await callerShame.save();

    await shameRoomChannel.send(
      `Rate limit change! ${commandCaller} attempted to alter their slowdown! ` +
      `Instead, I've added ${seconds} to their original slowdown of ${slowdown} ` +
      `and increased the counting goal to ${callerShame.countingTarget}!` +
      `\nThe room is now set to a ${new_slowdown} second slow mode.`
    );

    await botNoiseChannel.send(
      `${commandCaller} attempted to alter their slowdown! Their counting target is ` +
      `now ${callerShame.countingTarget} with a new slowdown of ${new_slowdown} seconds!`
    );
    await interaction.reply({
      content: 'Shame on you for trying to change your slowdown.',
      ephemeral: true,
    });
    return;
  }

  // Immediately set the slow mode to the seconds provided in shame room channel
  await shameRoomChannel.setRateLimitPerUser(
    seconds,
    `Slow down for ${user.username} based on the command by ${commandCaller.username}.`
  );

  // Different messages for rate limit 0 and other values
  if (seconds === 0) {
    await shameRoomChannel.send(
        `Rate limit change! ${user} has been relieved ${subcommand}ly from ` +
        `slow mode by ${commandCaller}. The room is back to normal speed.`
    );
    await interaction.reply({
      content: `The corner of shame for ${user} is now without a rate limit.`,
      ephemeral: true,
    });
  } else {
    await shameRoomChannel.send(
      `Rate limit change! ${user} has been slowed down ${subcommand}ly ` +
      `by ${commandCaller}! The room is now set to a ${seconds} second slow mode.`
    );
    await interaction.reply({
      content: `The corner of shame for ${user} is now rate limited by ${seconds} seconds.`,
      ephemeral: true,
    });
  }
}
