import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import {MessageStacker} from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';
import {ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';


export const data = new SlashCommandBuilder()
  .setName('slow_corner_admin_audit')
  .setDescription('Audit all slowdowns.')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const list = ensureBrainKeyExists('slowdown');
  const stacker = new MessageStacker(interaction, '', true);
  for (let slowed_id in list) {
    for (let slow_setter_id in list[slowed_id]) {
      await stacker.appendOrSend(`<@${slowed_id}>: **${list[slowed_id][slow_setter_id].seconds}s** Set by <@${slow_setter_id}>.\n`);
    }
  }
  await stacker.finalize();
  await interaction.followUp({ content: 'Slowdown audit completed.', ephemeral: true });
}
