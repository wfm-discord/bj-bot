import { SlashCommandBuilder } from 'discord.js';
import {ensureBrainKeyExistsPromise, brain} from 'bot-commons-utils/src/utils/brain.mjs';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';

export const data = new SlashCommandBuilder()
  .setName('slow_corner')
  .setDescription('Set a slowdown for a user for when they count in the corner.')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user to set slowdown')
      .setRequired(true))
  .addIntegerOption(option =>
    option.setName('seconds')
      .setDescription('The number of seconds to slow down')
      .setRequired(true));

export async function execute(interaction) {
  // Get user and seconds
  let user = interaction.options.getUser('user');
  let seconds = interaction.options.getInteger('seconds');

  await ensureBrainKeyExistsPromise('slowdown', user.id, interaction.user.id);

  // always clear, regardless of consent
  if (seconds === 0) {
    delete brain.data.slowdown[user.id][interaction.user.id];
    await brain.write();
    await interaction.reply(`Your slowdown for ${user} has been cleared.`);
    return;
  }

  // check consent before continuing
  if (!await topHasConsent(bot, interaction.user, user)) {
    await interaction.reply({
      content: 'You do not have consent to rate limit that user.',
      ephemeral: true
    });
    return;
  }

  await ensureBrainKeyExistsPromise('slowdown', user.id, interaction.user.id);
  brain.data.slowdown[user.id][interaction.user.id] = {
    'set_by': interaction.user.id,
    'seconds': seconds,
    'self': (user.id === interaction.user.id)
  };
  await brain.write();
  await interaction.reply(`A slowdown has been added to ${user} of ${seconds} seconds.`);
}
