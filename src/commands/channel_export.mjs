// src/commands/channel_export.mjs
import { SlashCommandBuilder, PermissionFlagsBits, AttachmentBuilder } from 'discord.js';
import { splitMessagesByTokenLimit, splitMessagesBySize } from '../utils/messageExports.mjs';
import { writeFile } from 'node:fs/promises';
import { withFile } from 'tmp-promise';
import { gatherMessages } from 'bot-commons-utils/src/utils/messageExports.mjs';

export const data = new SlashCommandBuilder()
  .setName('channel_export')
  .setDescription('Export messages in the current channel as text files.')
  .addStringOption(option => option.setName('split_by')
    .setDescription('Choose how to split the messages (tokens/size). Defaults to size.')
    .setRequired(false)
    .addChoices(
      { name: 'size (default)', value: 'size' },
      { name: 'tokens', value: 'tokens' },
    )
  )
  .addChannelOption(option => option.setName('reply_channel')
    .setDescription('Place to reply. By default it will reply here.')
    .setRequired(false)
  )
  .addBooleanOption(option => option.setName('ephemeral_reply')
    .setDescription('Choose if the reply should be ephemeral. Defaults to true.')
    .setRequired(false)
  )
  .addStringOption(option => option.setName('since_date')
    .setDescription('Export messages since the specified date. Format: YYYY-MM-DD')
    .setRequired(false)
  )
  .addIntegerOption(option => option.setName('split_limit')
    .setDescription('The limit for splitting the messages. Defaults to 2000 tokens or 20,480 KB.')
    .setRequired(false)
  )
  .addBooleanOption(option => option.setName('include_dates')
    .setDescription('Choose if the messages should include dates. Defaults to true.')
    .setRequired(false)
  )
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  try {
    const channel = interaction.channel;
    const replyChannel = interaction.options.getChannel('reply_channel') || channel;
    let ephemeralReply = true;
    if (!interaction.options.getChannel('reply_channel')) {
      ephemeralReply = interaction.options.getBoolean('ephemeral_reply') ?? true;
    }
    await interaction.deferReply({ ephemeral: ephemeralReply });
    const splitBy = interaction.options.getString('split_by') || 'size';
    const exportDate = interaction.options.getString('since_date');
    const includeDates = interaction.options.getBoolean('include_dates') ?? true;


    await interaction.editReply({ content: 'Please hold on. I am working on getting the messages together...'});
    const startDate = exportDate ? new Date(exportDate) : undefined;
    const messages = await gatherMessages(channel, startDate);

    if (messages.length === 0) {
      await interaction.editReply({ content: 'There were no messages.' });
      return;
    }

    await interaction.editReply({ content: 'I have the messages I just need to form them into text files for you.'});

    let splitMessageText = [];
    const splitLimit = interaction.options.getInteger('split_limit');

    if (splitBy === 'tokens') {
      let tokenLimit = splitLimit; // User-provided token limit
      if (tokenLimit > (4096)) {
        // GPT-4 token limit is 4096
        tokenLimit = 4096;
      } else if (!tokenLimit) {
        tokenLimit = 2000;
      }
      splitMessageText = splitMessagesByTokenLimit(messages, tokenLimit, false, includeDates);
    } else {
      let sizeLimit = splitLimit * 1024; // Convert KB to bytes for the size limit
      if (!sizeLimit || sizeLimit > 20 * 1024 * 1024) {
        sizeLimit = 20 * 1024 * 1024; //20MB
      }
      splitMessageText = await splitMessagesBySize(messages, sizeLimit, false, includeDates);
    }

    for (let i = 0; i < splitMessageText.length; i++) {
      await withFile(async ({ path }) => {
        const filePath = path;
        await writeFile(filePath, splitMessageText[i]);
        const fileAttachment = new AttachmentBuilder(filePath);

        const replyContent = `Here is the messages export file (${i + 1}/${splitMessageText.length}) for #${channel.name}:`;

        if (replyChannel.id !== channel.id) {
          if (i === 0) {
            await replyChannel.send({
              content: replyContent,
              files: [fileAttachment]
            });
            await interaction.editReply({
              content: `The messages have been exported and sent to ${replyChannel}. There are ${splitMessageText.length} files waiting to be downloaded.`,
            });
          } else {
            await replyChannel.send({
              content: replyContent,
              files: [fileAttachment]
            });
          }
        } else {
          if (i === 0) {
            await interaction.editReply({
              content: replyContent,
              files: [fileAttachment],
            });
          } else {
            await interaction.followUp({
              content: replyContent,
              files: [fileAttachment],
              ephemeral: ephemeralReply
            });
          }
        }

      }, { postfix: `_messages_export_${channel.name}_${i + 1}.txt` });
    }

    if (ephemeralReply && replyChannel.id !== channel.id) {
      await interaction.editReply({
        content: `The messages have been exported and sent to ${replyChannel}. There are ${splitMessageText.length} files waiting to be downloaded.`,
      });
    }
  } catch (error) {
    console.error(error);
    await interaction.editReply({
      content: 'An error occurred while exporting messages. Please check the bot logs for details.'
    });
  }
}
