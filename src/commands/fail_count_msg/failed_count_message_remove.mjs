import { SlashCommandBuilder } from 'discord.js';
import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';
import { clearUserMessages, findMessageById } from '../../utils/failed_count_msg.mjs';

export const data = new SlashCommandBuilder()
  .setName('failed_count_message_remove')
  .setDescription('Remove a message that has a chance to show when someone messes up the count.')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('User from whom to remove the message')
      .setRequired(false))
  .addStringOption(option =>
    option.setName('message_ids')
      .setDescription('The IDs of the messages to remove, separated by a semicolon (;)')
      .setRequired(false));

export async function execute(interaction) {
  await interaction.deferReply({ ephemeral: true });

  const user = interaction.options.getUser('user');
  const messageIds = interaction.options.getString('message_ids');

  let mentioned_user_id = user ? user.id : null;
  let sender = interaction.user; // User object

  if (mentioned_user_id) {
    await ensureBrainKeyExistsPromise('count_fail_msgs', mentioned_user_id);
    // A user was given!
    let delete_count = await clearUserMessages(mentioned_user_id, sender.id)

    if (delete_count > 0) {
      await interaction.editReply({content: `${delete_count} messages cleared for ${user}.`, ephemeral: true});
    } else {
      await interaction.editReply({content: 'No messages found to clear.', ephemeral: true});
    }
  } else if (messageIds) {
    // Split the messageIds string into an array of IDs
    const ids = messageIds.split(';').map(id => id.trim());

    let removedCount = 0;
    let notFoundCount = 0;
    let unauthorizedCount = 0;

    for (const id of ids) {
      let found_message = findMessageById(id);

      if (found_message) {
        if (sender.id === found_message.set_by) {
          delete brain.data.count_fail_msgs[found_message.given_to][id]; // Delete found message
          removedCount++;
        } else {
          unauthorizedCount++;
        }
      } else {
        notFoundCount++;
      }
    }

    await brain.write();
    let reply = '';
    if (removedCount > 0) reply += `${removedCount} message(s) removed successfully. `;
    if (notFoundCount > 0) reply += `${notFoundCount} message(s) not found. `;
    if (unauthorizedCount > 0) reply += `${unauthorizedCount} message(s) could not be removed due to insufficient permissions. `;
    await interaction.editReply({content: reply, ephemeral: true});
  } else {
    await interaction.editReply({content: 'Please specify a user or message IDs.', ephemeral: true});
  }
}
