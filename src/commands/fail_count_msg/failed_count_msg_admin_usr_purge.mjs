import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('failed_count_msg_admin_usr_purge')
  .setDescription('Purge a user from the failed count messages')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('User to purge messages for')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  await interaction.deferReply({ ephemeral: true });

  const user = interaction.options.getUser('user');
  let userId = user.id;

  await ensureBrainKeyExistsPromise('count_fail_msgs', userId);

  let count_fail_msgs = brain.data.count_fail_msgs;

  let deletedCount = 0;

  // Remove all messages set by the user
  for (let userId in count_fail_msgs) {
    for (let messageId in count_fail_msgs[userId]) {
      if (count_fail_msgs[userId][messageId].set_by === user.id) {
        delete count_fail_msgs[userId][messageId];
        deletedCount++;
      }
    }
  }

  // Remove all messages given to the user
  if (count_fail_msgs[userId]) {
    deletedCount += Object.keys(count_fail_msgs[userId]).length;
    delete count_fail_msgs[userId];
  }

  await brain.write();

  await interaction.editReply({
    content: `Messages purged successfully. ${deletedCount} messages were removed.`,
    ephemeral: true
  });
}
