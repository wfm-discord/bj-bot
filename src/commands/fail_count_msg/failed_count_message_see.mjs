import { SlashCommandBuilder } from 'discord.js';
import {ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';
import {MessageStacker} from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';


export const data = new SlashCommandBuilder()
  .setName('failed_count_message_see')
  .setDescription('See the count fail messages set by a user')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('User to see the messages of')
      .setRequired(false))
  .addBooleanOption(option =>
    option.setName('ephemeral')
      .setDescription('Whether to reply quietly or not (defaults true)')
      .setRequired(false));

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  let ephemeral = interaction.options.getBoolean('ephemeral');
  if (ephemeral === null) {
    ephemeral = true;
  }
  let mentioned_user_id = user ? user.id : null;
  let sender = interaction.user; // User object

  await interaction.deferReply({ephemeral: ephemeral});

  if (mentioned_user_id) {
    // A user was given!
    let messages = ensureBrainKeyExists('count_fail_msgs', mentioned_user_id);
    let prepend = `You have set the following messages on <@${mentioned_user_id}>: \n`;
    const stacker = new MessageStacker(interaction, prepend, ephemeral);
    for (let id in messages) {
      if (messages[id].set_by === sender.id) {
        let m = messages[id]
        await stacker.appendOrSend('💠' + id + (m.is_one_time ? '(one time)' : '') + ': ' + m.text + '\n');
      }
    }
    await stacker.finalize('*You can remove any of these using `/failed_count_message_remove`*',
      'You don\'t have any message set for that user at the moment.'
    );
  } else {
    // Need to sort through to find all your messages
    let messages = ensureBrainKeyExists('count_fail_msgs');
    const stacker = new MessageStacker(interaction, 'You have set the following messages: \n', ephemeral);
    for (let sub_id in messages) {
      for (let id in messages[sub_id]) {
        if (sender.id === messages[sub_id][id].set_by) {
          let m = messages[sub_id][id]
          await stacker.appendOrSend(`💠<@${sub_id}> (${id})` + (m.is_one_time ? ' (one time)' : '') + ': ' + m.text + '\n');
        }
      }
    }
    await stacker.finalize('*You can remove any of these using `/failed_count_message_remove`*',
      'You don\'t have any message set at the moment.'
    );
  }
}
