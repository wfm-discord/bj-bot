import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {MessageStacker} from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';

export const data = new SlashCommandBuilder()
  .setName('failed_count_msg_admin_audit')
  .setDescription('Audit all fail count messages')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  await interaction.deferReply({ ephemeral: true });

  const stacker = new MessageStacker(interaction, 'Messages:\n', true);
  let messages = ensureBrainKeyExists('count_fail_msgs');
  for (let user_id in messages) {
    for (let key in messages[user_id]) {
      let this_message = `💠 id: ${messages[user_id][key].id}\n` +
        `is_one_time: ${messages[user_id][key].is_one_time}\n` +
        `text: ${messages[user_id][key].text}\n` +
        `date_set: ${messages[user_id][key].date_set}\n` +
        `set_by: <@${messages[user_id][key].set_by}>\n` +
        `given_to: <@${messages[user_id][key].given_to}>`;
      await stacker.appendOrSend(this_message);
    }
  }
  await stacker.finalize();
}
