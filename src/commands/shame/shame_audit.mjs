import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { MessageStacker } from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';

export const data = new SlashCommandBuilder()
  .setName('shame_audit')
  .setDescription('Audits what\'s in the brain for the shame role')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction, bot) {
  await interaction.deferReply({ ephemeral: true });

  const stacker = new MessageStacker(interaction, '');

  for await (const shame of bot.shame.allUsers()) {
    let reply = '';
    try {
      let user = await interaction.guild.members.fetch(shame.getUserId());
      reply += `**${user.displayName}**\n`;
    } catch (error) {
      reply += `**<@${shame.getUserId()}>** (Unknown user)\n`;
    }

    // Convert expiration to Unix timestamp and format it for Discord
    const expiresTimestamp = Math.floor((shame.expiresInMs + Date.now()) / 1000);
    reply += `Expires: <t:${expiresTimestamp}:F>\n`;
    reply += `Source: ${shame.source}\n`;
    reply += `Counting Target: ${shame.countingTarget}\n`;
    await stacker.appendOrSend(reply);
  }

  await stacker.finalize('', 'The corner is empty.');
}
