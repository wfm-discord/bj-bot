import { SlashCommandBuilder } from 'discord.js';
import {backfires} from '../../utils/backfire.mjs';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';

const shame_room = process.env.SHAME_ROOM;

export const data = new SlashCommandBuilder()
  .setName('shame')
  .setDescription('Shame a user')
  .addUserOption(option =>
    option.setName('user')
      .setDescription('The user you want to shame')
      .setRequired(true))
  .addIntegerOption(option => option.setName('counting_target')
    .setDescription('Choose a different number for the user to count to to get out of the corner.')
    .setRequired(false))
  .addIntegerOption(option => option.setName('slowdown')
    .setDescription('One-time slow down for the corner.')
    .setRequired(false))
  .addIntegerOption(option => option.setName('mistake_penalty')
    .setDescription('Increase the target by this number if a mistake is made')
    .setRequired(false))
  ;
export async function execute(interaction, bot) {
  await interaction.deferReply();

  // No shame from shamed users
  const callerShame = await bot.shame.forUser(interaction.user.id);
  if (callerShame.isShamed()) {
    await interaction.editReply('Unless you\'re transferring it ' +
      '(which is a whole different command), you cannot ' +
      'shame someone when you are already shamed yourself!\n' +
      'However, for trying, here\'s another 50 in the Corner of Shame...');

    callerShame.countingTarget += 50;
    await callerShame.save();

    await interaction.followUp(`You can count to **${callerShame.countingTarget}** now. :heart:`);
    return;
  }

  // No more shame on shamed users
  const mentioned_user = interaction.options.getUser('user');
  const targetShame = await bot.shame.forUser(mentioned_user.id);
  if (targetShame.isShamed()) {
    await interaction.editReply(`${mentioned_user} is already shamed! **Double shame!** \n` +
      'But no, you can\'t actually shame them more.');
    return;
  }

  const count_to = interaction.options.getInteger('counting_target');
  let count_reply = '';
  if (count_to) {
    count_reply = `\n**Now count to ${count_to} in <#${shame_room}>!**`;
  }

  const slowdown = interaction.options.getInteger('slowdown');
  const mistake_penalty = interaction.options.getInteger('mistake_penalty');

  let slowdown_reply = slowdown ? `\nOne-time slowdown: ${slowdown} seconds` : '';
  let mistake_penalty_reply = mistake_penalty ? `\nMistake penalty: ${mistake_penalty}` : '';

  const channel = interaction.channel.id;

  // Check if user tries to shame the bot itself
  if (mentioned_user.id === interaction.client.user.id) {
    await interaction.editReply('Nice try. Go to the corner for that!');
    await callerShame.start({
      source: 'shaming_bj',
      countingTarget: count_to,
      givenInChannelId: channel,
      oneTimeSlowdown: slowdown,
      mistakePenalty: mistake_penalty,
    });
    await interaction.followUp(`SHAME ON <@${interaction.user.id}>!` + count_reply + slowdown_reply + mistake_penalty_reply);
    return;
  }

  if (mentioned_user.bot) {
    await interaction.editReply("I don't allow bot-shaming around these parts.");
    await callerShame.start({
      source: 'bot_shaming',
      countingTarget: count_to,
      givenInChannelId: channel,
      oneTimeSlowdown: slowdown,
      mistakePenalty: mistake_penalty,
    });
    await interaction.followUp(`SHAME ON <@${interaction.user.id}>!` + count_reply + slowdown_reply + mistake_penalty_reply);
    return;
  }

  if (await backfires(interaction.member, mentioned_user)) {
    let reply = '# Backfire!\n';
    reply += `<@${interaction.user.id}> tried to shame <@${mentioned_user.id}> but it backfired!`;
    await callerShame.start({
      source: 'shame_backfire',
      countingTarget: count_to,
      givenByUserId: interaction.user.id,
      givenInChannelId: channel,
      oneTimeSlowdown: slowdown,
      mistakePenalty: mistake_penalty,
    });
    await interaction.editReply(reply + count_reply + slowdown_reply + mistake_penalty_reply);
    return;
  }

  if (await topHasConsent(bot, interaction.user, mentioned_user)) {
    await targetShame.start({
      source: 'shame_user',
      countingTarget: count_to,
      givenByUserId: interaction.user.id,
      givenInChannelId: channel,
      oneTimeSlowdown: slowdown,
      mistakePenalty: mistake_penalty,
    });
    await interaction.editReply(`SHAME ON <@${mentioned_user.id}>!` + count_reply + slowdown_reply + mistake_penalty_reply);
  } else {
    await interaction.editReply('You do not have consent to shame that user.');
  }
}
