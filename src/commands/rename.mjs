import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
// import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';


export const data = new SlashCommandBuilder()
  .setName('rename')
  .setDescription('Rename another user on the server (with consent)')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you want to rename')
    .setRequired(true))
  .addStringOption(option => option.setName('new_name')
    .setDescription('Max 32 characters, please keep it SFW')
    .setRequired(true))
  // TODO: enforce nickname changes for a period of time
  // .addIntegerOption(option => option.setName('duration')
  //  .setDescription('The name will be enforced for this long (minutes)')
  //  .setRequired(false))
;

export async function execute(interaction, bot) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  let nickname = interaction.options.getString('new_name');
  // let duration = interaction.options.getInteger('duration');


  // check consent
  if (!await topHasConsent(bot, sender, to_user)) {
    await interaction.reply({content: 'You do not have consent to rename that user.', ephemeral: true});
    return;
  }
  await interaction.deferReply();

  let reply = '';

  if (nickname.length > 31) {
    // Can only be 32 characters long
    nickname = nickname.substring(0, 31);
    reply = 'New name has been truncated to \''
      + nickname
      + '\'. Only 32 characters allowed.\n';
  }

  const member = await interaction.guild.members.fetch(to_user.id);
  const oldName = member.displayName;

  // TODO: when I'm ready to enforce it, then start saving the records
  // otherwise they're useless records
  // let record = ensureBrainKeyExists('renamed_subs', to_user.id);
  // record.old_name = oldName;
  // record.new_name = nickname;
  // record.changed_by = sender.id;
  // record.setTime = new Date();
  // record.duration = duration;
  // await brain.write();

  try {
    await member.setNickname(nickname);
    reply += `${sender} has renamed ${oldName} to ${to_user}!`;
  } catch (error) {
    // Delete the record if setNickname fails
    // delete brain.data.renamed_subs[to_user.id];
    // await brain.write();

    reply += 'Failed to set the nickname =(\n' +
      'Error: ' + error.message;
  }

  await interaction.editReply(reply);
}
