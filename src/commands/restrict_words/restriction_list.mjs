import { SlashCommandBuilder, EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle } from 'discord.js';
import { brain } from 'bot-commons-utils/src/utils/brain.mjs';
import {checkRestrictedWordForRemoval} from '../../utils/restricted_words.mjs';

const itemsPerPage = 3; // Adjust the number of items per page as needed

export const data = new SlashCommandBuilder()
  .setName('restriction_list')
  .setDescription('View all restrictions set on a user')
  .addUserOption(option =>
    option
      .setName('target_user')
      .setDescription('The user whose restrictions you want to view')
      .setRequired(true)
  )
  .addStringOption(option =>
    option
      .setName('display_type')
      .setDescription('Select display type')
      .setRequired(false)
      .addChoices(
        { name: 'Detailed', value: 'detailed' },
        { name: 'Simple', value: 'simple' }
      )
  );

export async function execute(interaction) {
  const targetUser = interaction.options.getUser('target_user');
  const displayType = interaction.options.getString('display_type') || 'detailed';
  const restrictions = brain.data.restricted_words[targetUser.id] || {};

  // Check if the user is checking their own restrictions
  const isSelfCheck = interaction.user.id === targetUser.id;

  if (Object.keys(restrictions).length === 0) {
    // Custom message for self check with no restrictions
    if (isSelfCheck) {
      await interaction.reply("You're restriction-free! " +
        "Well, aren't you lucky... " +
        "Don't worry, I'm sure there's *someone* lurking around here just itching to change that. " +
        'You only need to ask :smiling_imp: :sparkling_heart:');
    } else {
      await interaction.reply(`${targetUser.username} has no restrictions.`);
    }
    return;
  }

  if (displayType === 'simple') {
    await interaction.reply({
      content: await createSimpleList(targetUser, restrictions)
    });
  } else {
    const pages = await paginate(restrictions, itemsPerPage, targetUser);
    await interaction.reply({
      embeds: [await createEmbed(targetUser, pages, 0)],
      components: [createRow(0, pages.length, targetUser.id)]
    });
  }
}

async function createSimpleList(targetUser, restrictions) {
  let list = `Restrictions for ${targetUser.username}:\n`;
  for (const [word, settings] of Object.entries(restrictions)) {
    if (await checkRestrictedWordForRemoval(targetUser, word)) continue;
    const type = settings.secret ? '(secret restriction)' : word;
    list += `• \`${type}\`\n`;
  }
  return list;
}

async function paginate(items, itemsPerPage, targetUser) {
  const filteredEntries = await Promise.all(Object.entries(items).map(async ([word, settings]) => {
    const shouldRemove = await checkRestrictedWordForRemoval(targetUser, word);
    return shouldRemove ? null : [word, settings];
  }));

  return filteredEntries.filter(entry => entry !== null).reduce((result, value, index) => {
    const pageIndex = Math.floor(index / itemsPerPage);
    if (!result[pageIndex]) {
      result[pageIndex] = [];
    }
    result[pageIndex].push(value);
    return result;
  }, []);
}

async function createEmbed(targetUser, pages, currentPage) {
  const embed = new EmbedBuilder()
    .setColor(0x0099FF)
    .setTitle(`Restrictions for ${targetUser.username} (Page ${currentPage + 1} of ${pages.length})`)
    .setDescription('Detailed list of restrictions')
    .setTimestamp();

  pages[currentPage].forEach(([word, settings]) => {
    const type = settings.secret ? '(secret restriction)' : word;
    const expiration = settings.expirationDate
      ? `expires <t:${Math.floor(new Date(settings.expirationDate).getTime() / 1000)}:f>`
      : 'Never expires';
    const setter = `Set by: <@${settings.setterId}>`;
    const caseSensitive = settings.secret ? 'May be case sensitive? Or not?' : `${settings.caseSensitive ? 'Case sensitive' : 'Not case sensitive'}`;
    const wholeWord = settings.secret ? 'May be whole word? Or not?' : `${settings.whole_word ? 'Whole word' : 'Partial words'}`;
    const penaltyType = `Punishment: ${settings.penalty.type}`;
    const penaltyCount = `Violations: ${settings.penalty.penaltyCount}`;
    const lastPenalty = settings.penalty.lastPenaltyDate
      ? 'Last Violation: ' +
      `<t:${Math.floor(new Date(settings.penalty.lastPenaltyDate).getTime() / 1000)}:f>`
      : '';

    embed.addFields(
      { name: `\`${type}\``, value: setter, inline: true },
      { name: 'Settings', value: `${caseSensitive}\n${wholeWord}`, inline: true },
      { name: 'Penalties', value: `${penaltyType}\n${penaltyCount}\n${(lastPenalty ? lastPenalty + '\n' : '')}${expiration}`, inline: true }
    );
  });
  return embed;
}

function createRow(currentPage, totalPages, userId) {
  return new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId(`previous_${currentPage - 1}_${userId}`)
        .setLabel('Previous')
        .setStyle(ButtonStyle.Primary)
        .setDisabled(currentPage === 0),
      new ButtonBuilder()
        .setCustomId(`next_${currentPage + 1}_${userId}`)
        .setLabel('Next')
        .setStyle(ButtonStyle.Primary)
        .setDisabled(currentPage === totalPages - 1)
    );
}

// Button handlers
export const buttons = {
  previous: async (interaction, page, userId) => {
    const targetUser = await interaction.client.users.fetch(userId);
    const restrictions = brain.data.restricted_words[targetUser.id];
    const pages = await paginate(restrictions, itemsPerPage, targetUser);

    await interaction.update({
      embeds: [await createEmbed(targetUser, pages, page)],
      components: [createRow(page, pages.length, userId)]
    });
  },
  next: async (interaction, page, userId) => {
    const targetUser = await interaction.client.users.fetch(userId);
    const restrictions = brain.data.restricted_words[targetUser.id];
    const pages = await paginate(restrictions, itemsPerPage, targetUser);

    await interaction.update({
      embeds: [await createEmbed(targetUser, pages, page)],
      components: [createRow(page, pages.length, userId)]
    });
  }
};
