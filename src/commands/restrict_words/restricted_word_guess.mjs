// src/commands/restrict_words/restricted_word_guess.mjs
import { SlashCommandBuilder } from 'discord.js';
import { brain } from 'bot-commons-utils/src/utils/brain.mjs';
import {applyGuessPenaltyAndGetPenaltyReplyFromType, findClosestMatch} from '../../utils/restricted_words.mjs';

export const data = new SlashCommandBuilder()
  .setName('restricted_word_guess')
  .setDescription('Guess a secret restricted word to change its secret status')
  .addStringOption(option =>
    option
      .setName('guess')
      .setDescription('Your guess for the secret restricted word')
      .setRequired(true)
      .setMaxLength(50)
      .setMinLength(1)
  );

export async function execute(interaction) {
  const guess = interaction.options.getString('guess');
  const userId = interaction.user.id;

  // Get the restricted words for the user
  const restrictions = brain.data.restricted_words[userId];
  if (!restrictions) {
    await interaction.reply({
      content: 'You currently have no restricted words.'
    });
    return;
  }

  // Check to see if there are no restricted words
  let secret = false;
  for (const [word, settings] of Object.entries(restrictions)) {
    if (settings.secret) {
      secret = true;
      break;
    }
  }
  if (!secret) {
    await interaction.reply({
      content: 'You currently have no secret restrictions to guess.'
    });
    return;
  }


  // Initialize bad guess count if not present
  // restrictions.badGuessCount = restrictions.badGuessCount || 0;

  // Check if the guess matches any restricted word
  let isMatch = false;
  let matchedWord = null;
  for (const word in restrictions) {
    if (restrictions[word].caseSensitive) {
      if (word === guess) {
        isMatch = true;
        matchedWord = word;
        break;
      }
    } else {
      if (word.toLowerCase() === guess.toLowerCase()) {
        isMatch = true;
        matchedWord = word;
        break;
      }
    }
  }

  if (isMatch) {
    // Check if the word is already not a secret
    if (restrictions[matchedWord].secret === false) {
      await interaction.reply({
        content: `The word \`${matchedWord}\` is already not a secret.`
      });
    } else {
      // Change the secret setting of the matched word
      restrictions[matchedWord].secret = false;
      await brain.write();

      await interaction.reply({
        content: `Correct guess! The restriction on the word \`${matchedWord}\` is now public.`
      });
    }
  } else {
    // Increase bad guess count
    // restrictions.badGuessCount++;

    // Find the closest match among secret restricted words
    let closestMatch = findClosestMatch(guess, restrictions);

    // Apply the penalty for the closest match
    if (closestMatch) {
      const response = await applyGuessPenaltyAndGetPenaltyReplyFromType(guess, closestMatch, restrictions[closestMatch], interaction.user, interaction.channel, interaction.guild);
      await interaction.reply({
        content: response.reply,
        embeds: response.embeds
      });
    } else {
      await interaction.reply({
        content: 'Wow. You were SO FAR off that I don\'t even know what you were trying to guess. Good job. I guess.'
      });
    }
    await brain.write();
  }
};
