//src/commands/restricted_word_admin.mjs
import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { brain } from 'bot-commons-utils/src/utils/brain.mjs';
import { MessageStacker } from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';

export const data = new SlashCommandBuilder()
  .setName('restricted_word_admin')
  .setDescription('Admin commands for managing restricted words')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
  .addSubcommand(subcommand =>
    subcommand
      .setName('remove_word')
      .setDescription('Remove a specific restricted word from a user')
      .addUserOption(option =>
        option
          .setName('target_user')
          .setDescription('The user from whom you want to remove the restriction')
          .setRequired(true)
      )
      .addStringOption(option =>
        option
          .setName('restricted_word')
          .setDescription('The word you want to remove from restrictions')
          .setRequired(true)
      )
  )
  .addSubcommand(subcommand =>
    subcommand
      .setName('clear_all')
      .setDescription('Clear all restricted words from a user')
      .addUserOption(option =>
        option
          .setName('target_user')
          .setDescription('The user whose restrictions you want to clear')
          .setRequired(true)
      )
      .addUserOption(option =>
        option
          .setName('by_user')
          .setDescription('The user requesting the clear action (optional)')
          .setRequired(false)
      )
  )
  .addSubcommand(subcommand =>
    subcommand
      .setName('show_restrictions')
      .setDescription('Show all restricted words for a user')
      .addUserOption(option =>
        option
          .setName('target_user')
          .setDescription('The user whose restrictions you want to see')
          .setRequired(true)
      )
  );

export async function execute(interaction) {
  const subcommand = interaction.options.getSubcommand();

  switch (subcommand) {
    case 'remove_word':
      await removeWord(interaction);
      break;
    case 'clear_all':
      await clearAll(interaction);
      break;
    case 'show_restrictions':
      await showRestrictions(interaction);
      break;
  }
}

async function removeWord(interaction) {
  const targetUser = interaction.options.getUser('target_user');
  const restrictedWord = interaction.options.getString('restricted_word');
  const restrictions = brain.data.restricted_words[targetUser.id];

  if (!restrictions || !restrictions[restrictedWord]) {
    await interaction.reply({
      content: `There is no restriction on the word \`${restrictedWord}\` for ${targetUser.username}.`,
      ephemeral: true
    });
    return;
  }

  delete restrictions[restrictedWord];
  await brain.write();

  await interaction.reply({
    content: `Successfully removed the restriction on the word \`${restrictedWord}\` for ${targetUser.username}.`,
    ephemeral: true
  });
}

async function clearAll(interaction) {
  const targetUser = interaction.options.getUser('target_user');
  const byUser = interaction.options.getUser('by_user');
  let content = `Successfully cleared all restrictions for ${targetUser.username}`;

  if (byUser) {
    let userRestrictions = brain.data.restricted_words[targetUser.id];
    if (!userRestrictions) {
      content = `${targetUser.username} has no restrictions to clear.`;
    } else {
      for (const word in userRestrictions) {
        if (userRestrictions.hasOwnProperty(word) &&
          userRestrictions[word].setterId === byUser.id
        ) {
          delete userRestrictions[word];
        }
      }
      content += ` set by ${byUser.username}.`;
    }
  } else {
    delete brain.data.restricted_words[targetUser.id];
    content += `.`;
  }
  await brain.write();

  await interaction.reply({
    content: content,
    ephemeral: true
  });
}

async function showRestrictions(interaction) {
  const targetUser = interaction.options.getUser('target_user');
  const restrictions = brain.data.restricted_words[targetUser.id];

  if (!restrictions || Object.keys(restrictions).length === 0) {
    await interaction.reply({
      content: `${targetUser.username} has no restrictions set.`,
      ephemeral: true
    });
    return;
  }

  // Initialize MessageStacker with the interaction
  const messageStacker = new MessageStacker(interaction, '', true);

  // Response preamble
  let responsePreamble = `Restricted words for ${targetUser.username}:\n`;
  await messageStacker.appendOrSend(responsePreamble);

  for (const [word, details] of Object.entries(restrictions)) {
    let messageTemp = `* \`${word}\`, set by <@${details.setterId}>, secret: ${details.secret}, expiration: ${details.expirationDate || 'none'}, case-sensitive: ${details.caseSensitive}, whole word: ${details.whole_word}\n`;
    await messageStacker.appendOrSend(messageTemp);
  }

  // Finalize the message; send what's left or react if empty
  await messageStacker.finalize();
}
