// src/commands/restrict_words/restricted_word_set.mjs
import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';
import {escapeArray, isValidWord} from '../../utils/restricted_words.mjs';

export const data = new SlashCommandBuilder()
  .setName('restricted_word_set')
  .setDescription('Set a restricted word on another user with consent')
  .addUserOption(option =>
    option
      .setName('target_user')
      .setDescription('The user you want to set the restriction on')
      .setRequired(true)
  )
  .addStringOption(option =>
    option
      .setName('restricted_word')
      .setDescription('The word you want to restrict')
      .setRequired(true)
      .setMaxLength(100)
      .setMinLength(1)
  )
  .addStringOption(option =>
    option
      .setName('case_sensitive')
      .setDescription('Whether the restriction should be case-sensitive')
      .setRequired(true)
      .addChoices(
        { name: 'Case sensitive', value: 'sensitive' },
        { name: 'Not case sensitive', value: 'insensitive' }
      )
  )
  .addStringOption(option =>
    option
      .setName('whole_word')
      .setDescription('Match type for the restriction')
      .setRequired(true)
      .addChoices(
        { name: 'Match on whole word', value: 'whole' },
        { name: 'Match on partial words', value: 'partial' }
      )
  )
  .addStringOption(option =>
    option
      .setName('punishment')
      .setDescription('Punishment for using restricted words (starts small, will escalate)')
      .setRequired(true)
      .addChoices(
        { name: 'Nothing; just warn', value: 'nothing' },
        { name: 'Gagging', value: 'gag' },
        { name: 'Shame', value: 'shame' },
        { name: 'Grounding', value: 'grounding' },
        { name: 'Lines', value: 'lines' },
        { name: 'Random', value: 'random' }
      )
  )
  .addIntegerOption(option =>
    option
      .setName('expiration')
      .setDescription('Number of hours after which the restriction expires')
      .setRequired(false)
  )
  .addIntegerOption(option =>
    option
      .setName('violation_goal')
      .setDescription('If the goal is hit, the restriction will fall off')
      .setRequired(false)
  )
  .addStringOption(option =>
    option
      .setName('tattle_tale')
      .setDescription('Choose if you want to be notified (tattle) when a violation or bad guess occurs')
      .setRequired(false)
      .addChoices(
        { name: 'Notify me when a violation or bad guess occurs', value: 'true' },
        { name: 'Do not notify me (default)', value: 'false' }
      )
  )
  .addStringOption(option =>
    option
      .setName('where')
      .setDescription('Where the restriction applies')
      .setRequired(false)
      .addChoices(
        { name: 'Apply to almost everywhere (default)', value: 'most' },
        { name: 'Apply EVERYWHERE including lines, corner, math', value: 'everywhere' }
      )
  )
  .addBooleanOption(option =>
    option
      .setName('ephemeral')
      .setDescription('Set the restriction secretly (ephemeral response)')
      .setRequired(false)
  );

export async function execute(interaction, bot) {
  const sender = interaction.user;
  const targetUser = interaction.options.getUser('target_user');
  let restrictedWord = interaction.options.getString('restricted_word');
  const caseSensitive = interaction.options.getString('case_sensitive') === 'sensitive';
  const whole_word = interaction.options.getString('whole_word') === 'whole';
  const punishment = interaction.options.getString('punishment') || 'nothing';
  const expirationHours = interaction.options.getInteger('expiration');
  const isEphemeral = interaction.options.getBoolean('ephemeral') || false;
  const tattleTaleChoice = interaction.options.getString('tattle_tale');
  const notifyOnViolation = tattleTaleChoice === 'true';
  const applyScope = interaction.options.getString('where') || 'most';

  // Check for consent
  if (!await topHasConsent(bot, sender, targetUser)) {
    await interaction.reply({
      content: 'You do not have consent to set a restriction for that user.',
      ephemeral: true
    });
    return;
  }

  if (!isValidWord(restrictedWord)) {
    await interaction.reply({
      content: 'Invalid word: unpaired brackets or parentheses are not allowed unless they are escaped.',
      ephemeral: true
    });
    return;
  }

  // Disallow certain words and tags
  const disallowedWords = ['red', 'safeword', '@everyone', '@here'];
  if (disallowedWords.includes(restrictedWord.toLowerCase()) || restrictedWord.startsWith('@')) {
    await interaction.reply({
      content: 'You cannot set a restriction on this word or tag.',
      ephemeral: true
    });
    return;
  }

  // Escape some characters when set alone
  if (escapeArray.includes(restrictedWord)) {
    restrictedWord = `\\${restrictedWord}`;
  }

  // Calculate expiration date if expiration is set
  let expirationDate = null;
  if (expirationHours) {
    expirationDate = new Date(Date.now() + expirationHours * 3600000).toISOString();
  }

  // Get or create the object {} inside the brain for this user's restricted word list
  const restrictions = await ensureBrainKeyExistsPromise('restricted_words', targetUser.id);

  // Check if the word already exists
  if (restrictions[restrictedWord]) {
    await interaction.reply({
      content: `The word \`${restrictedWord}\` is already restricted for ${targetUser.username}.`,
      ephemeral: true
    });
    return;
  }

  // Add the new restriction
  restrictions[restrictedWord] = {
    word: restrictedWord,
    setterId: sender.id,
    timestamp: new Date().toISOString(),
    caseSensitive: caseSensitive,
    whole_word: whole_word,
    secret: isEphemeral,
    penalty: {
      type: punishment,
      lastPenaltyDate: null,
      penaltyCount: 0,
      guessCount: 0
    },
    scope: applyScope,
    expirationDate: expirationDate,
    notifyOnViolation: notifyOnViolation,
    violationGoal: interaction.options.getInteger('violation_goal')
  };

  // Write the updated restrictions to the brain
  await brain.write();

  // Send a confirmation message
  await interaction.reply({
    content: `Successfully set the word restriction \`${restrictedWord}\` for ${targetUser}.`,
    ephemeral: isEphemeral
  });
}
