import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import { brain } from 'bot-commons-utils/src/utils/brain.mjs';
import {escapeArray} from '../../utils/restricted_words.mjs';

export const data = new SlashCommandBuilder()
  .setName('restricted_word_remove')
  .setDescription('Remove a restricted word from a user with consent')
  .addUserOption(option =>
    option
      .setName('target_user')
      .setDescription('The user from whom you want to remove the restriction')
      .setRequired(true)
  )
  .addStringOption(option =>
    option
      .setName('restricted_word')
      .setDescription('The word you want to remove from restrictions')
      .setRequired(true)
      .setMaxLength(100)
      .setMinLength(1)
  )
  .addBooleanOption(option =>
    option
      .setName('ephemeral')
      .setDescription('Set the response to be visible only to you')
      .setRequired(false)
  );

export async function execute(interaction, bot) {
  const sender = interaction.user;
  const targetUser = interaction.options.getUser('target_user');
  const ephemeral = interaction.options.getBoolean('ephemeral') || false;
  let restrictedWord = interaction.options.getString('restricted_word');

  // Check for consent
  if (!await topHasConsent(bot, sender, targetUser)) {
    await interaction.reply({
      content: 'You do not have consent to remove a restriction for that user.',
      ephemeral: true
    });
    return;
  }

  const restrictions = brain.data.restricted_words[targetUser.id];

  // Handle auto-escaped characters
  const escapedWord = escapeArray.includes(restrictedWord[0]) ? `\\${restrictedWord}` : restrictedWord;

  let wordRemoved = ''; // Variable to store which word was actually removed

  // Check both the original and escaped versions of the word
  if (!restrictions || (!restrictions[restrictedWord] && !restrictions[escapedWord])) {
    await interaction.reply({
      content: `There is no restriction on the word \`${restrictedWord}\` for ${targetUser.username}.`,
      ephemeral: ephemeral
    });
    return;
  }

  // Remove the restriction and record which word was removed
  if (restrictions[restrictedWord]) {
    delete restrictions[restrictedWord];
    wordRemoved = restrictedWord;
  } else if (restrictions[escapedWord]) {
    delete restrictions[escapedWord];
    wordRemoved = escapedWord;
  }

  // Write the updated restrictions to the brain
  await brain.write();

  // Send a confirmation message including the word that was removed
  await interaction.reply({
    content: `Successfully removed the restriction on the word \`${wordRemoved}\` for ${targetUser.username}.`,
    ephemeral: ephemeral
  });
}
