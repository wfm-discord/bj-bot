import { SlashCommandBuilder } from 'discord.js';
import { brain } from 'bot-commons-utils/src/utils/brain.mjs';
import { MessageStacker } from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';

export const data = new SlashCommandBuilder()
  .setName('restricted_words_i_set')
  .setDescription('View all restricted words you have set on others (replies ephemerally)');

export async function execute(interaction) {
  const senderId = interaction.user.id;
  const allRestrictions = brain.data.restricted_words;

  const stacker = new MessageStacker(interaction, '', true);

  let hasRestrictions = false;

  for (const [userId, restrictions] of Object.entries(allRestrictions)) {
    for (const [word, settings] of Object.entries(restrictions)) {
      if (settings.setterId === senderId) {
        await stacker.appendOrSend(`• On <@${userId}>: \`${word}\`\n`);
        hasRestrictions = true;
      }
    }
  }

  if (!hasRestrictions) {
    await stacker.finalize('', 'You have not set any restricted words.');
  } else {
    await stacker.finalize();
  }
}
