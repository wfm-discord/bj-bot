import { SlashCommandBuilder } from 'discord.js';
import {ensureBrainKeyExistsPromise, brain} from 'bot-commons-utils/src/utils/brain.mjs';

const typing_role = process.env.CURRENTLY_TYPING_ROLE_ID;

export const data = new SlashCommandBuilder()
  .setName('hard_at_work')
  .setDescription('Set yourself a role to show the server you are busy typing. Optionally ping your dominant.')
  .addUserOption(option =>
    option.setName('working_for')
      .setDescription('The dominant you want to ping to let them know you are typing for them')
      .setRequired(false))
  .addIntegerOption(option =>
    option.setName('duration')
      .setDescription('How many minutes you want to hold onto the role (default 30, max 180 minutes)')
      .setRequired(false))
;

export async function execute(interaction) {
  // Fetch the user from the interaction
  const member = interaction.guild.members.cache.get(interaction.user.id);

  // Check if the user has the role
  if (member.roles.cache.has(typing_role)) {
    await interaction.reply({ content: 'You already have the typing role!', ephemeral: true });
    return;
  }

  // Get the "working_for" option from the command (if provided)
  const userOption = interaction.options.getUser('working_for');

  // Get the "duration" option from the command (if provided), and ensure it doesn't exceed 180
  let duration = interaction.options.getInteger('duration') || 30; // default to 30 minutes if not provided
  if (duration > 180) {
    duration = 180; // max of 180 minutes
  }

  // Set the temporary role for the user
  await member.roles.add(typing_role);

  // Construct the reply message
  let content = `Nice to see you have your priorities in check, ${member.displayName}. We'll still be here when you're done typing`;
  if (userOption) {
    content += ` for ${userOption}!`;
  } else {
    content += '.';
  }

  // Reply to the interaction
  await interaction.reply(content);

  const hard_at_work = await ensureBrainKeyExistsPromise('hard_at_work', interaction.user.id);
  let timeout = duration * 60000; // convert minutes to milliseconds
  hard_at_work.timeout = timeout;
  hard_at_work.end = new Date().getTime() + timeout;

  await brain.write();
}
