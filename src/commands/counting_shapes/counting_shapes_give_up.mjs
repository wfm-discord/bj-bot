import {SlashCommandBuilder} from 'discord.js';
import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('counting_shapes_give_up')
  .setDescription('Give up and have me tell you how many shapes were there.');

export async function execute(interaction, bot) {
  const user = interaction.user;

  let shape_record = ensureBrainKeyExists('shape_count');
  if (!shape_record[user.id]) {
    return interaction.reply('You aren\'t counting any shapes right now. ' +
      'Use `/counting_shapes_start` to get started!'
    );
  }

  const correctCount = shape_record[user.id].count;
  const messageLink = shape_record[user.id].message_link;

  const endTime = Date.now();
  const timeTaken = (endTime - shape_record[user.id].start_time) / (1000 * 60); // in minutes

  const stats = await bot.stats.shapeCountForUser(user.id);
  await stats.update({
    timeTaken: timeTaken,
    difficulty: shape_record[user.id].difficulty,
    size: shape_record[user.id].size,
    movement: shape_record[user.id].movement,
    color: shape_record[user.id].colorTheme
  });

  // Delete the user's record as they gave up
  delete shape_record[user.id];

  await brain.write(); // save changes to the brain

  return interaction.reply(`You gave up! There were ${correctCount} shapes. Here's the [original image](${messageLink}).`);
}
