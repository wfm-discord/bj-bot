import {SlashCommandBuilder, AttachmentBuilder, EmbedBuilder} from 'discord.js';
import {createCanvas} from 'canvas';
import GIFEncoder from 'gif-encoder-2';

import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('counting_shapes_start')
  .setDescription('Generate an image with random colored shapes')
  .addStringOption(option =>
    option.setName('size')
      .setDescription('Select the size of the canvas')
      .setRequired(true)
      .addChoices(
        {name: 'x-small', value: 'x_small'},
        {name: 'small', value: 'small'},
        {name: 'medium', value: 'medium'},
        {name: "I'm going to be here a while", value: 'large'},
      )
  )
  .addStringOption(option =>
    option.setName('difficulty')
      .setDescription('Select the difficulty level')
      .setRequired(true)
      .addChoices(
        {name: 'easy', value: 'easy'},
        {name: 'medium', value: 'medium'},
        {name: "oh lord that's a lot of shapes", value: 'hard'},
      )
  )
  .addStringOption(option =>
    option.setName('movement')
      .setDescription('Select how the shapes should move')
      .setRequired(true)
      .addChoices(
        {name: "don't move", value: 'still'},
        {name: 'fade in and out', value: 'fade'},
      )
  )
  .addStringOption(option =>
    option.setName('color_theme')
      .setDescription('Select a color theme for the shapes')
      .setRequired(true)
      .addChoices(
        {name: 'random (default)', value: 'random'},
        {name: 'black', value: 'black'},
        {name: 'white', value: 'white'},
        {name: 'rainbow', value: 'rainbow'},
        {name: 'pastel', value: 'pastel'}
      )
  )
  .addIntegerOption(option =>
    option.setName('target_shape_sides')
      .setDescription('How many sides would you like the shapes to have? (0 for circles)')
      .setRequired(true)
//  )
//  .addIntegerOption(option =>
//    option.setName('distraction_probability')
//      .setDescription('Chance for shapes you shouldn\'t count to appear (in percent)')
//      .setRequired(false)
);

export async function execute(interaction) {
  const user = interaction.user;

  let shape_record = ensureBrainKeyExists('shape_count');
  if (shape_record[user.id]) {
    // Link back to the original image message
    return interaction.reply('You already have shapes to count. ' +
      `[Here's the message with the shapes.](${shape_record[user.id].message_link})\n\n` +
      'If you are ready to submit your count use `/counting_shapes_answer`'
    );
  }

  await interaction.deferReply();
  const size = interaction.options.getString('size');
  const difficulty = interaction.options.getString('difficulty');
  const movement = interaction.options.getString('movement');
  const colorTheme = interaction.options.getString('color_theme');
  const targetShapeSides = interaction.options.getInteger('target_shape_sides');
//  const distractionProbability = interaction.options.getInteger('distraction_probability') / 100;

  const config = generateShapes(size, difficulty, colorTheme, targetShapeSides);
  let attachment;
  if (movement === 'fade') {
    attachment = new AttachmentBuilder(
      await renderAnimatedImage(config),
      {name: 'shapes.gif'},
    );
  } else {
    attachment = new AttachmentBuilder(
      renderStillImage(config),
      {name: 'shapes.png'},
    );
  }

  let reply = '**Count the shapes in the image!**\n';
  reply += `Size: ${size}\n`;
  reply += `Difficulty: ${difficulty}\n\n`;
  reply += 'Submit your answer using `/counting_shapes_answer` or give up with `/counting_shapes_give_up`';
  const sentMessage = await interaction.editReply({ content: reply, files: [attachment] });
  const messageId = sentMessage.id;
  const messageLink = `https://discord.com/channels/${interaction.guildId}/${interaction.channelId}/${messageId}`;

  shape_record[user.id] = {
    'start_time': Date.now(),
    'message_id': messageId,
    'message_link': messageLink,
    'count': config.shapes.length,
    'size': size,
    'difficulty': difficulty,
    'movement': movement,
    'colorTheme': colorTheme,
    'targetShapeSides': targetShapeSides,
    'guesses': 0,
  }
  await brain.write();
}

class Color {
  constructor(red, green, blue) {
    this.red = red;
    this.green = green;
    this.blue = blue;
  }

  css(alpha = 1.0) {
    return `rgba(${this.red}, ${this.green}, ${this.blue}, ${alpha})`;
  }
}

function makeAlphaSequence(holdIn, fadeOut, holdOut, fadeIn) {
  const seq = [];
  for (let i = 0; i < holdIn; i++) {
    seq[i] = 1.0;
  }
  for (let i = 0; i < fadeOut; i++) {
    seq[holdIn + i] = (fadeOut - i) / (fadeOut + 1);
  }
  for (let i = 0; i < holdOut; i++) {
    seq[holdIn + fadeOut + i] = 0.0;
  }
  for (let i = 0; i < fadeIn; i++) {
    seq[holdIn + fadeOut + holdOut + i] = (i + 1) / (fadeIn + 1);
  }
  return seq;
}
const ALPHA_SEQUENCE = makeAlphaSequence(16, 8, 16, 8);

class Shape {
  constructor(color, phase) {
    this.color = color;
    this.phase = phase;
  }

  _drawPath(ctx, frame) {}

  draw(ctx, frame) {
    ctx.beginPath();
    this._drawPath(ctx, frame);
    ctx.closePath();
    ctx.fillStyle = this.color.css(
      ALPHA_SEQUENCE[(frame + this.phase) % ALPHA_SEQUENCE.length]
    );
    ctx.fill();
  }
}

class Polygon extends Shape {
  constructor(x, y, sides, sidelen, color, phase) {
    super(color, phase);
    this.x = x;
    this.y = y;
    this.sides = sides;
    this.sidelen = sidelen;
  }

  _drawPath(ctx, frame) {
    for (let i = 0; i < this.sides; i++) {
      let angle = 2*Math.PI * i / this.sides;
      let px = this.x + Math.cos(angle) * this.sidelen, py = this.y + Math.sin(angle) * this.sidelen;
      if (i == 0) ctx.moveTo(px, py);
      else ctx.lineTo(px, py);
    }
  }
}

class Circle extends Shape {
  constructor(x, y, radius, color, phase) {
    super(color, phase);
    this.x = x;
    this.y = y;
    this.radius = radius;
  }

  _drawPath(ctx, frame) {
    ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
  }
}

// Function to generate shape image

function generateShapes(size, difficulty, colorTheme, targetShapeSides, distractionProbability) {
  let width, height;

  // Define canvas sizes
  switch (size) {
    case 'x_small':
      width = height = 200;
      break;
    case 'small':
      width = height = 400;
      break;
    case 'medium':
      width = height = 600;
      break;
    case 'large':
      width = height = 800;
      break;
    default:
      throw new Error('Size not given');
  }

  // Define shape count based on difficulty and canvas size
  let baseShapeCount, variation;
  switch (difficulty) {
    case 'easy':
      baseShapeCount = width * height / 4000;
      variation = baseShapeCount / 2;
      break;
    case 'medium':
      baseShapeCount = width * height / 2000;
      variation = baseShapeCount / 2;
      break;
    case 'hard':
      baseShapeCount = width * height / 300;
      variation = baseShapeCount / 2;
      break;
    default:
      throw new Error('Difficulty not given');
  }

  const plannedShapeCount = Math.floor(Math.random() * variation) + baseShapeCount;


  const shapes = [];
  const padding = 4; // Distance from other shapes and the canvas edge
  const radius = 5;

  // Ensure that the generated shape is at least padding away from others and the canvas edge
  for (let i = 0; i < plannedShapeCount; i++) {
    let x, y;
    let attemptCount = 0; // Counter for the number of placement attempts

    // Ensure that the generated shape is at least padding away from others and the canvas edge
    do {
      x = padding + radius + (Math.random() * (width - 2 * (radius + padding)));
      y = padding + radius + (Math.random() * (height - 2 * (radius + padding)));
      attemptCount++;
    } while (isShapeTooClose(shapes, x, y, padding + radius) && attemptCount < 1000);

    // If a valid position wasn't found after 1000 attempts, break out of the entire loop
    if (attemptCount >= 1000) {
      break;
    }

//    let isDistraction = Math.random() < distractionProbability;

    const phase = Math.floor(Math.random() * ALPHA_SEQUENCE.length);
    if (targetShapeSides > 2) shapes.push(new Polygon(x, y, targetShapeSides, radius, getColorByTheme(colorTheme), phase));
    else shapes.push(new Circle(x, y, radius, getColorByTheme(colorTheme), phase));
  }

  return {
    width,
    height,
    shapes,
  };
}

function renderStillImage(config) {
  const canvas = createCanvas(config.width, config.height);
  const ctx = canvas.getContext('2d');

  for (const shape of config.shapes) {
    shape.draw(ctx);
  }

  return canvas.toBuffer();
}

async function renderAnimatedImage(config) {
  const gif = new GIFEncoder(config.width, config.height, 'octree', true);
  const canvas = createCanvas(config.width, config.height);
  const ctx = canvas.getContext('2d');

  gif.setDelay(75);
  gif.start();

  for (let frame = 0; frame < ALPHA_SEQUENCE.length; frame++) {
    ctx.fillStyle = '#313338'; // Discord background color
    ctx.fillRect(0, 0, config.width, config.height);

    for (const shape of config.shapes) {
      shape.draw(ctx, frame);
    }

    gif.addFrame(ctx);
  }

  gif.finish();
  return gif.out.getData();
}

// Check if a shape is too close to other shapes
function isShapeTooClose(shapes, x, y, minDistance) {
  return shapes.some(shape => {
    const dx = shape.x - x;
    const dy = shape.y - y;
    return Math.sqrt(dx * dx + dy * dy) < minDistance;
  });
}

// Get colors based on the selected theme
function getColorByTheme(theme) {
  switch (theme) {
    case 'random':
      let r, g, b;
      do {
        r = Math.floor(Math.random() * 255);
        g = Math.floor(Math.random() * 255);
        b = Math.floor(Math.random() * 255);
      } while (r + g + b < 200 || r + g + b > 600);
      return new Color(r, g, b);

    case 'black':
      return new Color(0, 0, 0);

    case 'white':
      return new Color(255, 255, 255);

    case 'rainbow':
      const rainbowColors = [
        new Color(0xff, 0x00, 0x00), // red
        new Color(0xff, 0xa5, 0x00), // orange
        new Color(0xff, 0xff, 0x00), // yellow
        new Color(0x00, 0x80, 0x00), // green
        new Color(0x00, 0x00, 0xff), // blue
        new Color(0x4b, 0x00, 0x82), // indigo
        new Color(0xee, 0x82, 0xee), // violet
      ];
      return rainbowColors[Math.floor(Math.random() * rainbowColors.length)];

    case 'pastel':
      const pastelColors = [
        new Color(0xFF, 0xB6, 0xC1),
        new Color(0xFF, 0xD1, 0xDC),
        new Color(0xB0, 0xE0, 0xE6),
        new Color(0xFF, 0xDD, 0xA4),
        new Color(0xFF, 0xA0, 0x7A),
        new Color(0x98, 0xFB, 0x98),
        new Color(0xF0, 0xE6, 0x8C),
      ];
      return pastelColors[Math.floor(Math.random() * pastelColors.length)];

    default:
      return new Color(128, 128, 128); // Default to grey
  }
}
