import {SlashCommandBuilder} from 'discord.js';
import {ensureBrainKeyExists, brain} from 'bot-commons-utils/src/utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('counting_shapes_answer')
  .setDescription('Submit your answer for the shape count')
  .addIntegerOption(option => option
    .setName('number')
    .setDescription('Your count of the shapes')
    .setRequired(true)
  );

export async function execute(interaction, bot) {
  const user = interaction.user;

  let shape_record = ensureBrainKeyExists('shape_count');
  if (!shape_record[user.id]) {
    return interaction.reply('You aren\'t counting any shapes right now. ' +
      'Use `/counting_shapes_start` to get started!'
    );
  }

  const guess = interaction.options.getInteger('number');
  const correctCount = shape_record[user.id].count; // retrieve correct shape count from the saved record
  shape_record[user.id].guesses++;
  let guesses = shape_record[user.id].guesses;

  const endTime = Date.now();
  const timeTaken = (endTime - shape_record[user.id].start_time) / (1000 * 60); // in minutes
  const size = shape_record[user.id].size;
  const difficulty = shape_record[user.id].difficulty;
  const movement = shape_record[user.id].movement;
  const colorTheme = shape_record[user.id].colorTheme;

  const stats = await bot.stats.shapeCountForUser(user.id);
  if (guess === correctCount) {
    delete shape_record[user.id]; // delete the record for this user
    await interaction.reply(`Correct! Well done! There were ${correctCount} shapes.\n` +
      `You chose a ${size} canvas with ${difficulty} difficulty.\n` +
      `It took you ${timeTaken.toFixed(2)} minutes and ${guesses} guess${guesses > 1 ? 'es' : ''}!`);
    await stats.update({
      isSuccess: true,
      timeTaken,
      difficulty,
      size,
      movement,
      color: colorTheme,
      guesses: 1,
    });
  } else {
    await interaction.reply(`Wrong! ${guess} was not the answer! You have made ${guesses} guesses so far.`);
    await stats.update({
      difficulty,
      size,
      movement,
      color: colorTheme,
      guesses: 1,
    });
  }
  await brain.write(); // save changes to the brain
}
