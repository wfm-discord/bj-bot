import { SlashCommandBuilder } from 'discord.js';

import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import { wfmUtils } from '../../utils/wfm.mjs';
import {punishMassPings} from '../../utils/security.mjs';

export const data = new SlashCommandBuilder()
  .setName('set_wfm')
  .setDescription('Set your profile link')
  .addStringOption(option => option.setName('link')
    .setDescription('Your WFM profile link')
    .setRequired(true));

export async function execute(interaction) {
  let link = interaction.options.getString('link');
  link = link.trim();

  await ensureBrainKeyExistsPromise('wfm_profiles');

  // Check if the link starts with "http://" or "https://"
  if (!link.startsWith('https://writeforme.org/')) {
    interaction.reply('That didn\'t look like a valid link. ' +
      'Please make sure it starts with "https://writeforme.org/"');
    return;
  }

  if (link === 'https://writeforme.org/profile/') {
    interaction.reply('I\'m sorry that\'s the page everyone views their own profiles on. Use `wfm profile` if you need help finding your link.');
  } else if (await punishMassPings(interaction, link)) {
    // handled by punishMassPings
  } else {
    brain.data.wfm_profiles[interaction.user.id] = link;
    await brain.write();
    await wfmUtils.createUserProfileEmbed('I will remember your profile is '
        + link
        + '\nAnyone can call it up using "wfm <@'
        + interaction.user.id
        + '>"', interaction, interaction.user.id);
  }
}
