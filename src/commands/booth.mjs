import { SlashCommandBuilder, PermissionFlagsBits, ChannelType } from 'discord.js';


const category = process.env.BOOTH_CATEGORY_ID;

export const data = new SlashCommandBuilder()
  .setName('booth')
  .setDescription('Create a booth for a user.')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to make the booth for.')
    .setRequired(true))
  .addStringOption(option => option.setName('special_name')
    .setDescription('If something other than the default name.')
    .setRequired(false))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  await interaction.deferReply({ ephemeral: true });
  const user = interaction.options.getUser('user');
  const name = interaction.options.getString('special_name') || user.username;

  const server = interaction.guild;
  let channel = await server.channels.create({
    name: 'under-construction',
    type: ChannelType.GuildText,
    parent: category
  });

  // Remove perms from @everyone to view the channel, message in it, or create threads
  await channel.permissionOverwrites.create(channel.guild.roles.everyone, {
    ViewChannel: false,
    SendMessages: false,
    CreatePublicThreads: false,
    CreatePrivateThreads: false,
  });

  // Give the mentioned user the ability to manage the channel and create threads
  await channel.permissionOverwrites.create(user, {
    ManageChannels: true,
    SendMessages: true,
    CreatePublicThreads: true,
    CreatePrivateThreads: true,
    ManageMessages: true
  });

  // Allow Typists, Task Setters, and Switches to view the channel
  let role = server.roles.cache.find(role => role.name === 'Typist');
  await channel.permissionOverwrites.create(role, {
    ViewChannel: true,
  });
  role = server.roles.cache.find(role => role.name === 'Task Setter');
  await channel.permissionOverwrites.create(role, {
    ViewChannel: true,
  });
  role = server.roles.cache.find(role => role.name === 'Switch');
  await channel.permissionOverwrites.create(role, {
    ViewChannel: true,
  });

  // Don't let quarantine members do anything, blocks the owner from using their booth while quarantined
  role = server.roles.cache.find(role => role.name === 'quarantine');
  await channel.permissionOverwrites.create(role, {
    SendMessages: false,
    ViewChannel: false,
    ReadMessageHistory: false,
  });

  // Set the booth name
  await channel.setName(name);

  await interaction.editReply('Booth has been created. <#' + channel.id + '>');

  // Send a DM to the user
  try {
    await user.send(`Hi there! Your booth has been created. <#${channel.id}>
 
It's at the bottom of the list at the moment but every time you post it'll pop up to the top, keeping the most active booths at the top of the list. 

Please keep in mind we're looking for regular updates to the channel. If your booth goes unupdated too long you may get a DM to start poking at you to post, and if it really goes unupdated too long we'll remove it. Generally an update once per month is good, more of course is great. Channels get hidden and eventually deleted after about 6 months of no activity.

It's currently set as a general audience room. If you intend on posting NSFW content we can mark it as an "age restricted" room. That's really up to you how you'd like it marked. If you're unsure, you can just mark it as age restricted to start. You can change that setting but if you need help, please message or ping beta.

You have control over your booth to edit your channel name and topic, and manage any messages posted in your booth. Booths are set up by default such that only you can post in them and they are read-only for everyone else. If you need permissions changed message or ping beta. 

*This is an automated message and I am a bot so please don't reply and expect anything back from me because I can't help you.* :relaxed:`);
    await interaction.followUp({ content: `DM sent to ${user.toString()}.`, ephemeral: true });
    await interaction.channel.send(`Booth has been created for ${user.toString()}. Please check your DMs for a message from me.`);
  } catch (error) {
    console.error(`Could not send DM to ${user.tag}.`, error);
    // Optionally inform the command executor that the DM could not be sent
    await interaction.followUp({ content: `Failed to send a DM to ${user.toString()}.`, ephemeral: true });
  }
}
