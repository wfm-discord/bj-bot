import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('clear_lines')
  .setDescription('Manually clear a users lines, will also delete their channel if they have one.')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you wish to clear.')
    .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction, bot) {
  const user = interaction.options.getUser('user');
  const lines = await bot.lines.forUser(user.id);
  if (lines.hasLines()) {
    await lines.cancelLinesNow();
    await interaction.reply({content: 'Lines cleared successfully', ephemeral: true});
  } else {
    await interaction.reply({content: 'That user has no lines assigned.', ephemeral: true});
  }
}
