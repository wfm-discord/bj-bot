import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {backfires} from '../../utils/backfire.mjs';
import {punishMassPings} from '../../utils/security.mjs';

export const data = new SlashCommandBuilder()
  .setName('assign_lines')
  .setDescription('Assigns a line to a user')
  .addSubcommand(subcommand =>
    subcommand
      .setName('set')
      .setDescription('Assign a specific line to the user.')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to assign lines to.')
        .setRequired(true))
      .addIntegerOption(option => option.setName('num_lines')
        .setDescription('The number of lines the user should type.')
        .setRequired(true))
      .addStringOption(option => option.setName('line')
        .setDescription('The line you want the user to write.')
        .setRequired(true))
      .addStringOption(option => option.setName('personalized_instructions')
        .setDescription('Special instructions for the lines (free text) - please use ||spoiler text|| for anything NSFW.')
        .setRequired(false))
      .addBooleanOption(option => option.setName('hide_number_completed')
        .setDescription('Set whether the number of lines completed should be hidden')
        .setRequired(false))
      .addStringOption(option => option.setName('random_case')
        .setDescription('Set whether the typist should have to type the lines WiTH rAndOME CapiTaLIZaTIon')
        .setRequired(false)
        // When xkcdoer first implemented random case they wrote a bug that made it so that
        // when assigning random lines, random_case only affects the line after a
        // mistake. Buggy mode reenables this bug. If lines aren't random, it will
        // also only randomize on errors.
        //
        // The way this works is simple(-ish), but quite confusing.  There are two
        // if statements that branch on this value. The one that determines if the
        // line after a correctly typed line is not taken with the value "buggy",
        // but the one after an incorrectly typed line is.
        .addChoices(
          {name: 'rANdoM CAsE', value: 'random'},
          {name: 'Normal case', value: 'normal'},
          {name: 'Buggy', value: 'buggy'},
        ))
      .addStringOption(option => option.setName('spectator')
        .setDescription('Controls who can view and speak in the channel.')
        .setRequired(false)
        .addChoices(
          { name: 'Off - Just you and the person doing lines', value: 'off' },
          { name: 'Viewing - everyone can see the channel', value: 'viewing' },
          { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
        ))
      .addIntegerOption(option => option.setName('penalty_lines')
        .setDescription('The number of extra lines the user should type for making a mistake.')
        .setRequired(false)))
  .addSubcommand(subcommand =>
    subcommand
      .setName('random')
      .setDescription('Assign random lines to the user based on a selected option.')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to assign lines to.')
        .setRequired(true))
      .addIntegerOption(option => option.setName('num_lines')
        .setDescription('The number of lines the user should type.')
        .setRequired(true))
      .addStringOption(option => option.setName('type')
        .setDescription('The type of random lines to generate.')
        .setRequired(true)
        .addChoices(
          { name: 'Random Words', value: 'random_words' },
          { name: 'Sentences', value: 'sentences' },
          { name: 'Lorem Ipsum', value: 'lorem_ipsum' },
          { name: 'Quotes', value: 'quotes' },
        ))
      .addStringOption(option => option.setName('personalized_instructions')
        .setDescription('Special instructions for the lines (free text) - please use ||spoiler text|| for anything NSFW.')
        .setRequired(false))
      .addBooleanOption(option => option.setName('hide_number_completed')
        .setDescription('Set whether the number of lines completed should be hidden')
        .setRequired(false))
      .addStringOption(option => option.setName('random_case')
        .setDescription('Set whether the typist should have to type the lines WiTH rAndOME CapiTaLIZaTIon')
        .setRequired(false)
        // When xkcdoer first implemented random case they wrote a bug that made it so that
        // when assigning random lines, random_case only affects the line after a
        // mistake. Buggy mode reenables this bug. If lines aren't random, it will
        // also only randomize on errors.
        //
        // The way this works is simple(-ish), but quite confusing.  There are two
        // if statements that branch on this value. The one that determines if the
        // line after a correctly typed line is not taken with the value "buggy",
        // but the one after an incorrectly typed line is.
        .addChoices(
          {name: 'rANdoM CAsE', value: 'random'},
          {name: 'Normal case', value: 'normal'},
          {name: 'Buggy', value: 'buggy'},
        ))
      .addStringOption(option => option.setName('spectator')
        .setDescription('Controls who can view and speak in the channel.')
        .setRequired(false)
        .addChoices(
          { name: 'Off - Just you and the person doing lines', value: 'off' },
          { name: 'Viewing - everyone can see the channel', value: 'viewing' },
          { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
        ))
      .addIntegerOption(option => option.setName('penalty_lines')
        .setDescription('The number of extra lines the user should type for making a mistake.')
        .setRequired(false))
  );

export async function execute(interaction, bot) {
  const subcommand = interaction.options.getSubcommand();
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const lines_todo = interaction.options.getInteger('num_lines');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const command_channel = interaction.channel.id;
  const hide_number_completed = interaction.options.getBoolean('hide_number_completed');
  const random_case = interaction.options.getString('random_case');
  const penalty_lines = interaction.options.getInteger('penalty_lines');
  const spectator = interaction.options.getString('spectator') || 'off';
  let should_hide_completed = false;

  if (hide_number_completed) should_hide_completed = true;

  const lines = await bot.lines.forUser(to_user.id);
  const senderLines = await bot.lines.forUser(sender.id);

  // check to see if user is already assigned lines
  if (senderLines.hasLines()) {
    await senderLines.addLinesToDo(lines_todo);
    let reply = `***<@${sender.id}> has given <@${to_user.id}> lines to write!***\n` +
      'Hey! Wait a minute!!! Shouldn\'t you be doing your *own* lines???\n';
    if (to_user.id !== sender.id) {
      reply += `**Never mind** <@${to_user.id}>, I'm just going to add ${lines_todo} to <@${sender.id}>'s lines.\n\n`;
    } else {
      reply += `${lines_todo} have been added to your lines to do!\n\n`;
    }
    reply += 'Stop screwing around and **get to it!!**';
    await interaction.reply(reply);
    return;
  }

  let line = interaction.options.getString('line');
  let randomLineType = false;
  if (subcommand === 'set' &&
    line !== null && (
      line.startsWith('/') || // line is a slash command
      line.includes('<@') || // tags another user
      line.toLowerCase() === 'cancel' ||
      lines.isCopyPasting(line))
    ) {
    await interaction.reply({
      content: 'You cannot assign someone that line. Try another one.',
      ephemeral: true
    });
    return;
  } else {
    randomLineType = interaction.options.getString('type');
  }

  if (await punishMassPings(interaction, line, personalized_instructions)) return;

  if (await backfires(interaction.member, to_user)) {
    await interaction.deferReply();
    let reply = '# Backfire!\n';
    reply += `***<@${sender.id}> tried to give <@${to_user.id}> lines to write but it backfired!***\n`;

    await senderLines.assignLines({
      setByUserId: sender.id,
      linesTodo: lines_todo,
      line: line,
      channelIdWhereAssigned: command_channel,
      hideNumberCompleted: should_hide_completed,
      randomCase: random_case,
      penaltyLines: penalty_lines,
      spectatorSetting: spectator,
      randomLineType,
    });
    reply += senderLines.linesAssignedMessage;
    if (personalized_instructions) {
      reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
    }
    await interaction.editReply(reply);

    return;
  }

  if (!await topHasConsent(bot, sender, to_user)) {
    await interaction.reply({
      content: 'You do not have consent to set lines for that user.',
      ephemeral: true
    });
    return;
  }

  if (lines.hasLines()) {
    await lines.addLinesToDo(lines_todo);
    let reply = `***<@${to_user.id}> already has lines!***\n`;
    await interaction.reply(reply);
    return;
  }

  await interaction.deferReply();

  let reply = `***<@${sender.id}> has given <@${to_user.id}> lines to write!***\n`;

  await lines.assignLines({
    setByUserId: sender.id,
    linesTodo: lines_todo,
    line: line,
    channelIdWhereAssigned: command_channel,
    hideNumberCompleted: should_hide_completed,
    randomCase: random_case,
    penaltyLines: penalty_lines,
    spectatorSetting: spectator,
    randomLineType,
  });
  reply += lines.linesAssignedMessage;
  if (personalized_instructions) {
    reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
  }
  await interaction.editReply(reply);
}
