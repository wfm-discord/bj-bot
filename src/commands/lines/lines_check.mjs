import { SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('lines_check')
  .setDescription('Check a user\'s lines status')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to check the lines status of')
    .setRequired(true));

export async function execute(interaction, bot) {
  const user = interaction.options.getUser('user');
  const lines = await bot.lines.forUser(user.id);

  if (!lines.hasLines()) {
    await interaction.reply(`<@${user.id}> currently has no lines to write.`);
  } else {
    let lines_todo = `<@${user.id}> has ${lines.linesToDo} lines to write, `;

    let completed = lines.linesCompleted ?
      `${lines.linesCompleted} lines have been completed.\n` :
      `Nothing has been completed yet so there's still ${lines.linesToDo} lines to do.`;

    const left = lines.linesToDo - lines.linesCompleted;
    let left_msg = left === lines.linesToDo ? '' : `${left} lines left.`;

    await interaction.reply(lines_todo + completed + left_msg);
  }
}
