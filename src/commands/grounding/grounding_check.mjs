import { SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('grounding_check')
  .setDescription('Check a user\'s grounding status')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to check the grounding status of')
    .setRequired(true));

export async function execute(interaction, bot) {
  const user = interaction.options.getUser('user');
  const grounding = await bot.grounding.forUser(user.id);
  await interaction.reply(grounding.statusMessage);
}
