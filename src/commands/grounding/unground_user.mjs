import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('unground_user')
  .setDescription('Ungrounds a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to unground')
    .setRequired(true))
  .addIntegerOption(option => option.setName('minutes')
    .setDescription('The number of minutes to update the grounding for')
    .setRequired(false))
  .addIntegerOption(option => option.setName('minimum_session')
    .setDescription('The minimum session duration in minutes')
    .setRequired(false))
  .addNumberOption(option => option.setName('completion_percentage')
    .setDescription('The required percentage of completion')
    .setRequired(false))
  .addNumberOption(option => option.setName('mistake_penalty')
    .setDescription('Extra minutes added for each missed or incorrect click.')
    .setRequired(false))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction, bot) {
  const to_user = interaction.options.getUser('user');
  const minutes_todo = interaction.options.getInteger('minutes');
  const minutes_minimum = interaction.options.getInteger('minimum_session');
  const percentage = interaction.options.getNumber('completion_percentage');
  const mistake_penalty = interaction.options.getNumber('mistake_penalty') || 0;
  const grounding = await bot.grounding.forUser(to_user.id);

  // make sure minimum is 12 or less
  if (!grounding.isMinutesMinimumOkay(minutes_minimum)) {
    interaction.reply({content: grounding.minimumMinutesMessage, ephemeral: true});
    return;
  }

  // user found, proceed with ungrounding
  if (minutes_todo > 0 || minutes_minimum || percentage || mistake_penalty) {
    if (typeof minutes_todo === 'undefined'
      && typeof minutes_minimum === 'undefined'
      && typeof percentage === 'undefined'
      && typeof mistake_penalty === 'undefined'
    ) {
      await interaction.reply({content: 'Please enter a value for at least one field to make an update.', ephemeral: true});
      return;
    }

    await grounding.overwrite({
      minutesToDo: minutes_todo,
      minutesMinimum: minutes_minimum,
      percentage: percentage,
      mistakePenalty: mistake_penalty
    });

    interaction.reply({
      content: `Updated grounding for ${to_user}.`,
      embeds: [grounding.getGroundingEmbed()],
      ephemeral: true
    });
  } else {
    // Clear grounding
    await grounding.end();
    await interaction.reply(`Removed grounding for ${to_user}.`);
  }
}
