import { SlashCommandBuilder } from 'discord.js';
import _ from 'lodash';

export const data = new SlashCommandBuilder()
  .setName('ground_myself_randomly')
  .setDescription('Grounds yourself for some amount of time')
  .addIntegerOption(option => option.setName('minutes')
    .setDescription('The maximum amount of time you\'ll tolerate in minutes (default 60).')
    .setRequired(false))
  .addIntegerOption(option => option.setName('minimum_session')
    .setDescription('The minimum session duration in minutes (will randomize if not set)')
    .setRequired(false))
  .addNumberOption(option => option.setName('completion_percentage')
    .setDescription('The required percentage of completion (will randomize if not set)')
    .setRequired(false))
  .addNumberOption(option => option.setName('mistake_penalty')
    .setDescription('Extra minutes added for each missed or incorrect click. (will randomize if not set)')
    .setRequired(false))
  .addStringOption(option => option
    .setName('secret_settings')
    .setDescription('Choose if the settings should be hidden or public.')
    .addChoices(
      { name: 'Settings Not Hidden', value: 'normal' },
      { name: 'Settings are a Secret!', value: 'secret' }
    )
  );

export async function execute(interaction, bot) {
  const sender = interaction.user;
  let minutes_max = interaction.options.getInteger('minutes') || 60;
  let minutes_minimum = interaction.options.getInteger('minimum_session');
  let percentage = interaction.options.getNumber('completion_percentage');
  let mistake_penalty = interaction.options.getNumber('mistake_penalty') || Math.floor(Math.random() * 6);
  const isSecret = interaction.options.getString('secret_settings') === 'secret';
  const grounding = await bot.grounding.forUser(sender.id);

  // make sure minimum is 12 or less
  if (!grounding.isMinutesMinimumOkay(minutes_minimum)) {
    interaction.reply({content: grounding.minimumMinutesMessage, ephemeral: true});
    return;
  }

  // generate random values if not provided
  if (!minutes_minimum) {
    minutes_minimum = _.random(1, 12, false);
  }
  if (!percentage) {
    percentage = _.random(1, 100, false);
  }

  const minutes_todo = _.random(1, minutes_max, false);

  await grounding.start({
    minutesToDo: minutes_todo,
    minutesMinimum: minutes_minimum,
    percentage: percentage,
    mistakePenalty: mistake_penalty,
    secret: isSecret,
  });
  const embed =  grounding.getGroundingEmbed();

  if (isSecret) {
    await interaction.reply({
      content: 'Your grounding has been created.',
      ephemeral: true
    });
    await interaction.followUp({
      content: '🎲 **Random Grounding!** 🎲',
      embeds: [embed]
    });
  } else {
    await interaction.reply({
      content: '🎲 **Random Grounding!** 🎲',
      embeds: [embed]
    });
  }
}
