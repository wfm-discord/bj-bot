import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {backfires} from '../../utils/backfire.mjs';
import _ from 'lodash';

export const data = new SlashCommandBuilder()
  .setName('ground_user')
  .setDescription('Grounds a user')
  .addSubcommand(subcommand =>
    subcommand.setName('direct')
      .setDescription('Grounds a user')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to ground')
        .setRequired(true))
      .addIntegerOption(option => option.setName('minutes')
        .setDescription('The number of minutes to ground the user for')
        .setRequired(true))
      .addStringOption(option => option.setName('personalized_instructions')
        .setDescription('Special instructions for the grounding (free text) - please use ||spoiler text|| for anything NSFW.')
        .setRequired(false))
      .addStringOption(option => option.setName('instruction_presets')
        .setDescription('Special instructions for the grounding (preset choices)')
        .setRequired(false)
        .addChoices(
          {name: 'Kneel while completing', value: 'Kneel while completing this grounding.'},
          {name: 'Run as surprise me', value: 'Run the groundings as "surprise me" sessions until you\'ve completed this grounding.'},
          {name: 'Shame yourself after grounding', value: 'When your grounding is over, shame yourself and count your way out.'},
          {name: 'Shame after grounding + 1s slowdown for every miss.', value: 'After the grounding, give yourself a slowdown of 1s/miss. Shame yourself. Count out of the corner.'},
          {name: 'Shame for every miss.', value: 'After the grounding, shame yourself once for each miss. Count out of the corner each time.'},
        ))
      .addIntegerOption(option => option.setName('minimum_session')
        .setDescription('The minimum session duration in minutes')
        .setRequired(false))
      .addNumberOption(option => option.setName('completion_percentage')
        .setDescription('The required percentage of completion')
        .setRequired(false))
      .addNumberOption(option => option.setName('mistake_penalty')
        .setDescription('Extra minutes added for each missed or incorrect click.')
        .setRequired(false))
      .addStringOption(option => option
        .setName('secret_settings')
        .setDescription('Choose if the settings should be hidden or public.')
        .addChoices(
          { name: 'Settings Not Hidden', value: 'normal' },
          { name: 'Settings are a Secret!', value: 'secret' }
        )
        .setRequired(false))
  )
  .addSubcommand(subcommand =>
    subcommand.setName('random')
      .setDescription('Grounds someone for a random amount of time')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to ground')
        .setRequired(true))
      .addIntegerOption(option => option.setName('min_minutes')
        .setDescription('The minimum amount of time in minutes (default 1).')
        .setRequired(false))
      .addIntegerOption(option => option.setName('min_minimum_session')
        .setDescription('The minimum minimum session duration in minutes (default 0).')
        .setRequired(false))
      .addNumberOption(option => option.setName('min_completion_percentage')
        .setDescription('The minimum required percentage of completion (default 0).')
        .setRequired(false))
      .addIntegerOption(option => option.setName('max_minutes')
        .setDescription('The maximum amount of time in minutes (default 60).')
        .setRequired(false))
      .addIntegerOption(option => option.setName('max_minimum_session')
        .setDescription('The maximum minimum session duration in minutes (default 12).')
        .setRequired(false))
      .addNumberOption(option => option.setName('max_completion_percentage')
        .setDescription('The maximum required percentage of completion (default 100).')
        .setRequired(false))
      .addNumberOption(option => option.setName('mistake_penalty')
        .setDescription('Extra minutes added for each missed or incorrect click. (will randomize if not set).')
        .setRequired(false))
      .addStringOption(option => option.setName('secret_settings')
        .setDescription('Choose if the settings should be hidden or public (will randomize if not set).')
        .addChoices(
          { name: 'Settings Not Hidden', value: 'normal' },
          { name: 'Settings are a Secret!', value: 'secret' }
        )
        .setRequired(false))
  );

export async function execute(interaction, bot) {
  const to_user = interaction.options.getUser('user');
  const grounding = await bot.grounding.forUser(to_user.id);

  const subcommand = interaction.options.getSubcommand();

  let minutes_todo;
  let minutes_minimum;
  let percentage;
  let mistake_penalty;

  if (subcommand === 'direct') {
    minutes_todo = interaction.options.getInteger('minutes');
    minutes_minimum = interaction.options.getInteger('minimum_session') || 0;
    percentage = interaction.options.getNumber('completion_percentage') || 0;
    mistake_penalty = interaction.options.getNumber('mistake_penalty') || 0;
  } else if (subcommand === 'random') {
    let min_minutes = interaction.options.getInteger('min_minutes');
    min_minutes = min_minutes > 0 ? min_minutes : 1;
    let max_minutes = interaction.options.getInteger('max_minutes');
    max_minutes = max_minutes > 0 ? max_minutes : 60;
    minutes_todo = _.random(min_minutes, max_minutes, false);

    let min_minimum_session = interaction.options.getInteger('min_minimum_session');
    min_minimum_session = min_minimum_session !== null && min_minimum_session <= 12 && min_minimum_session >= 0 ? min_minimum_session : 0;
    let max_minimum_session = interaction.options.getInteger('max_minimum_session')
    max_minimum_session = max_minimum_session !== null && max_minimum_session <= 12 && max_minimum_session >= 0 ? max_minimum_session : 12;
    minutes_minimum = _.random(min_minimum_session, max_minimum_session, false);

    let min_percentage = interaction.options.getNumber('min_completion_percentage');
    min_percentage = min_percentage !== null && min_percentage >= 0 && min_percentage <= 100 ? min_percentage : 0;
    let max_percentage = interaction.options.getNumber('max_completion_percentage');
    max_percentage = max_percentage !== null && max_percentage >= 0 && max_percentage <= 100 ? max_percentage : 100;
    percentage = _.random(min_percentage, max_percentage, false);

    mistake_penalty = interaction.options.getNumber('mistake_penalty') || Math.floor(Math.random() * 6);
  }

  // make sure minimum is 12 or less
  if (!grounding.isMinutesMinimumOkay(minutes_minimum)) {
    interaction.reply({content: grounding.minimumMinutesMessage, ephemeral: true});
    return;
  }

  const sender = interaction.user;
  let instructions = interaction.options.getString('instruction_presets');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const isSecret = interaction.options.getString('secret_settings') === 'secret';

  let pFlag = false;
  if (!!personalized_instructions) {
    pFlag = true;
    instructions = personalized_instructions;
  }

  if (await backfires(interaction.member, to_user)) {
    const backfireGrounding = await bot.grounding.forUser(sender.id);
    if (isSecret || (backfireGrounding.isGrounded() && backfireGrounding.isSecret())) {
      await interaction.deferReply({ephemeral: true});
    } else {
      await interaction.deferReply({ephemeral: false});
    }

    // update the brain accordingly and reply with what's going on
    let reply = '# Backfire!\n';
    reply += `***<@${sender.id}> tried to give <@${to_user.id}> a grounding but it backfired!***\n`;


    await backfireGrounding.start({
      minutesToDo: minutes_todo,
      minutesMinimum: minutes_minimum,
      percentage: percentage,
      mistakePenalty: mistake_penalty,
      instructions: instructions,
      personalizedInstructions: pFlag,
      secret: isSecret,
    });
    const embed =  backfireGrounding.getGroundingEmbed();

    await sendReply(reply, isSecret, interaction, embed);
    return;
  }

  // check consent
  if (!await topHasConsent(bot, sender, to_user)) {
    interaction.reply({
      content: 'You do not have consent to ground that user.',
      ephemeral: true
    });
    return;
  }

  if (isSecret || (grounding.isGrounded() && grounding.isSecret())) {
    await interaction.deferReply({ephemeral: true});
  } else {
    await interaction.deferReply({ephemeral: false});
  }

  // update the brain accordingly and reply with what's going on
  const reply = `***<@${sender.id}> has given <@${to_user.id}> a grounding!***\n`;

  await grounding.start({
    minutesToDo: minutes_todo,
    minutesMinimum: minutes_minimum,
    percentage: percentage,
    mistakePenalty: mistake_penalty,
    instructions: instructions,
    personalizedInstructions: pFlag,
    secret: isSecret,
  });

  let embed;
  let senderEmbed = null; // Only used for 'random' subcommand with 'secret' setting
  if (subcommand === 'direct') {
    embed = grounding.getGroundingEmbed();
  } else if (subcommand === 'random') {
    embed = grounding.getGroundingEmbed(grounding.isSecret());
    if (grounding.isSecret()) {
      senderEmbed = grounding.getGroundingEmbed(false);
    }
  }
  await sendReply(reply, grounding.isSecret(), interaction, embed, senderEmbed);
}

async function sendReply(reply, isSecret, interaction, embed, senderEmbed = null) {
  if (isSecret) {
    await interaction.editReply({
      content: 'Your grounding has been created.',
      embeds: senderEmbed ? [senderEmbed] : [],
      ephemeral: true
    });
    await interaction.followUp({
      content: reply,
      embeds: [embed]
    });
  } else {
    await interaction.editReply({
      content: reply,
      embeds: [embed]
    });
  }
}
