import { SlashCommandBuilder } from 'discord.js';
import {joinLottery} from '../../utils/lottery.mjs';

export const data = new SlashCommandBuilder()
  .setName('join_lottery')
  .setDescription('Join the lottery and test your luck!')

export async function execute(interaction) {
  await joinLottery(interaction, null)
}
