import { SlashCommandBuilder } from 'discord.js';
import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {hasBackfirableRole} from '../utils/backfire.mjs';

export const data = new SlashCommandBuilder()
  .setName('backfire_opt_in')
  .setDescription('Opt in to backfiring commands')
  .addStringOption(option =>
    option.setName('options')
      .setDescription('Options for your opt-in')
      .setRequired(false)
      .addChoices(
        { name: 'Cancel Opt-in', value: 'cancel' },
      ))
;

export async function execute(interaction) {
  const options = interaction.options.getString('options');
  await ensureBrainKeyExistsPromise('backfire_opt_in');

  if (await hasBackfirableRole(interaction.member)) {
    interaction.reply('Oh honey, no. Typists don\'t get a choice here. \n' +
      'You\'ve got a chance for your commands to backfire as long as ' +
      'you\'re wearing that orange role.');
    return;
  }

  if (options === 'cancel') {
    // Remove user from backfire_opt_in if they exist
    if (brain.data.backfire_opt_in[interaction.user.id]) {
      delete brain.data.backfire_opt_in[interaction.user.id];
      await brain.write();
      await interaction.reply('Ugh, coward. You\'re no longer opted in for backfiring commands.');
    } else {
      await interaction.reply('You were not opted in for backfiring commands.');
    }
  } else {
    if (brain.data.backfire_opt_in[interaction.user.id]) {
      await interaction.reply('Don\'t worry! You\'re already opted in for backfiring commands.');
    } else {
      await ensureBrainKeyExistsPromise('backfire_opt_in', interaction.user.id);
      await brain.write();
      await interaction.reply('Ah! I see you like to live dangerously! ' +
        'You\'re opted in for backfiring commands now. Good luck!');
    }
  }
}
