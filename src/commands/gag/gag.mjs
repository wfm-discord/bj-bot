import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {backfires} from '../../utils/backfire.mjs';
import {punishMassPings} from '../../utils/security.mjs';
import {getGuildMemberByUserID} from 'bot-commons-utils/src/utils/users.mjs';

const TIMEOUT_MAX_MINUTES = 15;

export const data = new SlashCommandBuilder()
  .setName('gag')
  .setDescription('Uses the discord "timeout" feature to prevent a user for talking in the server (max 15 minutes).')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you want to gag')
    .setRequired(true))
  .addIntegerOption(option => option.setName('duration')
    .setDescription('The duration for timeout in minutes')
    .setRequired(true))
  .addStringOption(option => option.setName('reason')
    .setDescription('A fun reason')
    .setRequired(false))
;

export async function execute(interaction, bot) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const reason = interaction.options.getString('reason');
  let duration = interaction.options.getInteger('duration');
  await interaction.deferReply();

  if (await punishMassPings(interaction, reason)) return;

  // check if duration is valid
  if (duration > TIMEOUT_MAX_MINUTES) {
    await interaction.editReply({
      content: '15 minutes is the maximum time you can gag someone with this command.',
      ephemeral: true,
    });
    return;
  }

  if (await backfires(interaction.member, to_user)) {
    try {
      const gag = await bot.gag.forUser(interaction.user.id);
      await gag.newGag({gaggedByUser: interaction.user, reason: 'Backfire!', duration});
      let reply = '# Backfire!\n' +
        `${sender} tried to gag ${to_user} but it backfired and ` +
        `now ${sender} is gagged for ${duration} minute` +
        (duration > 1 ? 's' : '') +
        '!';
      await interaction.editReply(reply);
    } catch (error) {
      const member = await getGuildMemberByUserID(interaction.guild, to_user.id);
      console.error(`Failed to backfire gag ${member.displayName}: `, error);
      await interaction.editReply({
        content: `Failed to backfire gag ${member.displayName}. Please try again later.`
      });
    }
    return;
  }

  // check consent
  if (!await topHasConsent(bot, sender, to_user)) {
    await interaction.editReply({content: 'You do not have consent to gag that user.', ephemeral: true});
    return;
  }

  // time the user out here:
  try {
    const gag = await bot.gag.forUser(to_user.id);
    await gag.newGag({gaggedByUser: interaction.user, reason, duration});

    let message = `${sender} gagged ${to_user} for ${duration} minute` +
        (duration > 1 ? 's' : '') +
        '.';
    if (reason) {
      message += `\nReason: ${reason}`;
    }

    // Reply to the interaction
    await interaction.editReply({ content: message });
  } catch (error) {
    const member = await getGuildMemberByUserID(interaction.guild, to_user.id);
    console.error(`Failed to gag ${member.displayName}: `, error);
    await interaction.editReply({ content: `Failed to gag ${member.displayName}. Please try again later.` });
  }
}
