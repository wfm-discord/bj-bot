import {SlashCommandBuilder} from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {backfires} from '../../utils/backfire.mjs';
import {getGuildMemberByUserID} from 'bot-commons-utils/src/utils/users.mjs';

export const data = new SlashCommandBuilder()
  .setName('gag_bomb')
  .setDescription('Gag a random subset of active users.');

export async function execute(interaction, bot) {
  await interaction.deferReply(); // Defer the reply to ensure quick response time

  // Use the reusable editReplyOrSendNew function
  await editReplyOrSendNew(interaction, 'Looking for users...');

  const sender = interaction.user;

  if (await backfires(interaction.member, null)) {
    try {
      const gag = await bot.gag.forUser(sender.id);
      await gag.newGag({gaggedByUser: sender, reason: 'Backfire!', duration: 15});
      let reply = '# Gag Bomb Backfire!\n' +
        `${sender} tried to throw a gag bomb but it exploded on them!\n` +
        'They\'re now gagged for 15 minutes.';
      await interaction.followUp(reply);
    } catch (error) {
      const member = await getGuildMemberByUserID(interaction.guild, to_user.id);
      console.error(`Failed to backfire gag ${member.displayName}: `, error);
      await interaction.followUp({
        content: `Failed to backfire gag ${member.displayName}. Please try again later.`
      });
    }
    return;
  }

  const guild = interaction.guild;

  // Fetch all channels the bot has access to
  const channels = guild.channels.cache.filter(channel => channel.isTextBased());

  // Get the timestamp for 20 minutes ago
  const twentyMinutesAgo = Date.now() - 20 * 60 * 1000;

  let activeUsers = new Set();

  for (const channel of channels.values()) {
    try {
      const messages = await channel.messages.fetch({ limit: 100 });
      messages.forEach(message => {
        if (message.createdTimestamp > twentyMinutesAgo) {
          activeUsers.add(message.author);
        }
      });
    } catch (error) {
      console.error(`Failed to fetch messages from channel ${channel.id}: `, error);
    }
  }

  activeUsers = [...activeUsers]; // Convert Set to Array

  if (activeUsers.length === 0) {
    await editReplyOrSendNew(interaction, {
      content: 'No active users found.',
      ephemeral: false,
    });
    return;
  }

  await editReplyOrSendNew(interaction, 'Checking for consent...');

  // Check for consent and filter users accordingly
  const consentedUsers = [];
  for (const user of activeUsers) {
    if (await topHasConsent(bot, sender, user)) {
      consentedUsers.push(user);
    }
  }

  if (consentedUsers.length === 0) {
    await editReplyOrSendNew(interaction, {
      content: 'No active users with consent found.',
      ephemeral: false,
    });
    return;
  }

  await editReplyOrSendNew(interaction, 'Tick tick tick tick...');

  // Determine the users to be gagged
  let usersToGag;
  if (consentedUsers.length <= 5) {
    usersToGag = consentedUsers;
  } else {
    const numberToGag = Math.ceil(consentedUsers.length * 0.7);
    usersToGag = consentedUsers.sort(() => 0.5 - Math.random()).slice(0, numberToGag);
  }

  // Gag each user for a random duration between 2 and 15 minutes
  const gaggedUsers = [];
  for (const user of usersToGag) {
    const duration = Math.floor(Math.random() * 14) + 2; // Random duration between 2 and 15 minutes
    try {
      const gag = await bot.gag.forUser(user.id);
      let gagged = await gag.newGag({gaggedByUser: sender, reason: 'Gag Bomb!', duration});
      if (gagged) {
        gaggedUsers.push({ user, duration });
      }
    } catch (error) {
      console.error(`Failed to gag ${user.username}: `, error);
    }
  }

  // Respond with the results
  if (gaggedUsers.length > 0) {
    let reply = `# Gag Bomb!\n${sender} set off a gag bomb. The following users have been gagged:\n`;
    for (const { user, duration } of gaggedUsers) {
      reply += `* ${user} for ${duration} minutes\n`;
    }
    await interaction.followUp(reply);
  } else {
    await interaction.followUp({
      content: 'Failed to gag any users.',
      ephemeral: false,
    });
  }
}

async function editReplyOrSendNew(interaction, content) {
  try {
    // Try editing the original reply
    await interaction.editReply(content);
  } catch (error) {
    if (error.code === 10008) { // Message Not Found
      const newMessage = await interaction.followUp(content);
      interaction.editReply = (newContent) => newMessage.edit(newContent); // Update editReply for subsequent calls
      return newMessage;
    } else {
      throw error; // Re-throw if it's a different error
    }
  }
}
