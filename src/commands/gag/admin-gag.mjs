import {PermissionFlagsBits, SlashCommandBuilder} from 'discord.js';
import { getGuildMemberByUserID } from 'bot-commons-utils/src/utils/users.mjs';

export const data = new SlashCommandBuilder()
  .setName('admin-gag')
  .setDescription('Admin command to gag a user without limitations or backfire.')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you want to gag')
    .setRequired(true))
  .addIntegerOption(option => option.setName('duration')
    .setDescription('The duration for timeout in minutes')
    .setRequired(true))
  .addStringOption(option => option.setName('reason')
    .setDescription('A fun reason')
    .setRequired(false))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction, bot) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const reason = interaction.options.getString('reason');
  const duration = interaction.options.getInteger('duration');

  await interaction.deferReply();

  // time the user out here:
  try {
    const gag = await bot.gag.forUser(to_user.id);
    await gag.newGag({gaggedByUser: interaction.user, reason, duration});

    let message = `${sender} admin-gagged ${to_user} for ${duration} minute${duration > 1 ? 's' : ''}.`;
    if (reason) {
      message += ` Reason: ${reason}`;
    }

    // Reply to the interaction
    await interaction.editReply({ content: message });
  } catch (error) {
    const member = await getGuildMemberByUserID(interaction.guild, to_user.id);
    console.error(`Failed to gag ${member.displayName}: `, error);
    await interaction.editReply({ content: `Failed to gag ${member.displayName}. Please try again later.` });
  }
}
