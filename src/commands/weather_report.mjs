import {SlashCommandBuilder} from 'discord.js';
import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';
import {WEATHER_PATTERNS} from '../utils/backfire.mjs';

export const data = new SlashCommandBuilder()
  .setName('weather_report')
  .setDescription('Check the current backfire weather. Are you safe or should you take cover?');

export async function execute(interaction) {
  await interaction.deferReply();
  const weather = ensureBrainKeyExists('weather');
  if (weather.report) {
    await interaction.editReply(weather.report);
    return;
  }

  // 30% chance of a bad weather report
  const isAccurate = Math.random() < 0.7;

  if (isAccurate) {
    weather.report = formatWeatherReport(weather.description, weather.chance);
  } else {
    // Choose a random weather to report instead
    const randomWeatherIndex = Math.floor(Math.random() * WEATHER_PATTERNS.length);
    const pattern = WEATHER_PATTERNS[randomWeatherIndex];
    weather.report = formatWeatherReport(pattern.description, pattern.chance);
  }
  await brain.write();
  await interaction.editReply(weather.report);
}

function formatWeatherReport(description, chance) {
  let percent = (chance * 100).toFixed(0);
  return `## ${description}\n*There is currently a ${percent}% chance of commands backfiring.*`;
}
