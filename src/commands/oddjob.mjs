import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';
import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';
import { wfmUtils } from '../utils/wfm.mjs';
import { isUserInGuild } from 'bot-commons-utils/src/utils/users.mjs';

export const data = new SlashCommandBuilder()
  .setName('oddjob')
  .setDescription('Assigns a random public task from people on this server who set their WFM profile.')
  .addIntegerOption(option =>
    option.setName('duration')
      .setDescription('How many minutes you want to hold onto the "Hard At Work" role (default 15, max 180 minutes)')
      .setRequired(false))
  .addStringOption(option =>
    option.setName('penalty_lines')
      .setDescription('Select the amount of penalty lines you are willing to accept for the task')
      .setRequired(false)
      .addChoices(
        { name: '0 penalty lines', value: '0' },
        { name: '0-1 penalty lines', value: '0-1' },
        { name: 'Any number (default)', value: 'any' } // Default
      ));

export async function execute(interaction) {
  await interaction.deferReply();
  const profiles = await ensureBrainKeyExistsPromise('wfm_profiles');
  const typing_role = process.env.CURRENTLY_TYPING_ROLE_ID;
  const penaltyLinesOption = interaction.options.getString('penalty_lines', false) || 'any';

  async function findTask(profiles, penaltyLinesOption, attempts = 0) {
    if (attempts >= 25) {
      throw new Error('No tasks available right now.');
    }

    const userIds = Object.keys(profiles);
    const randomUserId = userIds[Math.floor(Math.random() * userIds.length)];

    const isInGuild = await isUserInGuild(interaction.guild, randomUserId);
    if (!isInGuild) {
      // user isn't in the guild so loop back to this recursive function to look again
      return await findTask(profiles, penaltyLinesOption, attempts + 1);
    }

    try {
      const wfmUserId = profiles[randomUserId].split('/').pop(); // Extracting WFM User ID from the URL
      const tasksResponse = await wfmUtils.getUserTasks(wfmUserId);
      const tasksData = await tasksResponse.json();

      if (!tasksData || tasksData.totalCount === 0) {
        return await findTask(profiles, penaltyLinesOption, attempts + 1);
      }

      if (penaltyLinesOption === 'any') {
        const task = tasksData.tasks[Math.floor(Math.random() * tasksData.tasks.length)];
        return { userId: randomUserId, task: task };
      } else {
        // Randomize the tasks list first so I don't have to hit the API for every task
        const shuffledTasks = tasksData.tasks.sort(() => 0.5 - Math.random());

        for (const task of shuffledTasks) {
          const taskId = task._id;
          const details = await wfmUtils.getTaskDetails(taskId);
          const taskDetails = await details.json();

          let meetsCriteria = false;
          if (penaltyLinesOption.includes('-')) {
            const [min, max] = penaltyLinesOption.split('-').map(Number);
            if (taskDetails.punishmentLines >= min && taskDetails.punishmentLines <= max) {
              meetsCriteria = true;
            }
          } else {
            const exact = Number(penaltyLinesOption);
            if (taskDetails.punishmentLines === exact) {
              meetsCriteria = true;
            }
          }

          if (meetsCriteria) {
            return { userId: randomUserId, task: task };
          }
        }

        // If no task meets the criteria after checking the randomized list, try again
        return await findTask(profiles, penaltyLinesOption, attempts + 1);
      }
    } catch (error) {
      console.error('Error fetching tasks:', error);
      return await findTask(profiles, penaltyLinesOption, attempts + 1);
    }
  }

  try {
    const { userId, task } = await findTask(profiles, penaltyLinesOption);
    const userMember = interaction.guild.members.cache.get(interaction.user.id);

    let duration = interaction.options.getInteger('duration');
    if (duration === null) duration = 15; // default duration
    duration = duration > 180 ? 180 : duration; // cap the duration to 180 minutes

    if (duration > 0) {
      await addRole(duration, userMember);

      async function addRole(duration, userMember) {
        if (duration > 0) {
          if (userMember.roles.cache.has(typing_role)) {
            await interaction.editReply({ content: 'You already have the typing role!', ephemeral: true });
            return;
          }

          await userMember.roles.add(typing_role);

          const hard_at_work = await ensureBrainKeyExistsPromise('hard_at_work', interaction.user.id);
          let timeout = duration * 60000; // convert minutes to milliseconds
          hard_at_work.timeout = timeout;
          hard_at_work.end = new Date().getTime() + timeout;

          await brain.write();
        }
      }
    }

    const taskId = task._id;
    const details = await wfmUtils.getTaskDetails(taskId);
    let taskDetails = await details.json();

    // Check if the channel or its parent category is marked as NSFW
    const isNSFW = interaction.channel.nsfw || (interaction.channel.parent && interaction.channel.parent.nsfw);
    const blindFlight = taskDetails.blindFlight;

    // Fetch Discord user who created the task
    const discordMember = interaction.guild.members.cache.get(userId);

    // Create the embed
    const embed = new EmbedBuilder()
      .setColor(0xC83F17)
      .setURL(`https://writeforme.org/task/${taskId}`)
      .setTimestamp()
      .addFields(
        { name: 'Task Creator', value: taskDetails.creator.name, inline: true },
        { name: 'Discord User', value: discordMember ? discordMember.displayName : 'Unknown User', inline: true }
      )
      .setFooter({ text: `Task ID: ${taskId}` });


    if (isNSFW) {
      embed.setTitle(taskDetails.name)
        .setDescription(taskDetails.text);
    } else {
      embed.setTitle('||' + taskDetails.name + '||')
        .setDescription('||' + taskDetails.text + '||')
        .addFields({
          name: 'Why is this spoilered?',
          value: 'Because this isn\'t a NSFW room and there\'s no promise the task is SFW text.'
        });
    }

    // Adding totalLines and punishmentLines based on blindFlight
    if (!blindFlight) {
      embed.addFields(
        { name: 'Total Lines', value: String(taskDetails.totalLines), inline: true },
        { name: 'Punishment Lines', value: String(taskDetails.punishmentLines), inline: true }
      );
    } else {
      embed.addFields(
        { name: 'Total Lines', value: 'Until you are done.', inline: true },
        { name: 'Punishment Lines', value: '?', inline: true }
      );
    }

    embed.addFields(
      { name: 'Link', value: `https://writeforme.org/task/${taskId}`, inline: false }
    );

    let content = `Oh you're looking for work? Great! I've found some work for you!`;
    if (duration > 0) {
      const minuteText = duration === 1 ? 'minute' : 'minutes';
      content += `\nWork hard for ${duration} ${minuteText}! Or you know... as long as it takes :smirk:`;
    }

    // Reply with the embed
    await interaction.editReply({ content: content, embeds: [embed] });

  } catch (error) {
    console.error('Error:', error);
    await interaction.editReply('Unfortunately, there are no jobs available right now.');
  }
}
