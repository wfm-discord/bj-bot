import { SlashCommandBuilder } from '@discordjs/builders';
import {CommandInteraction, PermissionFlagsBits} from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('dnd_move_voice')
  .setDescription('Moves all users from the DND voice channel to another voice channel')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

/**
 * Executes the move_all command.
 * @param {Bot} bot
 * @param {CommandInteraction} interaction
 */
export async function execute(interaction, bot) {
  const guild = bot.guild;

  const dndChannel = guild.channels.cache.get(process.env.DND_VOICE_CHANNEL_ID);
  const targetChannel = guild.channels.cache.get(process.env.VOICE_CHANNEL_ID);



  if (!dndChannel || !targetChannel || dndChannel.type !== 2 || targetChannel.type !== 2) {
    return interaction.reply('One or both of the voice channels are invalid.');
  }

  const members = dndChannel.members;

  if (members.size === 0) {
    return interaction.reply('There are no members in the DND voice channel.');
  }

  members.forEach(member => {
    member.voice.setChannel(targetChannel)
      .then(() => console.log(`Moved ${member.user.tag} to the target channel.`))
      .catch(err => console.error(`Failed to move ${member.user.tag}:`, err));
  });

  return interaction.reply('All members have been moved.');
}
