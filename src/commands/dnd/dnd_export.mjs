// src/commands/dnd_export.mjs
import { SlashCommandBuilder, PermissionFlagsBits, AttachmentBuilder } from 'discord.js';
import { splitMessagesByTokenLimit, splitMessagesBySize } from '../../utils/messageExports.mjs';
import { writeFile } from 'node:fs/promises';
import { withFile } from 'tmp-promise';
import { gatherMessagesFromMultipleChannels } from 'bot-commons-utils/src/utils/messageExports.mjs';

export const data = new SlashCommandBuilder()
  .setName('dnd_export')
  .setDescription('Export messages from the transcription and discussion channels.')
  .addStringOption(option => option.setName('since_date')
    .setDescription('Export messages since the specified date. Format: YYYY-MM-DD')
    .setRequired(true)
  )
  .addIntegerOption(option => option.setName('split_limit')
    .setDescription('The limit for splitting the messages. Defaults to 2000 tokens or 20,480 KB.')
    .setRequired(false)
  )
  .addStringOption(option => option.setName('alternative_discussion_channel')
    .setDescription('Alternative channel ID to export with the transcription.')
    .setRequired(false)
  )
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  try {
    const client = interaction.client;
    const transcriptionChannel = await client.channels.fetch(process.env.TRANSCRIPTION_CHANNEL_ID);
    const alternativeChannelId = interaction.options.getString('alternative_discussion_channel');
    const discussionChannel = alternativeChannelId
      ? await client.channels.fetch(alternativeChannelId)
      : await client.channels.fetch(process.env.DND_VOICE_CHANNEL_ID);

    await interaction.deferReply({ ephemeral: true });
    const splitBy = 'size';
    const exportDate = interaction.options.getString('since_date');


    await interaction.editReply({ content: 'Please hold on. I am working on getting the messages together...'});
    const startDate = exportDate ? new Date(exportDate) : undefined;
    const channels = [transcriptionChannel, discussionChannel];
    const messages = await gatherMessagesFromMultipleChannels(channels, startDate);

    if (messages.length === 0) {
      await interaction.editReply({ content: 'There were no messages.' });
      return;
    }

    await interaction.editReply({ content: 'I have the messages I just need to form them into text files for you.'});

    let splitMessageText = [];
    const splitLimit = interaction.options.getInteger('split_limit');

    if (splitBy === 'tokens') {
      let tokenLimit = splitLimit;
      if (tokenLimit > (4096)) {
        tokenLimit = 4096;
      } else if (!tokenLimit) {
        tokenLimit = 2000;
      }
      splitMessageText = splitMessagesByTokenLimit(messages, tokenLimit, true);
    } else {
      let sizeLimit = splitLimit * 1024;
      if (!sizeLimit || sizeLimit > 20 * 1024 * 1024) {
        sizeLimit = 20 * 1024 * 1024;
      }
      splitMessageText = await splitMessagesBySize(messages, sizeLimit, true);
    }

    for (let i = 0; i < splitMessageText.length; i++) {
      await withFile(async ({ path }) => {
        const filePath = path;
        await writeFile(filePath, splitMessageText[i]);
        const fileAttachment = new AttachmentBuilder(filePath);

        const replyContent = `Here is the messages export file (${i + 1}/${splitMessageText.length}):`;
        if (i === 0) {
          await interaction.editReply({
            content: replyContent,
            files: [fileAttachment],
          });
        } else {
          await interaction.followUp({
            content: replyContent,
            files: [fileAttachment],
            ephemeral: true
          });
        }
      }, { postfix: `_dnd_export_${i + 1}.txt` });
    }
  } catch (error) {
    console.error(error);
    await interaction.editReply({
      content: 'An error occurred while exporting messages. Please check the bot logs for details.'
    });
  }
}
