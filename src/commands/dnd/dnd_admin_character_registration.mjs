import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import {brain} from 'bot-commons-utils/src/utils/brain.mjs';
import {ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';

// Define the slash command
export const data = new SlashCommandBuilder()
  .setName('dnd_admin_character_registration')
  .setDescription('Registers a D&D character for a user.')
  .addUserOption(option =>
    option
      .setName('user')
      .setDescription('Select a user')
      .setRequired(true)
  )
  .addStringOption(option =>
    option
      .setName('character_name')
      .setDescription('Enter the character name')
      .setRequired(true)
  )
  .addIntegerOption(option =>
    option
      .setName('character_id')
      .setDescription('Enter the character ID')
      .setRequired(true)
  )
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  const characterName = interaction.options.getString('character_name');
  const characterId = interaction.options.getInteger('character_id');

  await ensureBrainKeyExistsPromise('dnd_registration', user.id);
  brain.data.dnd_registration[user.id] = {
    user: user.id,
    character: characterName,
    id: characterId
  };
  await brain.write();

  await interaction.reply(`Registered ${characterName} with ID ${characterId} for ${user.username}.`);
}
