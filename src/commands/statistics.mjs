import { SlashCommandBuilder, EmbedBuilder } from 'discord.js';
import {ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {timeUtils} from 'bot-commons-utils/src/utils/time.mjs';
import _ from 'lodash';
import {AssignmentBase} from '../services/assignmentBase.mjs';

export const data = new SlashCommandBuilder()
  .setName('statistics')
  .setDescription('Get various statistics.')
  .addSubcommand(subcommand =>
    subcommand
      .setName('math')
      .setDescription('Get math statistics for a user')
      .addUserOption(option =>
        option.setName('user')
          .setDescription('Whose stats you want to see?')
          .setRequired(false)
      )
  )
  .addSubcommand(subcommand =>
    subcommand
      .setName('shape_count')
      .setDescription('Get shape counting statistics for a user')
      .addUserOption(option =>
        option.setName('user')
          .setDescription('Whose stats you want to see?')
          .setRequired(false)
      )
  );

export async function execute(interaction, bot) {
  const user = interaction.options.getUser('user');
  const userId = user ? user.id : interaction.user.id;

  if (interaction.options.getSubcommand() === 'math') {
    const tracking = await bot.stats.mathForUser(userId);
    if (!tracking.hasTracking()) {
      await interaction.reply({content: `<@${userId}> needs to do some math.`, ephemeral: true});
      return;
    }
    const percentage = tracking.percentage;
    const grade = AssignmentBase.calculateLetterGrade(percentage);
    const gradeText = percentage > 0 ?
      `# Overall Grade: ${grade}\n${percentage}%\n*Grades are based solely on math tests: correct problems / assigned problems.*` :
      '# Overall Grade: Incomplete\n*Start a math test to get a grade.*';

    const embed = new EmbedBuilder()
      .setColor(189729)
      .setTitle('Math Statistics')
      .setDescription(`Math statistics for ${user ? user.tag : interaction.user.tag}\n\n${gradeText}`)
      .addFields(
        getFieldData('Addition', tracking.getOperationData('addition')),
        getFieldData('Subtraction', tracking.getOperationData('subtraction')),
        getFieldData('Multiplication', tracking.getOperationData('multiplication')),
        getFieldData('Division', tracking.getOperationData('division')),
        getFieldData('Hard Mode', tracking.getOperationData('hard_mode')),
        getPracticeTestData('Practice', tracking.getModeData('practice')),
        getPracticeTestData('Test', tracking.getModeData('test')),
        getPracticeTestData('All', tracking.getModeData('all'))
      )
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: interaction.user.avatarURL() });

    await interaction.reply({ embeds: [embed] });
  } else if (interaction.options.getSubcommand() === 'shape_count') {
    const stats = await ensureBrainKeyExistsPromise('tracking', 'shape_count', userId);

    if (_.isEmpty(stats)) {
      await interaction.reply({content: `I do not have any shape counting stats for <@${userId}>`, ephemeral: true});
      return;
    }

    // Ensure stats.all.totalGuesses is not zero to avoid division by zero
    const successRate = stats.all.totalGuesses > 0
      ? (stats.all.successfulGuesses / stats.all.totalGuesses) * 100
      : 0;
    const grade = AssignmentBase.calculateLetterGrade(successRate);
    const gradeText = successRate > 0
      ? `Grade: ${grade} (${successRate.toFixed(2)}%)`
      : 'Grade: Incomplete';


    const shapeStatsEmbed = new EmbedBuilder()
      .setColor(189729)
      .setTitle('Shape Counting Statistics')
      .setDescription(`Shape counting statistics for ${user ? user.tag : interaction.user.tag}`)

      // Overall
      .addFields(getShapeCountFieldData('Overall \n' + gradeText, stats.all))
      .addFields({ name: '\u200B', value: '\u200B', inline: false }) // Separator

      // Difficulty
      .addFields(
        { name: 'Difficulty', value: '\u200B', inline: false }, // Header
        getShapeCountFieldData('Easy', stats.difficultyStats.easy),
        getShapeCountFieldData('Medium', stats.difficultyStats.medium),
        getShapeCountFieldData('Hard', stats.difficultyStats.hard)
      )
      .addFields({ name: '\u200B', value: '\u200B', inline: false }) // Separator

      // Size
      .addFields(
        { name: 'Size', value: '\u200B', inline: false }, // Header
        getShapeCountFieldData('X Small', stats.sizeStats.x_small),
        getShapeCountFieldData('Small', stats.sizeStats.small),
        getShapeCountFieldData('Medium', stats.sizeStats.medium),
        getShapeCountFieldData('Large', stats.sizeStats.large)
      )
      .addFields({ name: '\u200B', value: '\u200B', inline: false }) // Separator

      // Movement
      .addFields(
        { name: 'Movement', value: '\u200B', inline: false }, // Header
        getShapeCountFieldData('Still', stats.movementStats.still),
        getShapeCountFieldData('Blinking', stats.movementStats.fade)
      )
      .addFields({ name: '\u200B', value: '\u200B', inline: false }) // Separator

      // Color
      .addFields(
        { name: 'Color', value: '\u200B', inline: false }, // Header
        getShapeCountFieldData('Random', stats.colorStats.random),
        getShapeCountFieldData('Black', stats.colorStats.black),
        getShapeCountFieldData('White', stats.colorStats.white),
        getShapeCountFieldData('Rainbow', stats.colorStats.rainbow),
        getShapeCountFieldData('Pastel', stats.colorStats.pastel)
      )
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: interaction.user.avatarURL() });

    await interaction.reply({ embeds: [shapeStatsEmbed] });
  }
}

function getShapeCountFieldData(name, data) {
  return {
    name: name,
    value: `Total Guesses: ${data.totalGuesses}\n` +
      `Successful Guesses: ${data.successfulGuesses}\n` +
      `Failed Guesses: ${data.failedGuesses}\n` +
      `Total Time: ${timeUtils.msToTime(data.timeTaken * 60000)}\n` +
      `Average Time: ${data.averageTime.toFixed(2)} minutes`,
    inline: true
  };
}

function getFieldData(name, data) {
  return {
    name: name,
    value: `Started: ${data.problemsStarted}\n` +
      `Completed: ${data.problemsCompleted}\n` +
      `Correct: ${data.problemsCorrect}\n` +
      `Incorrect: ${data.problemsIncorrect}`,
    inline: true
  };
}

function getPracticeTestData(name, data) {
  return {
    name: name,
    value: `Started: ${data.assignmentsStarted}\n` +
      `Cancelled: ${data.assignmentsCancelled}\n` +
      `Completed: ${data.assignmentsCompleted}\n` +
      `Problems Assigned: ${data.problemsAssigned}\n` +
      `Problems Completed: ${data.problemsCompleted}\n` +
      `Problems Correct: ${data.problemsCorrect}\n` +
      `Problems Incorrect: ${data.problemsIncorrect}\n` +
      `Time Spent: ${timeUtils.msToTime(data.timeSpent)}`,
    inline: false
  };
}
