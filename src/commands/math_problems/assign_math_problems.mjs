import { SlashCommandBuilder } from 'discord.js';
import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';

export const data = new SlashCommandBuilder()
  .setName('assign_math')
  .setDescription('Assigns math problems to a user or gives times table problems')

  .addSubcommand(subcommand =>
    subcommand
      .setName('problems')
      .setDescription('Assigns general math problems')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to assign a problem to.')
        .setRequired(true))
      .addIntegerOption(option => option.setName('num_problems')
        .setDescription('The number of problems the user should solve.')
        .setRequired(true))
      .addStringOption(option => option.setName('operation')
        .setDescription('Limit to one operation for the math problems.')
        .setRequired(false)
        .addChoices(
          { name: 'Addition', value: 'addition' },
          { name: 'Subtraction', value: 'subtraction' },
          { name: 'Multiplication', value: 'multiplication' },
          { name: 'Division', value: 'division' },
          { name: 'Mixed Operations', value: 'mixed' },
          { name: 'Algebra', value: 'algebra' },
          { name: 'Factorials', value: 'factorial' },
          { name: 'Mean Median Mode', value: 'mean-median-mode' },
          { name: 'Any', value: 'all' }
        ))
      //.addBooleanOption(option => option.setName('hard_mode')
      //  .setDescription('Enable hard mode.')
      //  .setRequired(false))
      .addBooleanOption(option => option.setName('math_test')
        .setDescription('Take a math test! Wrong answers aren\'t immediately revealed.')
        .setRequired(false))
      .addStringOption(option => option.setName('spectator')
        .setDescription('Controls who can view and speak in the channel.')
        .setRequired(false)
        .addChoices(
          { name: 'Off - Just you and the person doing math', value: 'off' },
          { name: 'Viewing - everyone can see the channel', value: 'viewing' },
          { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
        ))
  )
  .addSubcommand(subcommand =>
    subcommand
      .setName('single_digit_times_table')
      .setDescription('Assign single digit times tables to a user')
      .addUserOption(option => option.setName('user')
        .setDescription('The user to assign times tables to.')
        .setRequired(true))
      .addBooleanOption(option => option.setName('math_test')
        .setDescription('Take a math test! Wrong answers aren\'t immediately revealed.')
        .setRequired(false))
      .addStringOption(option => option.setName('spectator')
        .setDescription('Controls who can view and speak in the channel.')
        .setRequired(false)
        .addChoices(
          { name: 'Off - Just you and the person doing math', value: 'off' },
          { name: 'Viewing - everyone can see the channel', value: 'viewing' },
          { name: 'Free Talking - everyone can talk in the channel', value: 'free-talking' }
        ))
  );


export async function execute(interaction, bot) {
  const subcommand = interaction.options.getSubcommand();
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const spectator = interaction.options.getString('spectator') || 'off';
  const test_mode = interaction.options.getBoolean('math_test');

  if (!await topHasConsent(bot, sender, to_user)) {
    await interaction.reply({
      content: 'You do not have consent to set problems for that user.',
      ephemeral: true
    });
    return;
  }

  await interaction.deferReply();

  const mathObject = await bot.math.forUser(to_user.id);

  if (mathObject.hasProblems()) {
    await interaction.editReply(`***<@${sender.id}> tried to assign more math to <@${to_user.id}>!***\n` +
      `<@${to_user.id}> already has problems assigned to them. These problems have not been assigned.`);
    return;
  }

  let reply = '';

  if (test_mode) {
    reply += '# Time for a Math Test!\n';
  }

  reply += `***<@${sender.id}> has given <@${to_user.id}>`;

  if (subcommand === 'problems') {
    const num_problems = interaction.options.getInteger('num_problems');
    const operation = interaction.options.getString('operation') || 'all';

    reply += ' math problems to solve!***\n';

    await mathObject.assignMath({
      setByUserId: sender.id,
      problemsTodo: num_problems,
      channelIdWhereAssigned: interaction.channel.id,
      operation: operation,
      testMode: test_mode,
      spectatorSetting: spectator
    });

    reply += `Problems to solve: ${num_problems}\n`;
  } else if (subcommand === 'single_digit_times_table') {
    reply += ' single digit times table problems to solve!***\n';

    await mathObject.assignMath({
      setByUserId: sender.id,
      channelIdWhereAssigned: interaction.channel.id,
      testMode: test_mode,
      setting: 'times_table',
      problemsTodo: 81,
      spectatorSetting: spectator
    });

    reply += 'Times Table Practice!\n';
  }

  reply += '\nA new channel has been created just for you to do your problems in.';
  await interaction.editReply(reply);
}
