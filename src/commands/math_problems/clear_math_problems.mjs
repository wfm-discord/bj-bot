import { PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('clear_math_problems')
  .setDescription('Manually clear a users math problems, will also delete their channel if they have one.')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you wish to clear.')
    .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction, bot) {
  const user = interaction.options.getUser('user');

  const mathObject = await bot.math.forUser(user.id);

  if (mathObject.hasProblems()) {
    await mathObject.cancelMathNow();
    await interaction.reply({content: 'Math problems cleared successfully', ephemeral: true});
  } else {
    await interaction.reply({content: 'That user has no math problems assigned.', ephemeral: true});
  }
}
