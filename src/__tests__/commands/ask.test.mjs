/* eslint-env jest */

import { execute } from '../../commands/ask.mjs';
import { getMessageMock } from '../../utils/mocks.mjs';

test('sends an embed with the user question when the ask command is executed', async () => {
  const interactionMock = getMessageMock();
  const question = 'Will it rain tomorrow?';
  interactionMock.options.getString.mockReturnValue(question);

  await execute(interactionMock);

  expect(interactionMock.reply).toHaveBeenCalledTimes(1);
  const sentEmbed = interactionMock.reply.mock.calls[0][0].embeds[0];
  expect(sentEmbed.data.title).toEqual('Prediction');
  expect(sentEmbed.data.description).toContain(question);
});
