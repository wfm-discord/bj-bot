// src/bot.mjs
import { config } from 'dotenv';
import { Client, GatewayIntentBits, Partials } from 'discord.js';
import path from 'node:path';

import {bot} from './services/bot.mjs';
import {ShameRepository} from "./services/shame.mjs";
import {StatisticsRepository} from './services/statistics.mjs';
import {initBrain, brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {ensureEnvironmentVariables} from 'bot-commons-utils/src/utils/env.mjs';
import {getRandomWeather, scheduleWeatherChange} from './utils/backfire.mjs';
import {ConsentRepository} from 'bot-commons-consent';
import {GagRepository} from './services/gag.mjs';
import {GroundingRepository} from './services/grounding.mjs';
import {LinesRepository} from './services/lines.mjs';
import {MathRepository} from './services/math.mjs';
import {Gag} from './services/gag.mjs';

import {
  registerCommands,
  registerEvents,
  registerScripts,
  registerCronJobs,
  loginBot,
  setupGracefulShutdown
} from 'bot-commons-utils/src/utils/botSetup.mjs';

await initBrain();
// Start of brain handling. Ensure things happen to the brain file that need to happen
// ensure restricted word list exists
await ensureBrainKeyExistsPromise('restricted_words');
// ensure necessary brain keys exist
// while the constructor was supposed to handle this, it didn't do it well, so it's been moved out
await ensureBrainKeyExistsPromise('plusPlus', 'scores');
await ensureBrainKeyExistsPromise('plusPlus', 'log');
await ensureBrainKeyExistsPromise('plusPlus', 'reasons');
await ensureBrainKeyExistsPromise('plusPlus', 'last');

await completeAnyMigrations();

async function completeAnyMigrations() {
  // put one-time migrations in here
  //await ensureBrainKeyExistsPromise('migrations');

  // after all migrations have been run and commented out, uncomment this
  // and comment out the migrations creation
  //
  if (brain.data.migrations) {
      delete brain.data.migrations;
      await brain.write();
  }
}

await brain.write();

// End of brain handling


// Define the bot's root directory
bot.botRoot = path.resolve('.');

await ensureEnvironmentVariables(bot); // Ensure all environment variables are set correctly
config(); //for dotenv

// Create a new client instance
bot.client = new Client({
  partials: [Partials.Message, Partials.Channel, Partials.Reaction],
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.GuildVoiceStates,
  ],
});

// Register all commands in the folder
await registerCommands(bot);
// Register events
await registerEvents(bot);
// Register scripts in the folder
await registerScripts(bot);
// Register all cron jobs in the folder
await registerCronJobs(bot);

// Initialize the weather when the bot starts up
await ensureBrainKeyExistsPromise('weather');
brain.data.weather = getRandomWeather();
brain.data.weather.report = null;
await brain.write();
await scheduleWeatherChange();

// Log bot into Discord, set Guild in Bot service
await loginBot(bot);
bot.logsChannel = await bot.client.channels.fetch(process.env.LOGS_ROOM);

bot.consent = new ConsentRepository(bot);
bot.gag = new GagRepository();
bot.grounding = new GroundingRepository();
bot.lines = new LinesRepository();
bot.math = new MathRepository();
bot.shame = new ShameRepository();
bot.stats = new StatisticsRepository();

// Register the ungag cron jobs
await Gag.scheduleAllGagCron();

// Stop the bot when the process is closed (via Ctrl-C).
setupGracefulShutdown(bot);
