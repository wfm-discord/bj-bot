import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import { EmbedBuilder } from 'discord.js';
import { bot } from '../services/bot.mjs';
import { wfmUtils } from './wfm.mjs';

export async function postRandomTask() {
  ensureBrainKeyExists('taskoftheday');

  const postTaskInternal = async (attemptedKeys = new Set()) => {
    let brainData = brain.data;
    let tasks = brainData.taskoftheday;

    let totalTasks = Object.keys(tasks).length;
    // Calculate the threshold of days to exclude (60% of total task count)
    let exclusionDaysThreshold = Math.ceil(totalTasks * 0.6);

    let taskKeys = Object.keys(tasks).filter(key => {
      let task = tasks[key];
      if (!task.last_posted_date) return true; // Always include tasks that haven't been posted yet

      let lastPostedDate = new Date(task.last_posted_date);
      let daysSinceLastPost = (new Date() - lastPostedDate) / (1000 * 60 * 60 * 24);
      return daysSinceLastPost > exclusionDaysThreshold && !attemptedKeys.has(key);
    });

    if (taskKeys.length === 0 || taskKeys.length === attemptedKeys.size) {
      await bot.logsChannel?.send('No more tasks available or all tasks have been attempted.');
      return;
    }

    let randomKey = taskKeys[Math.floor(Math.random() * taskKeys.length)];
    let task = tasks[randomKey];
    const { taskId, accessCode } = wfmUtils.getInfoFromUrl(task.url);

    try {
      const detailsResponse = await wfmUtils.getTaskDetails(taskId, accessCode);
      if (!detailsResponse.ok) {
        if (detailsResponse.status === 404 || detailsResponse.status === 410) { // Indicators for deleted or inaccessible tasks
          await bot.logsChannel?.send(`Task with ID ${taskId} is no longer accessible and has been removed.`);
          delete brainData.taskoftheday[randomKey];
          await brain.write();
          attemptedKeys.add(randomKey); // Mark this key as attempted
          await postTaskInternal(attemptedKeys); // Try the next task
          return;
        }
        // If API is down or unreachable, log and proceed with cached data
        console.log(`API down or unreachable for task ${taskId}, using cached data.`);
      } else {
        let taskDetails = await detailsResponse.json();
        // Update task details in the brain if the API is reachable and task exists
        task = {
          ...task,
          name: taskDetails.name,
          text: taskDetails.text,
          totalLines: taskDetails.totalLines,
          punishmentLines: taskDetails.punishmentLines,
          blindFlight: taskDetails.blindFlight,
        };
        brainData.taskoftheday[randomKey] = task;
      }
    } catch (error) {
      console.error(`Unexpected error fetching task details for ${taskId}: ${error}`);
      // Continue with cached data in case of unexpected errors
    }

    // Always update the last posted date and save brain data
    task.last_posted_date = new Date().toISOString().slice(0, 10);
    await brain.write();

    // Prepare and send the embed using either updated or cached task details
    const embed = prepareEmbed(task);
    const totdRoom = process.env.DAILY_TASK_ROOM;
    const channel = bot.client.channels.cache.get(totdRoom);
    if (channel) {
      await channel.send({ embeds: [embed] });
    } else {
      console.log("The task room is not properly configured.");
    }
  };

  await postTaskInternal();
}

function prepareEmbed(task) {
  const embed = new EmbedBuilder()
    .setColor('#c4000d')
    .setAuthor({ name: 'Task of the Day' })
    .setTitle(task.name || 'Task Details Unavailable')
    .setURL(task.url)
    .setDescription(`"${task.text}"`)
    .setFooter({ text: `Submitted by ${task.submitted_by}. \r\nIf you would like to add your task(s) to our task-of-the-day, please reach out to projectm.` });

  embed.addFields(
    { name: 'Link', value: `${task.url}`, inline: false }
  );

  const blindFlight = task.blindFlight;
  if (blindFlight !== undefined) {
    embed.addFields(
      { name: 'Total Lines', value: blindFlight ? 'Until you are done.' : String(task.totalLines), inline: true },
      { name: 'Punishment Lines', value: blindFlight ? '?' : String(task.punishmentLines), inline: true }
    );
  }

  if (task.notes) {
    embed.addFields(
      { name: 'Notes', value: task.notes || 'None', inline: false }
    );
  }

  return embed;
}
