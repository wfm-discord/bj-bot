// src/utils/restricted_words.mjs
import {PermissionsBitField, TextChannel} from 'discord.js';
import _ from 'lodash';
import {getGuildMemberByUserID} from 'bot-commons-utils/src/utils/users.mjs';
import {brain, brainKeyOrFalse} from 'bot-commons-utils/src/utils/brain.mjs';
import {topHasConsentByID} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {bot} from '../services/bot.mjs';

export const escapeArray = ['?', '\\', '.', '^', '$', '"', "'", '*', '|', '+', '!'];

export async function skipThisWord(word, user, channel) {
  // if not in scope or word is removed, return true to skip this word
  if (!await isInScope(word, user, channel) || await checkRestrictedWordForRemoval(user, word)) {
    return true;
  }
  // check for consent on this word, if no consent return true to skip this word
  const settings = brain.data.restricted_words[user.id][word];
  return !await topHasConsentByID(bot, settings.setterId, user.id);
}

async function isInScope(word, user, channel) {
  const settings = brain.data.restricted_words[user.id][word];
  // if everywhere, it's always in scope
  if (settings.scope === 'everywhere') return true;

  const ch = channel.id.toString();
  // if no scope, default is "most"
  if (!settings.scope || settings.scope === 'most') {
    // skip corner of shame
    if (ch === process.env.SHAME_ROOM.toString()) return false;

    // skip personal lines channel
    const lines = await bot.lines.forUser(user.id);
    if (lines.hasLines() && lines.inAssignmentChannel(ch)) return false;

    // skip personal math channel
    const math = brainKeyOrFalse('problems', user.id);
    if (math && math.channel && math.channel.toString() === ch) return false;
  }

  return true; // If none of the conditions are met, it's in scope
}

export async function checkRestrictedWordForRemoval(user, word) {
  const settings = brain.data.restricted_words[user.id][word];
  // remove expired restrictions
  if (settings.expirationDate && new Date(settings.expirationDate) <= new Date()) {
    delete brain.data.restricted_words[user.id][word];
    await brain.write();
    return true;
  }
  // remove restrictions that hit a violation goal
  if (settings.violationGoal && settings.violationGoal <= settings.penalty.penaltyCount) {
    delete brain.data.restricted_words[user.id][word];
    await brain.write();
    return true;
  }
  return false;
}

export function isValidWord(word) {
  const regex = /(?<!\\)(?:\\\\)*([()[\]])/g;

  let match;
  const pairs = { '(': ')', '[': ']' };
  const stack = [];

  while ((match = regex.exec(word)) !== null) {
    const char = match[1];
    if (pairs[char]) {
      stack.push(char);
    } else {
      const last = stack.pop();
      if (pairs[last] !== char) {
        return false;
      }
    }
  }

  return stack.length === 0;
}

export async function getRestrictedWordPenaltyType(guild, user, penaltyType) {
  const member = await getGuildMemberByUserID(guild, user.id);
  if (penaltyType === 'random') {
    let randomPenalties = ['gag', 'shame', 'grounding', 'lines', 'nothing'];
    // "gag" doesn't work on admin
    if (member.permissions.has(PermissionsBitField.Flags.Administrator)) {
      randomPenalties = ['shame', 'grounding', 'lines', 'nothing'];
    }
    penaltyType = _.sample(randomPenalties);
  } else if (member.permissions.has(PermissionsBitField.Flags.Administrator) && penaltyType === 'gag') {
    // "gag" doesn't work on admin
    penaltyType = 'nothing';
  }
  return penaltyType;
}

export function buildRegex(word, settings) {
  let regexPattern = settings.whole_word ? `\\b(${word})\\b` : word;
  let flags = settings.caseSensitive ? '' : 'i';
  return new RegExp(regexPattern, flags);
}

export async function applyPenaltyAndGetPenaltyReplyFromType(word, settings, user, channel, guild) {
  // get the penalty type given the penalty type setting and if user is admin or not
  const penaltyType = await getRestrictedWordPenaltyType(guild, user, settings.penalty.type);

  // Update the penalty count and last penalty date
  settings.penalty.penaltyCount++;
  let penaltyCount = settings.penalty.penaltyCount;
  settings.penalty.lastPenaltyDate = new Date().toISOString();

  switch (penaltyType) {
    case 'nothing':
      return applyWarning(
        word,
        settings,
        penaltyCount,
        'Warning: You used a restricted word.'
      );
    case 'gag':
      return await applyGag(
        word,
        settings,
        user,
        guild,
        penaltyCount,
        'Gagged for using a restricted word!'
      );
    case 'shame':
      return await applyShame(
        word,
        settings,
        user,
        channel,
        penaltyCount,
        'Shame on you for using a restricted word!'
      );
    case 'grounding':
      return await applyGrounding(
        word,
        settings,
        user,
        guild,
        penaltyCount,
        'You\'re grounded for using a restricted word!'
      );
    case 'lines':
      return await applyLines(
        word,
        settings,
        user,
        guild,
        penaltyCount,
        channel,
        'Write some lines for using a restricted word!'
      );
  }
}

export async function applyGuessPenaltyAndGetPenaltyReplyFromType(guess, word, settings, user, channel, guild) {
  const penaltyType = await getRestrictedWordPenaltyType(guild, user, settings.penalty.type);

  settings.penalty.guessCount = settings.penalty.guessCount || 0;
  settings.penalty.guessCount++;
  let penaltyCount = settings.penalty.guessCount;

  let warningTitle = `Warning: You guessed ***wrong*** (${guess}).`;
  let gagTitle = `Gagged for guessing (${guess}) wrong on a restricted word!`;
  let shameTitle = `Shame on you for guessing (${guess}) on a restricted word wrong!`;
  let groundingTitle = `You're grounded for guessing (${guess}) wrong!`;
  let linesTitle = `Write some lines for guessing (${guess}) wrong!`;

  switch (penaltyType) {
    case 'nothing':
      return applyWarning(
        word,
        settings,
        penaltyCount,
        warningTitle,
        'guess'
      );
    case 'gag':
      return await applyGag(
        word,
        settings,
        user,
        guild,
        penaltyCount,
        gagTitle,
        'guess'
      );
    case 'shame':
      return await applyShame(
        word,
        settings,
        user,
        channel,
        penaltyCount,
        shameTitle,
        'guess'
      );
    case 'grounding':
      return await applyGrounding(
        word,
        settings,
        user,
        guild,
        penaltyCount,
        groundingTitle,
        'guess'
      );
    case 'lines':
      return await applyLines(
        word,
        settings,
        user,
        guild,
        penaltyCount,
        channel,
        linesTitle,
        'guess'
      );
  }
}

function applyWarning(word, settings, penaltyCount, title, violationText = 'violation') {
  let replyMessage = `${title} ` +
    (settings.secret ? '' : `||(${word})||`) +
    '\n' +
    `This is ${violationText} #${penaltyCount}`;

  if (settings.notifyOnViolation) {
    replyMessage += `\n<@${settings.setterId}>`;
  }

  return {
    'reply': replyMessage,
    'embeds': []
  };
}

async function applyGag(word, settings, user, guild, penaltyCount, title, violationText = 'violation') {
  const gag = await bot.gag.forUser(user);
  await gag.newGag({gaggedByUser: user, reason: 'Restricted word usage', duration: penaltyCount});

  let replyMessage = `***${title}*** ` +
    (settings.secret ? '' : `||(${word})||`) +
    '\n' +
    `This is ${violationText} #${penaltyCount}\n` +
    `Now sit quietly and reflect on your language for ${penaltyCount} minutes`;

  if (settings.notifyOnViolation) {
    replyMessage += `\n<@${settings.setterId}>`;
  }

  return { 'reply': replyMessage, 'embeds': [] };
}

async function applyShame(word, settings, user, channel, penaltyCount, title, violationText = 'violation') {

  const shame = await bot.shame.forUser(user.id);
  await shame.start({
    source: 'restricted_word',
    countingTarget: calculateCountTo(penaltyCount), // Tapered 'to count to'
    channel: channel,
    oneTimeSlowdown: calculateSlowdown(penaltyCount), // Tapered 'slowdown'
    mistakePenalty: (10 * penaltyCount), // This could also be tapered if needed
  });

  let replyMessage = `***${title}*** ` +
    (settings.secret ? '' : `||(${word})||`) +
    '\n' +
    `This is ${violationText} #${penaltyCount}\n` +
    `You can count to ${shame.countingTarget}. You have a slowdown of ${shame.oneTimeSlowdown}, and a mistake penalty of ${shame.mistakePenalty}.`;

  if (settings.notifyOnViolation) {
    replyMessage += `\n<@${settings.setterId}>`;
  }

  return { 'reply': replyMessage, 'embeds': [] };
}

function calculateCountTo(penaltyCount) {
  if (penaltyCount <= 10) {
    return 20 * penaltyCount;
  } else {
    // Taper off the increase for penaltyCount > 10
    return 200 + (penaltyCount - 10) * 5;
  }
}

function calculateSlowdown(penaltyCount) {
  if (penaltyCount <= 10) {
    return Math.ceil(penaltyCount * 0.5);
  } else {
    // Taper off the increase for penaltyCount > 10
    return Math.ceil(5 + (penaltyCount - 10) * 0.08);
  }
}


async function applyGrounding(word, settings, user, guild, penaltyCount, title, violationText = 'violation') {
  const minutes = calculateGroundingMinutes(penaltyCount);
  const grounding = await bot.grounding.forUser(user.id);
  await grounding.start({
    minutesToDo: minutes,
    minutesMinimum: 2,
    percentage: 50
  });
  const embed = grounding.getGroundingEmbed();

  // Send the detailed embed to the designated "noise" channel
  const noiseChannelId = process.env.BOTNOISE_CHANNEL_ID;
  let groundingMessageUrl = '<#{noiseChannelId}>'; // default value
  const noiseChannel = guild.channels.cache.get(noiseChannelId);
  if (noiseChannel && noiseChannel instanceof TextChannel) {
    const message = await noiseChannel.send({ embeds: [embed] });
    groundingMessageUrl = message.url; // Get the URL of the sent message
  } else {
    console.error('Failed to fetch or incorrect type for BOTNOISE_CHANNEL_ID');
  }

  // Reply with a shorter message to the user
  let replyMessage = `***${title}*** ` +
    (settings.secret ? '' : `||(${word})||`) +
    '\n' +
    `This is ${violationText} #${penaltyCount}\n` +
    `${minutes} minutes have been applied. Details: ${groundingMessageUrl}`;

  if (settings.notifyOnViolation) {
    replyMessage += `\n<@${settings.setterId}>`;
  }

  return { 'reply': replyMessage, 'embeds': [] };
}

function calculateGroundingMinutes(penaltyCount) {
  if (penaltyCount <= 10) {
    return Math.ceil(penaltyCount * 1.2);
  } else {
    // Apply a different formula for penaltyCount > 10 to taper off the harshness
    return Math.ceil(12 + (penaltyCount - 10) * 0.45);
  }
}

async function applyLines(word, settings, user, guild, penaltyCount, channel, title, violationText = 'violation') {
  const lineCount = calculateLineCount(penaltyCount);
  let lineMessage;

  const lines = await bot.lines.forUser(user.id);

  if (lines.hasLines()) {
    await lines.addLinesToDo(lineCount);
    lineMessage = `Lines are already assigned so **${lineCount} more lines** have been added to the assignment.`;
  } else {
    await lines.assignLines({
      setByUserId: user.id,
      linesTodo: lineCount,
      channelIdWhereAssigned: channel.id,
      hideNumberCompleted: (penaltyCount > 10),
      randomCase: (penaltyCount > 30 ? 'random' : null),
      penaltyLines: Math.ceil(penaltyCount * 0.2),
      spectatorSetting: 'viewing',
    });
    lineMessage = lines.linesAssignedMessage;
  }

  let replyMessage = `***${title}*** ` +
    (settings.secret ? '' : `||(${word})||`) +
    '\n' +
    `This is ${violationText} #${penaltyCount}\n` +
    `${lineMessage}`;

  if (settings.notifyOnViolation) {
    replyMessage += `\n<@${settings.setterId}>`;
  }

  return { 'reply': replyMessage, 'embeds': [] };
}

function calculateLineCount(penaltyCount) {
  if (penaltyCount <= 10) {
    return Math.ceil(penaltyCount * 3);
  } else {
    // Taper off the increase for penaltyCount > 10
    return Math.ceil(30 + (penaltyCount - 10) * 1.01);
  }
}

/**
 * Calculates the Levenshtein distance between two strings.
 * I have no idea how this works and was written by ChatGPT
 * @param {string} a
 * @param {string} b
 * @return {number}
 */
function levenshteinDistance(a, b) {
  const matrix = [];

  // Increment along the first column of each row.
  for (let i = 0; i <= b.length; i++) {
    matrix[i] = [i];
  }

  // Increment each column in the first row.
  for (let j = 0; j <= a.length; j++) {
    matrix[0][j] = j;
  }

  // Fill in the rest of the matrix.
  for (let i = 1; i <= b.length; i++) {
    for (let j = 1; j <= a.length; j++) {
      if (b.charAt(i - 1) === a.charAt(j - 1)) {
        matrix[i][j] = matrix[i - 1][j - 1];
      } else {
        matrix[i][j] = Math.min(matrix[i - 1][j - 1] + 1, // substitution
          Math.min(matrix[i][j - 1] + 1, // insertion
            matrix[i - 1][j] + 1)); // deletion
      }
    }
  }

  return matrix[b.length][a.length];
}

/**
 * Finds the closest match for a given word among a set of restricted words.
 * @param {string} guess - The user's guess.
 * @param {Object} restrictions - The restrictions object from the user's data.
 * @return {string} - The closest matching restricted word.
 */
export function findClosestMatch(guess, restrictions) {
  let closestMatch = '';
  let lowestDistance = Infinity;

  for (const word in restrictions) {
    if (restrictions[word].secret) {
      const distance = levenshteinDistance(guess.toLowerCase(), word.toLowerCase());
      if (distance < lowestDistance) {
        lowestDistance = distance;
        closestMatch = word;
      }
    }
  }

  return closestMatch;
}
