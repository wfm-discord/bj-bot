import { ensureBrainKeyExistsPromise, brain } from 'bot-commons-utils/src/utils/brain.mjs';
import {topHasConsentByID} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {bot} from '../../services/bot.mjs';
import _ from 'lodash';

/**
 * Slowdown
 * Use this to get corner of shame slowdowns
 * @param  User user
 */
class Slowdown {
  constructor(user, guild) {
    this.user = user;
    this.guild = guild;
    this.count = 0;
    this.s = 0;
    this.setter_id = null;
  }

  /**
   * Gets total slowdown in seconds, including modifiers
   * @return int Seconds
   */
  async getTotalSlowdownSeconds() {
    let modifier = await this.getCountModifierSeconds(this.user);
    let setSeconds = await this.getSlowRecordSeconds(this.user);
    return +modifier + +setSeconds;
  }

  /**
   * gets the slow count modifier in seconds
   * for screwing up the count
   * @return int modifer seconds
   */
  async getCountModifierSeconds() {
    let modifier = 0;
    await ensureBrainKeyExistsPromise('count_fail_opt_out');
    if (brain.data.count_fail_opt_out[this.user.id]) {
      // if user has opted out, no slowdown
      modifier = 0;
    } else {
      // user has not opted out
      await ensureBrainKeyExistsPromise('count_fail_tracking');
      // get seconds from screwing up the count
      if (brain.data.count_fail_tracking[this.user.id]) {
        let tracking = brain.data.count_fail_tracking[this.user.id];
        if (tracking > 0) {
          modifier = +tracking - 1;
        }
      }
    }
    return modifier;
  }

  /**
   * Sifts though the set slowdown records and finds the highest one
   * If there are more than one that are highest, it returns the first one it found
   */
  async findSlowdownRecord() {
    let slowdown = await ensureBrainKeyExistsPromise('slowdown', this.user.id);
    if (_.isEmpty(slowdown)) {
      return false;
    } else {
      for (let setter_id in slowdown) {
        if (+this.s < +slowdown[setter_id].seconds) {
          // if this records is higher than current s
          // check for consent before updating
          if (await topHasConsentByID(bot, setter_id, this.user.id, this.guild)
            || slowdown[setter_id].self // set yourself
          ) {
            // consent is there, or you set this yourself
            this.s = slowdown[setter_id].seconds;
            this.setter_id = setter_id;
          }
        }
        this.count++;
      }
    }
  }

  /**
   * Get the set seconds from the record
   * @return int seconds
   */
  async getSlowRecordSeconds() {
    await this.findSlowdownRecord();
    return this.s;
  }

  /**
   * Get the quantity of set records
   * @return int count of records
   */
  async getSlowRecordCount() {
    await this.findSlowdownRecord();
    return this.count;
  }

  /**
   * Get the setter user ID from the record
   * @return string User ID
   */
  async getSlowRecordSetterId() {
    await this.findSlowdownRecord();
    return this.setter_id;
  }
}

export { Slowdown };
