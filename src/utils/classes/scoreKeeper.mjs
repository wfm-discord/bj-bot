// scorekeeper.mjs
// Description:
//   Helper class responsible for storing scores
//   This is a port over from https://github.com/ajacksified/hubot-plusplus
//   Some things are the same, some things are changed.
import { brain } from 'bot-commons-utils/src/utils/brain.mjs';


export default class ScoreKeeper {
  constructor() {
    this.storage = brain.data.plusPlus;
    if (typeof this.storage.last === 'string') {
      this.storage.last = {};
    }
  }

  getUser(user) {
    this.storage.scores[user] ||= 0;
    this.storage.reasons[user] ||= {};
    return user;
  }

  async saveUser(user, from, room, reason) {
    this.saveScoreLog(user, from, room, reason);
    await brain.write();
    return [this.storage.scores[user], this.storage.reasons[user][reason] || 'none'];
  }

  async add(user, from, room, reason) {
    if (this.validate(user, from)) {
      user = this.getUser(user);
      this.storage.scores[user]++;
      this.storage.reasons[user] ||= {};

      if (reason) {
        this.storage.reasons[user][reason] ||= 0;
        this.storage.reasons[user][reason]++;
      }

      await this.saveUser(user, from, room, reason);
      return [this.storage.scores[user], this.storage.reasons[user][reason] || 'none'];
    } else {
      return [null, null];
    }
  }

  async subtract(user, from, room, reason) {
    if (this.validate(user, from)) {
      user = this.getUser(user);
      this.storage.scores[user]--;
      this.storage.reasons[user] ||= {};

      if (reason) {
        this.storage.reasons[user][reason] ||= 0;
        this.storage.reasons[user][reason]--;
      }

      await this.saveUser(user, from, room, reason);
      return [this.storage.scores[user], this.storage.reasons[user][reason] || 'none'];
    } else {
      return [null, null];
    }
  }

  async erase(user, from, room, reason) {
    user = this.getUser(user);

    if (reason) {
      delete this.storage.reasons[user][reason];
      await this.saveUser(user, from, room);
      return true;
    } else {
      delete this.storage.scores[user];
      delete this.storage.reasons[user];
      return true;
    }
  }

  scoreForUser(user) {
    user = this.getUser(user);
    return this.storage.scores[user];
  }

  reasonsForUser(user) {
    user = this.getUser(user);
    return this.storage.reasons[user];
  }

  saveScoreLog(user, from, room, reason) {
    if (typeof this.storage.log[from] !== 'object') {
      this.storage.log[from] = {};
    }

    this.storage.log[from][user] = new Date();
    this.storage.last[room] = { user, reason };
  }

  last(room) {
    const last = this.storage.last[room];
    if (typeof last === 'string') {
      return [last, ''];
    } else {
      return [last.user, last.reason];
    }
  }

  isSpam(user, from) {
    // Ensure the `log` object and nested properties are initialized correctly
    if (!this.storage.log[from]) {
      this.storage.log[from] = {};
    }

    if (!this.storage.log[from][user]) {
      return false;
    }

    const dateSubmitted = this.storage.log[from][user];
    const date = new Date(dateSubmitted);
    const messageIsSpam = date.setSeconds(date.getSeconds() + 5) > new Date();

    if (!messageIsSpam) {
      delete this.storage.log[from][user]; // clean it up
    }

    return messageIsSpam;
  }

  validate(user, from) {
    return user !== from && user !== '' && !this.isSpam(user, from);
  }

  length() {
    return Object.keys(this.storage.scores).length;
  }

  top(amount) {
    const tops = [];

    for (const [name, score] of Object.entries(this.storage.scores)) {
      tops.push({ name, score });
    }

    tops.sort((a, b) => b.score - a.score);
    return tops.slice(0, amount);
  }

  bottom(amount) {
    const all = this.top(Object.keys(this.storage.scores).length);
    all.sort((a, b) => b.score - a.score).reverse();
    return all.slice(0, amount);
  }

  async normalize(fn) {
    const scores = {};

    for (const [name, score] of Object.entries(this.storage.scores)) {
      scores[name] = fn(score);
      if (scores[name] === 0) {
        delete scores[name];
      }
    }

    this.storage.scores = scores;
    await brain.write();
  }
}
