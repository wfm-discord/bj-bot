// src/utils/messageExports.mjs
import { encode } from 'gpt-3-encoder';
import {ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {handleEmbeds, formatMessage} from 'bot-commons-utils/src/utils/messageExports.mjs';

async function formatMessageWithEmbeds(message, forMeeting = false, includeDates = true) {
  if (forMeeting) return await formatMeetingMessage(message);

  return formatMessage(message, includeDates);
}

async function formatMeetingMessage(message) {
  let formattedMessage = '';

  const dnd = await ensureBrainKeyExistsPromise('dnd_registration');
  const userId = message.author.id;

  // Function to replace user tags with character names
  const replaceUserTagsWithCharacters = async (text) => {
    const guild = message.guild;

    for (const userId of Object.keys(dnd)) {
      const guildMember = await guild.members.fetch(userId).catch(() => null);
      if (!guildMember) continue;

      const userTag = guildMember.displayName; // Get the guild nickname or username
      const characterName = dnd[userId].character;
      const regex = new RegExp(`\\*\\*${userTag}\\*\\*`, 'g');
      text = text.replace(regex, characterName);
    }
    return text;
  };

  // loop through each user in dnd.
  // if user talking, replace with character name
  // if user tag with ** on each side of it, replace with character name
  if (dnd[userId]) {
    const characterName = dnd[userId].character;
    formattedMessage = `${characterName}: ${message.content}\n`;
  } else if (message.author.id === '1001955060210749492') {
    formattedMessage = `${message.content}\n`;
  } else if (message.author.username.includes('[Scriptly]')) {
    // Remove [Scriptly] from the message
    let cleanedTag = message.author.tag.replace('[Scriptly] ', '');
    formattedMessage = `${cleanedTag}: ${message.content}\n`;
  } else {
    formattedMessage = `${message.author.tag}: ${message.content}\n`;
  }

  // Replace any user tags within the message content with character names
  formattedMessage = await replaceUserTagsWithCharacters(formattedMessage);

  return handleEmbeds(message, formattedMessage);
}



// Known bug: if a Discord message can be larger than the GPT token limit, this might not work. But that shouldn't happen
export async function splitMessagesByTokenLimit(messages, tokenLimit, forMeeting = false, includeDates = true) {
  const result = [];
  let currentChunk = '';
  let currentChunkTokens = 0;

  for (const msg of messages) {
    const formattedMessage = await formatMessageWithEmbeds(msg, forMeeting, includeDates);
    const msgTokens = encode(formattedMessage).length;

    if (currentChunkTokens + msgTokens <= tokenLimit) {
      currentChunk += formattedMessage + '\n';
      currentChunkTokens += msgTokens;
    } else {
      result.push(currentChunk.trim());
      currentChunk = formattedMessage + '\n';
      currentChunkTokens = msgTokens;
    }
  }

  if (currentChunk) result.push(currentChunk.trim());

  return result;
}

export async function splitMessagesBySize(messages, sizeLimit, forMeeting = false, includeDates = true) {
  const result = [];
  let currentChunk = '';
  let currentChunkSize = 0;

  for (const msg of messages) {
    const formattedMessage = await formatMessageWithEmbeds(msg, forMeeting, includeDates);
    const msgSize = Buffer.byteLength(formattedMessage, 'utf8') + 1; // +1 for the newline character

    if (currentChunkSize + msgSize <= sizeLimit) {
      currentChunk += formattedMessage + '\n';
      currentChunkSize += msgSize;
    } else {
      result.push(currentChunk.trim());
      currentChunk = formattedMessage + '\n';
      currentChunkSize = msgSize;
    }
  }

  if (currentChunk) result.push(currentChunk.trim());

  return result;
}
