
import {topHasConsentByID} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {brain, ensureBrainKeyExists, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {bot} from '../services/bot.mjs';

export const getRandomMsg = async function (fail_user, guild) {
  const list = await ensureBrainKeyExistsPromise('count_fail_msgs');
  if (!list || list.length === 0) {
    return false;
  }

  let messages = list[fail_user.id];
  let array = [];
  let i = 0;

  // Make a new array of text messages
  // This way I can check consent for each one
  // in case there's a safeword active or
  // consent was removed from someone
  for (let id in messages) {
    // Check for consent by user IDs
    if (await topHasConsentByID(bot, messages[id].set_by, fail_user.id, guild)) {
      array[i++] = messages[id];
    }
  }

  if (Object.keys(array).length === 0) {
    return false;
  } else {
    // Return a random message from the list
    return array[Math.floor(Math.random() * array.length)];
  }
}


export function findMessageById(provided_id) {
  let messages = ensureBrainKeyExists('count_fail_msgs');
  for (let sub_id in messages) {
    for (let id in messages[sub_id]) {
      if (id === provided_id) {
        return messages[sub_id][id];
      }
    }
  }
}

export async function clearUserMessages(sub_id, top_id) {
  let user_messages = ensureBrainKeyExists('count_fail_msgs', sub_id);
  let count = 0;
  for (let id in user_messages) {
    if (user_messages[id].set_by === top_id) {
      count++;
      delete user_messages[id];
    }
  }
  await brain.write();
  return count;
}

export function countUserMessages(user_id) {
  let user_messages = ensureBrainKeyExists('count_fail_msgs', user_id);
  let count = 0;
  for (let id in user_messages) {
    count++;
  }
  return count;
}
