import { AttachmentBuilder, EmbedBuilder } from 'discord.js';
import fetch from 'node-fetch';

import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';
import {timeUtils} from 'bot-commons-utils/src/utils/time.mjs';

export const wfmUtils = {
  /**
   * Get the parameters needed to access a WFM task from a given WFM task URL
   * @param url
   * @returns {{accessCode: string, taskId: *}}
   */
  getInfoFromUrl(url) {
    let urlParts = url.split('/task/');
    let taskId = urlParts[1].split('?')[0];
    let accessCodeParam = urlParts[1].split('?')[1];
    let accessCode = accessCodeParam ? new URLSearchParams(accessCodeParam).get('accessCode') : null;
    return {taskId, accessCode};
  },

  /**
   * Get a list of users based on a search query
   * @param {string} query - The search query you wish to use
   * @param {number} limit - The number of users to list
   * @param {string} sortBy - How to sort the list, can be "created" or "name"
   */
  getUsers: async function(query, limit, sortBy) {
    const getUsersResponse = await fetch('https://api.writeforme.org/api/v1/getUsers', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ genderSearch: false, limit: limit, listType: 'userSearch', query: query, skip: 0, sortAsc: false, sortBy: sortBy })
    });

    return getUsersResponse;
  },
  /**
   * Get details about a user
   * @param {string} userID - The ID of the user to get information on
   */
  getUserDetails: async function(userID) {
    const userDataResponse = await fetch('https://api.writeforme.org/api/v1/userdetail', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ fullscreenPhoto: true, userID: userID })
    });

    return userDataResponse;
  },

  /**
   * Get information on the tasks a user has created.
   * @param {string} userID - The ID of the user to get information on
   * Returns task count and sum only
   */
  getUserTasksCreated: async function(userID) {
    const tasksCreatedResponse = await fetch('https://api.writeforme.org/api/v1/tasksCreatedStats', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ userID: userID })
    });

    return tasksCreatedResponse;
  },
  /**
   * Get information on the tasks a user has completed.
   * @param {string} userID - The ID of the user to get information on
   */
  getUserTasksCompleted: async function(userID) {
    const tasksDoneResponse = await fetch('https://api.writeforme.org/api/v1/tasksDoneStats', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ userID: userID })
    });

    return tasksDoneResponse;
  },

  /**
   * Get the top typists for the last 24 hours, 30 days, or the past year.
   * @param {number} chartsFilter - 0 = 24 hours, 1 = 30 days, 2 = 365 days
   * @param {number} limit - The number of typists to list
   */
  getTypistCharts: async function(chartsFilter, limit) {
    const typistChartResponse = await fetch('https://api.writeforme.org/api/v1/getCharts', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ chartsFilter: chartsFilter, limit: limit, listType: 'charts', minScore: 3333, query: '', skip: 0, sortAsc: true, sortBy: 'name', tasksetterCharts: false })
    });

    return typistChartResponse;
  },

  /**
   * Get the top tasksetters for the last 24 hours, 30 days, or the past year.
   * @param {number} chartsFilter - 0 = 24 hours, 1 = 30 days, 2 = 365 days
   * @param {number} limit - The number of typists to list
   */
  getTasksetterCharts: async function(chartsFilter, limit) {
    const tasksetterChartResponse = await fetch('https://api.writeforme.org/api/v1/getCharts', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ chartsFilter: chartsFilter, limit: limit, listType: 'charts', minScore: 3333, query: '', skip: 0, sortAsc: true, sortBy: 'name', tasksetterCharts: true })
    });

    return tasksetterChartResponse;
  },
  /**
   * Get the top tasks from the last 24 hours
   * @param {number} limit - The number of tasks to list
   */
  getTaskCharts: async function(limit) {
    const taskChartsResponse = await fetch('https://api.writeforme.org/api/v1/getTaskCharts', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ chartsFilter: chartsFilter, limit: limit, listType: 'taskCharts', minScore: 3333, skip: 0 })
    });

    return taskChartsResponse;
  },

  /**
   * Get a list of the latest public tasks
   * @param {number} limit - The number of tasks to list
   * @param {number} skip - The number of tasks that should be skipped (0 for latest)
   * @param {string} query - Search query, only show tasks that contain this string ("" for all)
   */
  getLatestPublicTasks: async function(limit, skip, query) {
    const latestPublicTasksResponse = await fetch('https://api.writeforme.org/api/v1/tasks', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ limit: limit, listType: 'latestPublicTasks', query: query, skip: skip, sortAsc: false, sortBy: 'created' })
    });

    return latestPublicTasksResponse;
  },

  getUserTasks: async function(userID, limit) {
    const taskResponse = await fetch('https://api.writeforme.org/api/v1/tasks', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({user: userID, limit: limit, listType: 'userTasks', query: '', skip: 0, sortAsc: false, sortBy: 'created' })
    });

    return taskResponse;
  },

  /**
   * Get information on a given task
   * @param {string} taskID - The ID of the task
   * @param {string} [accessCode] - Optional access code for the task
   */
  getTaskDetails: async function(taskID, accessCode = null) {
    const requestBody = { taskID: taskID };
    if (accessCode) {
      requestBody.accessCode = accessCode;
    }

    const taskDetailResponse = await fetch('https://api.writeforme.org/api/v1/taskDetail', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestBody)
    });

    return taskDetailResponse;
  },

  /**
   * Get a list of tasks completions (called solutions on wfm)
   * @param {number} limit - The number of completions to list
   * @param {string} sortBy - How to sort the list, can be "created", "mistakes", "userName", "totalTime"
   * @param {string} taskID - The ID of the task to get the solutions to
   */
  getTaskSolutions: async function(limit, sortBy, taskID) {
    const taskSolutionsResponse = await fetch('https://api.writeforme.org/api/v1/getSolutions', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ limit: limit, listType: 'taskSolutions', query: '', skip: 0, sortAsc: false, sortBy: sortBy, task: taskID })
    });

    return taskSolutionsResponse;
  },

  createUserProfileEmbed: async function(chatMessage, message, userID) {
    await ensureBrainKeyExistsPromise('wfm_profiles');
    if (brain.data.wfm_profiles[userID]) {
      let wfmUserID = /[^/]*$/.exec(brain.data.wfm_profiles[userID])[0];
      const guildUser = message.guild.members.cache.find(member => member.id === userID);
      // Get User data
      const userDataResponse = await this.getUserDetails(wfmUserID);
      const userTasksCreatedResponse = await this.getUserTasksCreated(wfmUserID);
      const userTasksCompletedResponse = await this.getUserTasksCompleted(wfmUserID);
      // Check we have data
      if (userDataResponse.status === 200 && userTasksCreatedResponse.status === 200 && userTasksCompletedResponse.status === 200) {
        userDataResponse.json().then(userData => {
          userTasksCreatedResponse.json().then(tasksCreatedData => {
            userTasksCompletedResponse.json().then(tasksDoneData => {
              if (message.channel.nsfw) {
                if (userData.photo !== undefined) {
                  const photoBuffer = new Buffer.from(userData.photo, 'base64');
                  const attatchment = new AttachmentBuilder(photoBuffer, {name: 'avatar.jpg'});
                  const embed = new EmbedBuilder()
                    .setColor(0xC83F17)
                    .setTitle(`${guildUser.displayName}'s WFM Profile`)
                    .setURL(brain.data.wfm_profiles[userID])
                    .setThumbnail('attachment://avatar.jpg')
                    .addFields(
                      { name: 'Name', value: userData.name },
                      { name: 'Bio', value: userData.bio ? userData.bio.length > 250 ? userData.bio.substring(0, 250) + '...' : userData.bio : 'No bio set.'},
                      { name: 'Tasks Created', value: `${tasksCreatedData.count} ${tasksCreatedData.sum === 0 ? '' : '(' + timeUtils.msToTime(tasksCreatedData.sum) + ')'}`, inline: true},
                      { name: 'Tasks Completed', value: `${tasksDoneData.count} ${tasksDoneData.sum === 0 ? '' : '(' + timeUtils.msToTime(tasksDoneData.sum) + ')'}`, inline: true},
                      { name: 'Link', value: brain.data.wfm_profiles[userID] }
                    )
                    .setFooter({text: 'WriteForMe', iconURL: 'https://i.imgur.com/UCrHoOS.png'});
                  message.reply({ content: chatMessage, embeds: [embed], files: [attatchment], ephemeral: false});
                } else {
                  const embed = new EmbedBuilder()
                    .setColor(0xC83F17)
                    .setTitle(`${guildUser.displayName}'s WFM Profile`)
                    .setURL(brain.data.wfm_profiles[userID])
                    .addFields(
                      { name: 'Name', value: userData.name },
                      { name: 'Bio', value: userData.bio ? userData.bio.length > 250 ? userData.bio.substring(0, 250) + '...' : userData.bio : 'No bio set.'},
                      { name: 'Tasks Created', value: `${tasksCreatedData.count} ${tasksCreatedData.sum === 0 ? '' : '(' + timeUtils.msToTime(tasksCreatedData.sum) + ')'}`, inline: true},
                      { name: 'Tasks Completed', value: `${tasksDoneData.count} ${tasksDoneData.sum === 0 ? '' : '(' + timeUtils.msToTime(tasksDoneData.sum) + ')'}`, inline: true},
                      { name: 'Link', value: brain.data.wfm_profiles[userID] }
                    )
                    .setFooter({text: 'WriteForMe', iconURL: 'https://i.imgur.com/UCrHoOS.png'})
                  message.reply({ content: chatMessage, embeds: [embed], ephemeral: false });
                }
              } else {
                const embed = new EmbedBuilder()
                  .setColor(0xC83F17)
                  .setTitle(`${guildUser.displayName}'s WFM Profile`)
                  .setURL(brain.data.wfm_profiles[userID])
                  .addFields(
                    { name: 'Name', value: userData.name },
                    { name: 'Tasks Created', value: `${tasksCreatedData.count} ${tasksCreatedData.sum === 0 ? '' : '(' + timeUtils.msToTime(tasksCreatedData.sum) + ')'}`, inline: true},
                    { name: 'Tasks Completed', value: `${tasksDoneData.count} ${tasksDoneData.sum === 0 ? '' : '(' + timeUtils.msToTime(tasksDoneData.sum) + ')'}`, inline: true},
                    { name: 'Link', value: brain.data.wfm_profiles[userID] }
                  )
                  .setFooter({text: 'WriteForMe', iconURL: 'https://i.imgur.com/UCrHoOS.png'});
                message.reply({ content: chatMessage, embeds: [embed], ephemeral: false });
              }
            });
          });
        });
      } else {
        message.reply("Unable to find that user's profile.");
      }
    } else {
      message.reply(`<@${userID}> doesn't have a link set.`);
    }
  }
}
