import {CommandInteraction, Message} from "discord.js"
import {bot} from "../services/bot.mjs";


export function pingsEveryone(messageContent) {
  if (typeof messageContent === 'string' && messageContent) {
    return messageContent.match(/@everyone/i) || messageContent.match(/@here/i);
  } else {
    return false;
  }
}

/**
 *
 * @param {CommandInteraction|Message} interaction
 * @param {string} messages
 * @returns {Promise<boolean>} whether the sender was punished for a mass ping
 */
export async function punishMassPings(interaction, ...messages) {
  if (messages.some(pingsEveryone)) {
    await interaction.reply('Fuck you. And SHAME on you for trying to ping everyone!');
    const shame = await bot.shame.forUser((interaction.user || interaction.author).id);
    await shame.start({source: 'mass_ping'});
    return true;
  }
  return false;
}