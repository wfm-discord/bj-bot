import _ from 'lodash';
import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';
import {isUserInGuild} from 'bot-commons-utils/src/utils/users.mjs';
import {bot} from '../services/bot.mjs';

export let rolling = false;
let keys;
let entries;
let sent = [];

let user_id, channel, guild;
let commandType = 'Slash'

// Thank you StackOverflow
// https://stackoverflow.com/questions/3583724/how-do-i-add-a-delay-in-a-javascript-loop
const timer = ms => new Promise(res => setTimeout(res, ms));
const pause_timer = 7000;

export const suspenseStrings = [
  'A few more seconds won\'t hurt anyone, right?',
  'A little more suspense never hurt anybody, right?',
  'Almost done...',
  'Almost there...',
  'And the winner is... (wait for it)',
  'Anticipation is half the fun, right?',
  'Are you on the edge of your seat?',
  'Are you ready for the moment of truth?',
  'Buckle up, it\'s going to be a bumpy ride!',
  'Can you feel the tension building?',
  'Can\'t have a winner without a little suspense, right?',
  'Don\'t blink now!',
  'Don\'t blink, you might miss the moment!',
  'Don\'t get too comfortable...the moment of truth is coming!',
  'Don\'t want to wait anymore? Suffer.',
  'Drumroll please... ',
  'Gosh, I hope this works...',
  'Gotta add some suspense here, ya know.',
  'Hang tight!',
  'Hold on a moment',
  'Hold on to your hats...this is going to be good!',
  'Hold onto your hats!',
  'Hold your breath...',
  'Hold your horses!',
  'Holding my breath...don\'t make me pass out now...',
  'https://tenor.com/view/lafuddyduddy-drum-duck-cute-drum-roll-please-gif-16950134',
  'https://tenor.com/view/lets-see-what-happens-steve-kornacki-msnbc-lets-wait-for-it-were-gonna-wait-for-the-result-gif-19694593',
  'https://tenor.com/view/lottery-lotto-gif-12706150',
  'https://tenor.com/view/psych-shawn-shawn-spencer-james-roday-wait-gif-8535911',
  'https://tenor.com/view/suspense-waiting-wait-mystery-daily-show-gif-22337508',
  'https://tenor.com/view/tea-shade-goth-halloween-you-just-wait-gif-15826642',
  'https://tenor.com/view/wait-for-it-barney-stinson-himym-neil-patrick-harris-gif-6055274',
  'https://tenor.com/view/willy-wonka-excited-smile-the-suspense-gene-wilder-gif-5933521',
  'Huh? Oh! Spaced out there a second.',
  'Hush, hush...don\'t say a word, the moment of truth is almost here.',
  'I could tell you who lost right now, but where\'s the fun in that?',
  'I could use a little help here, anyone want to lend me a hand?',
  'I don\'t want to rush this, especially because I know you probably do.',
  'I don\'t want to spoil the surprise...yet.',
  'I hope you\'re ready for this.',
  'I know you\'re like a kid before Christmas... just can\'t wait to open that present!',
  'I love to make you wait.',
  'I promise it\'s worth the wait.',
  'I\'m almost done, just a couple more shakes.',
  'I\'m almost ready, just one more second...',
  'I\'m counting down the seconds...are you?',
  'I\'m not stalling, I swear!',
  'I\'m not sure if you\'re ready for this yet...',
  'I\'m so excited, and I just can\'t hide it...',
  'If you\'re not excited yet, you will be soon.',
  'It\'s almost time...',
  'Is the anticipation is killing you?',
  'Just a little bit longer.',
  'Just another moment.',
  'Just one more minute...',
  'Just swishing the names around here.',
  'Just when you thought you couldn\'t wait any longer...',
  'Keep calm...I think I\'ve almost got it.',
  'Keep your eyes peeled...',
  'Keep your fingers crossed...',
  'Let me just double check my list...',
  'Let\'s add a little more tension...',
  'Let\'s build up the excitement!',
  'Let\'s just build the suspense a little bit longer.',
  'Oh hold on, I shook up my grocery list instead of the list of names.',
  'Okay, okay, I\'m hurrying. But I don\'t want to make any mistakes!',
  'One. More. Second.',
  'Oops, I dropped a few, give me a moment to make sure they\'re still all here.',
  'Patience is a virtue...a virtue you are putting to the test!',
  'Patience.',
  'Shake shake shaking up the names... ',
  'SHAME SHAME SHAME on you for being so impatient.',
  'Shhh...listen...do you hear that? It\'s the sound of suspense!',
  'Taking my time to savor this moment.',
  'The anticipation is real.',
  'The moment of truth is near.',
  'The moment we\'ve all been waiting for...',
  'The suspense is almost too much to bear!',
  'The suspense is killing me too!',
  'The tension is thick enough to cut with a knife.',
  'This definitely needs a few more seconds of shaking around.',
  'This is it, this is it, this is it!',
  'Tick tock, tick tock, the suspense is killing me too! But I love to make you suffer.',
  'Time seems to slow down when you\'re waiting for something, doesn\'t it?',
  'Time to stir things up a bit!',
  'Wait for iiiiiiiit....',
  'Wait for it...',
  'We\'re almost there.',
  'Who\'s ready for some action?',
  'You ready?',
  'You\'re on the edge of your seat, I can tell!',
  'Your anticipation is palpable.',
];
export const chooseStrings = [
  `Someone needs to go to the Corner of SHAME and it's going to be `,
  'I pulled a name and the loser is ',
  'SHAME SHAME SHAME on ',
  'And the loser is ',
  'Looks like we have a loser... it\'s ',
  'The odds were not in your favor ',
  'The lottery gods have spoken, and they\'ve chosen ',
  'The wheel of fortune has stopped spinning, and it\'s landed on ',
  'Better luck next time ',
  'And the unlucky person is ',
  'You can\'t win them all, ',
  'Don\'t feel too bad, it could have been worse... or could it? Either way, you\'re the loser ',
  'You have been chosen by the fates to receive the "loser" label, ',
  'And the winner is... NOT ',
  'Well, we can\'t all be winners. Especially not the loser ',
];


// Adds a user to the lottery
export async function joinLottery(interaction, message) {
  if (rolling) {
    await replyAppropriately(interaction, message, 'You can\'t join while the Lottery of Shame is being rolled.')
    return;
  }

  setFields(interaction, message);

  if (entries[user_id]) {
    await replyAppropriately(interaction, message, 'You\'re already in the Lottery of Shame, you can\'t join twice.');
    return
  }

  entries[user_id] = user_id;
  await brain.write();
  await replyAppropriately(interaction, message, 'You\'ve been added to the Lottery of Shame. Good luck! 😈');
}


// Removes a user from the lottery
export async function leaveLottery(interaction, message) {
  if (rolling) {
    await replyAppropriately(interaction, message, 'You can\'t leave while the Lottery of Shame is being rolled. ' +
      'Nice try!');
    return;
  }

  setFields(interaction, message);

  if (!entries[user_id]) {
    await replyAppropriately(interaction, message, 'You weren\'t entered in the first place.');
    return;
  }

  delete entries[user_id];
  await brain.write();
  await replyAppropriately(interaction, message, 'Coward. You\'ve been removed from the Lottery of Shame ' +
    'but you should go write some lines for chickening out.\n');
}

// Removes a user from the lottery if they have left the server.
export async function forceLeaveLottery(member) {
  entries = ensureBrainKeyExists('corner_line');

  if (!entries[member.user.id]) {
    // DO nothing
  } else {
    delete entries[member.user.id];
    await brain.write();
  }
}


// Lists the users in the lottery
export async function listLottery(interaction, message) {
  if (rolling) {
    await replyAppropriately(interaction, message, 'You can\'t check the Lottery of Shame while it\'s being rolled. Nice try!');
    return;
  }

  entries = ensureBrainKeyExists('corner_line');
  for (let user_id in entries) {
    if (!await isUserInGuild(interaction.guild, user_id)) {
      delete entries[user_id];
    }
  }
  await brain.write();

  setFields(interaction, message);
  let length = Object.keys(entries).length

  if (length === 0) {
    await replyAppropriately(interaction, message, 'No one is entered just yet. Maybe you should add yourself.');
    return;
  }

  let response = `There ${length > 1 ? 'are' : 'is'} **${length}** ${length > 1 ? 'people' : 'person'} in the lottery:`
  for (let user_id in entries) {
    response += `\n<@${user_id}>`
  }
  await replyAppropriately(interaction, message, response);
}

// Draw winners from the lottery
export async function rollLottery(interaction, message, winners) {
  if (rolling) {
    await replyAppropriately(interaction, message, 'The lottery is already rolling. Be patient!');
    return;
  }

  entries = ensureBrainKeyExists('corner_line');
  for (let user_id in entries) {
    if (!await isUserInGuild(interaction.guild, user_id)) {
      delete entries[user_id];
    }
  }
  await brain.write();

  setFields(interaction, message);
  let keys = Object.keys(entries);

  if (keys.length === 0) {
    await replyAppropriately(interaction, message, 'No one is entered just yet. Maybe you should add yourself.');
    return;
  }

  rolling = true;
  winners = _.clamp(winners, 1, keys.length)

  // Send a message saying the lottery is being rolled


  if (keys.length === 1) {
    await replyAppropriately(interaction, message, "Really? There's only one name. Okay. " +
      "I *guess* I'll shake up the list that consists of *one whole name* and pick one 🙄.");
    await addSuspense(message);
  } else if (keys.length === winners) {
    await replyAppropriately(interaction, message, `Really? You want me to draw ${winners} names??? ` +
      `The lottery only has ${winners} names. Not very fair for everyone is it? 😈\n` +
      'Okay. I guess I\'ll shake up the list and choose *everybody*.');
    await addSuspense(message);
  } else {
    await replyAppropriately(interaction, message, 'Okay give me a moment to shake up the ' +
      keys.length + ` names and I'll choose ${winners}.`);
    await addSuspense(message);
  }

  await addSuspense();
  await timer(pause_timer);
  await chooseWinners(winners);

  brain.data.corner_line = {}
  rolling = false;
  await brain.write();
}


// Sends 1-3 random suspense messages with a preset ms delay
async function addSuspense() {
  sent = []
  for (let i = 0; i < 3; i++) {
    await timer(pause_timer);

    let suspenseMessage = _.sample(suspenseStrings)
    while (true) {
      if (!sent.includes(suspenseMessage)) break;
      suspenseMessage = _.sample(suspenseStrings)
    }

    sent.push(suspenseMessage)
    await channel.send(suspenseMessage);

    if (Math.floor(Math.random() * 2) === 0) break;
  }
}


// Chooses winners with a small delay between messages
async function chooseWinners(count) {
  for (let i = 0; i < count; i++) {
    await timer(500);

    keys = Object.keys(brain.data.corner_line);
    let randIndex = Math.floor(Math.random() * keys.length);
    let random_id = keys[randIndex];

    const shame = await bot.shame.forUser(random_id);
    await shame.start({
      expiresInMs: 172800000, //48 hours
      source: 'lottery',
    });

    await channel.send(_.sample(chooseStrings) + `<@${random_id}>!`)

    delete brain.data.corner_line[random_id];
    await brain.write();
  }
}


// Handle variables correctly for text and slash commands
function setFields(interaction, message) {
  entries = ensureBrainKeyExists('corner_line');

  if (interaction != null) {
    user_id = interaction.user.id;
    channel = interaction.channel;
    guild = interaction.guild;
    commandType = 'Slash'
  } else {
    user_id = message.author.id;
    channel = message.channel;
    guild = message.guild;
    commandType = 'Text'
  }
}

// Reply to the message that initiated the command
async function replyAppropriately(interaction, message, text) {
  setFields(interaction, message);

  switch (commandType) {
    case 'Slash': {
      await interaction.reply(text);
      break;
    }
    case 'Text': {
      await message.reply(text);
      break;
    }
    default: console.log('Encountered an error while attempting to formulate a reply')
  }
}
