// Description:
//   Clears out any set shame roles in case the timer doesn't do it.
//
// Dependencies:
//   node-cron

const logs_room = process.env.LOGS_ROOM;

export default {
  // Cron schedule string - runs every minute at 1 second after each of these times
  schedule: '1 0,15,30,45 * * * *',

  // Cron task function - removes shame role for users whose timer has expired
  task: async function(bot) {
    const log_channel = bot.client.channels.cache.get(logs_room);

    for await (const shame of bot.shame.expiredUsers()) {
      try {
        await shame.clear(`shame expired`);
      } catch (error) {
        // Handle any unexpected errors
        console.error(`Error clearing shame for user ${shame.getUserId()}:`, error);
        log_channel.send(`Error occurred while clearing shame for <@${shame.getUserId()}>.`);
      }
    }
  }
}
