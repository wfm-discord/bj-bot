
import { ensureBrainKeyExists, brain } from 'bot-commons-utils/src/utils/brain.mjs';

const admin_room = process.env.ADMIN_ROOM;

/**
 * Cron to decrement slowdown timers every Monday night at midnight (I think)
 */
export default {
  schedule: '0 0 * * 1',
  task: async function(bot) {
    const list = ensureBrainKeyExists('count_fail_tracking');
    let template = 'Weekly cron to reduce fail counts has completed:';
    let message = template;

    for (let user_id in list) {
      if (brain.data.count_fail_tracking[user_id] > 0) {
        brain.data.count_fail_tracking[user_id]--;
        message += `<@${user_id}> slowdown: ${brain.data.count_fail_tracking[user_id] - 1}s\n`;
      }
    }
    await brain.write();

    if (message !== template) {
      await brain.write();
      const channel = bot.client.channels.cache.get(admin_room);
      channel.send(message);
    }
  }
}
