const run_time = process.env.DAILY_TASK_CRON || '1 0 0 * * *'; // 12:01am every day
import { postRandomTask } from '../utils/task-of-the-day.mjs'; // Adjust the path as necessary

export default {
  schedule: run_time,

  task: async function() {
    try {
      await postRandomTask();
      console.log('Task of the Day posted successfully.');
    } catch (error) {
      console.error('Failed to post Task of the Day:', error);
    }
  }
};
