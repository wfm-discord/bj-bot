import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';

const typing_role = process.env.CURRENTLY_TYPING_ROLE_ID;
const logs_room = process.env.LOGS_ROOM;

export default {
  schedule: '0 */2 * * * *',

  task: async function (bot) {
    const hard_at_work = await ensureBrainKeyExistsPromise('hard_at_work');
    // Get current time
    const currentTime = new Date().getTime();
    // Loop through all keys
    for (let userId in hard_at_work) {
      if (hard_at_work[userId].end <= currentTime) {
        // Get member
        const member = await bot.client.guilds.cache.get(process.env.GUILD_ID).members.fetch(userId);
        // Remove role
        await member.roles.remove(typing_role);
        // Log channel
        const log_channel = bot.client.channels.cache.get(logs_room);
        log_channel.send(`Typing role expired for <@${member.id}>`);
        // Delete brain object
        delete brain.data.hard_at_work[member.id];
        await brain.write();
      }
    }
  },
};
