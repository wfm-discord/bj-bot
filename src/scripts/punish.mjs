// src/scripts/punish.mjs
import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';
import {Slowdown} from '../utils/classes/Slowdown.mjs';
import _ from 'lodash';
import {delay, deleteAllUnpinnedMessages} from 'bot-commons-utils/src/utils/messageUtils.mjs';

const shame_room = process.env.SHAME_ROOM;

export const punish_and_count = {
  regex: /^\?punish$/i,
  tag_bot: false,
  execute: async function(message, bot) {
    const user = message.author;
    if (shame_room === message.channel.id) {
      // get any existing shame record
      const shame = await bot.shame.forUser(user.id);

      if (!shame.isShamed()) {
        message.reply(`You weren't shamed, but if you want shame so bad, you've got it.`);
        // if you want shame so bad, you've got it
        await shame.start({});
      }

      // Check if the user is already being punished
      if (shame.isCounting()) {
        message.reply(`You're already being punished! Keep counting to ${shame.countingTarget}.`);
        return;
      }

      let reply_text = 'You\'ve acted shamefully! Now you are in the Corner of Shame.\n';
      shame.startCounting();
      await brain.write();

      // look for having messed up the count.
      if (shame.hasValidLastFailNumber()) {
        reply_text += 'Oh *snap,* you messed up counting at ' + shame.failedCountNumber + '!!\n';
      }
      message.reply(reply_text + `Count up to **${shame.countingTarget}** to get out!`);

      // Slowdown part
      // this code is much simpler using a separate function even if it's only ever used in one place.
      let {
        slowdown,
        onetime,
        countMoreThanOne,
        selfImposed,
        modifier,
        top_id,
        count
      } = await calculateSlowdown(user, shame.oneTimeSlowdown);

      let slowdownMessage;
      if (slowdown) {
        slowdownMessage = `You've been slowed down! The room is now set to a ${slowdown} second slow mode.\n`;
        if (countMoreThanOne) {
          slowdownMessage += `You had ${count} slowdowns. The highest was chosen.`;
        }
        if (modifier > 0) {
          slowdownMessage += `\n**Oh snap** you messed up the counting! **${modifier} seconds** of your slowdown came from messing up the count. (Use \`slow modifier?\` for more information.)`;
        }
        if (selfImposed) {
          slowdownMessage += '\nYou have only yourself to blame.';
        } else if (top_id) {
          slowdownMessage += `\nYou should thank <@${top_id}> for that.`;
        }
      } else {
        slowdownMessage = 'Congrats, you don\'t have any slowdown set. Still, don\'t go too fast or I might miss some numbers.\n';
      }
      if (onetime || onetime === 0) {
        slowdownMessage += 'This is a one-time slowdown.';
      }
      const member = message.member;
      await message.channel.setRateLimitPerUser(slowdown, `Slow down for ${member.displayName} based on their settings`);
      message.channel.send(slowdownMessage);
      await brain.write();
    } else {
      // User attempted to trigger punishment outside of the shame room
      const shame = await bot.shame.forUser(user.id);
      // 150 for first offense, 100 for additional
      await shame.start({
        source: 'punish_elsewhere',
        countingTarget: (shame.countingTarget ? 100 : 150),
      });

      message.reply('Shame on you for even *trying* that in another channel. ' +
        `Your target is now ${shame.countingTarget}. ` +
        `Now get over to <#${shame_room}> and count your way out.`);
    }
  }
};

export async function calculateSlowdown(user, overrideSlowdown = null) {
  if (overrideSlowdown !== null) {
    // Override provided, skip calculations and return override values
    return {
      slowdown: overrideSlowdown,
      onetime: true,
      countMoreThanOne: false,
      selfImposed: false,
      modifier: 0,
      top_id: null, // If the slowdown is manually set, there is no 'top_id'
      count: 0
    };
  }

  const slowdownClass = new Slowdown(user);
  let slowdown = ensureBrainKeyExists('slowdown', user.id);
  let modifier = await slowdownClass.getCountModifierSeconds();
  let s = 0, countMoreThanOne = false, selfImposed = false, top_id = null, count = 0;

  if (!(_.isEmpty(slowdown)) || modifier !== 0) {
    top_id = await slowdownClass.getSlowRecordSetterId();
    count = await slowdownClass.getSlowRecordCount();
    s = await slowdownClass.getSlowRecordSeconds();
    countMoreThanOne = count > 1;

    if (modifier > 0) {
      s = +s + +modifier;
    }

    selfImposed = top_id && slowdown[top_id]?.self;
  }

  return {
    slowdown: s,
    onetime: false,
    countMoreThanOne,
    selfImposed,
    modifier,
    top_id,
    count
  };
}


export const increment = {
  regex: /^(\d+)$/,
  tag_bot: false,
  execute: async function(message, bot) {
    if (shame_room === message.channel.id) {
      const user = message.author;
      const shame = await bot.shame.forUser(user.id);
      // Check if the user is currently being punished
      if (!shame.isCounting() && !shame.hasCounted()) {
        message.reply('Did you read the directions?');
        return;
      }

      // if they stopped, they need to start over
      if (!shame.isCounting() && shame.hasCounted()) {
        message.reply('You need to start over, remember?');
        return;
      }

      // reset shame expiration
      if (shame.isLessThanTwoHours()) {
        shame.expiresInMs = 7200000 // 2 hours
        // not saving the brain because it will be saved later.
      }

      const userCount = shame.count;
      const targetCount = shame.countingTarget;

      if (parseInt(message.content) === userCount + 1) {
        shame.incrementCount();
        await brain.write();
        message.react('✅');

        if (shame.count >= targetCount) {
          await shame.finishedCounting();
          message.channel.send("You made it! You're released from the Corner of Shame! " +
            "Let's not repeat the same mistakes, shall we?");

          // Delete all messages that are not pinned
          await delay(10000); // 10 second delay
          await deleteAllUnpinnedMessages(message.channel);
        }
      } else {
        message.react('❌');
        // Get the user's current shame record
        const shame = await bot.shame.forUser(user.id);
        // If there's a mistake penalty, apply it
        if (shame.mistakePenalty) {
          shame.countingTarget += shame.mistakePenalty;
          message.reply(`Wrong! Your next number is ${userCount + 1}. Your target has increased to ${shame.countingTarget}.`);
        } else {
          message.reply(`Wrong! Your next number is ${userCount + 1}`);
        }
      }
    }
  }
};

/**
 * Anything said will match on this, check to see if it's related to shame, and act appropriately
 */
export const other_talk = {
  regex: /.*$/,
  tag_bot: false,
  execute: async function (message, bot) {
    const user = message.author;
    const shame = await bot.shame.forUser(user.id);
    if (shame_room === message.channel.id) {
      // Ignore list, do not react to these messages said in the shame channel
      const ignoreList = [/^\d+$/, /^\?punish$/i, /^\?waitcorner$/i, /^\??stoppunish$/i];

      // Check if the message matches any pattern in the ignore list
      const isIgnored = ignoreList.some(ignorePattern => ignorePattern.test(message.content));
      if (!isIgnored && shame.isCounting()) {
        // Not ignored. User is supposed to be counting. Who said they could do that!?
        await addShameCountingViolation(message, 'Who said you could talk!', bot);
      }
    } else {
      // things said elsewhere
      // if user is actively in a punishment
      if (shame.isCounting()) {
        await addShameCountingViolation(message, 'Shouldn\'t you be counting?', bot);
      }
    }
  }
};

async function addShameCountingViolation(message, reply, bot) {
  message.react('❌');
  const user = message.author;
  const shame = await bot.shame.forUser(user.id);
  shame.incrementViolations();
  const violations = shame.violations;
  await brain.write();
  reply += ' A violation has been recorded to this punishment.\n';
  reply += `You currently have ${violations} violation${violations > 1 ? 's' : ''}. Penalties grow with the violation count.\n\n`
  if (violations > 1) {
    // add 5 * violation to count
    let penalty = (violations - 1) * 5;
    const shame = await bot.shame.forUser(user.id);
    shame.countingTarget += penalty;
    await brain.write();
    reply += `**A penalty of ${penalty} has been added to your count. You now need to count to ${shame.countingTarget}**`;
  } else {
    reply += 'The first violation was free. You need to work off your shame!\n\n' +
      'Further violations will result in an increased count. If you need help run ' +
      '`stoppunish` in <#928783575627751505> to stop counting out and be free to talk again. ' +
      'But that will start you over when you\'re ready to go again.\n\n' +
      'Running `stoppunish` will *not* reset your violations for this session.';
  }
  message.reply(reply);
}

export const stoppunish = {
  regex: /^\??stoppunish$/i,
  tag_bot: false,
  execute: async function(message, bot) {
    if (shame_room === message.channel.id) {
      const user = message.author;
      const shame = await bot.shame.forUser(user.id);
      // Only the punished user can stop the punishment
      if (shame.isCounting()) {
        shame.stopCounting();
        await brain.write();
        message.reply('Stopping. Make sure to try again.');
      } else {
        message.reply("You're not currently counting in the Corner of Shame!");
      }
    }
  }
};
