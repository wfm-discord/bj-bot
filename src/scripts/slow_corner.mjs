// Description:
//    Slow users down in the counting room with consent
//
// Commands:
//    ?punish
//    ^slow\s?corner\??$
//    hubot slow\s?corner @
//    hubot slow my corner

import { MessageStacker } from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';
import {topHasConsentByID} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import { ensureBrainKeyExists, brain } from 'bot-commons-utils/src/utils/brain.mjs';
import { getFirstUserIdFromMentionOnRespond } from 'bot-commons-utils/src/utils/users.mjs';
import _ from 'lodash';

const admin_room = process.env.ADMIN_ROOM;

// TODO: remove slowdowns (just set to 0, really, but I want to delete it)
// TODO: see set slowdowns
// TODO: Reset after punishment over?


/**
 * slowdown audit
 */
export const slowdownAudit = {
  regex: /^slowdown audit$/i,
  tag_bot: false,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel) {
      message.reply("You can't do that here.");
    } else {
      const list = ensureBrainKeyExists('slowdown');
      const stacker = new MessageStacker(message, '');
      for (let slowed_id in list) {
        for (let slow_setter_id in list[slowed_id]) {
          if (list[slowed_id][slow_setter_id].seconds === '0') {
            // remove all 0 records
            await stacker.appendOrSend(`<@${slowed_id}>: **${list[slowed_id][slow_setter_id].seconds}s** Set by <@${slow_setter_id}>. REMOVED FOR BEING 0!\n`);
            delete brain.data.slowdown[slowed_id][slow_setter_id];
            await brain.write();
          } else {
            await stacker.appendOrSend(`<@${slowed_id}>: **${list[slowed_id][slow_setter_id].seconds}s** Set by <@${slow_setter_id}>.\n`);
          }
        }
      }
      await stacker.finalize();
    }
  }
}

/**
 * slowcorner?
 */
export const slowcorner = {
  regex: /^slow\s?corner\??$/,
  tag_bot: false,
  execute: function(message) {
    message.reply('Slow down the corner so they can\'t run through it so fast. '
      + 'Use `@BJ slowcorner @user 3` to slow that user messages down '
      + 'to one every 3 seconds (*requires consent*). Use however many seconds you want! :smiling_imp:\n\n'
      + 'Slow yourself down with `@BJ slow my corner 3` (Change 3 to however many seconds you want)\n\n'
      + 'New changes to the Punisher bot may increase one\'s punishment up to 250, '
      + 'so here is what you can expect a slowdown to do '
      + '(these are MINIMUM times, and could take much longer depending on the user):\n'
      + `:small_blue_diamond:0: however fast discord will let them
:small_blue_diamond:1: 1-5 minutes
:small_blue_diamond:2: 3-9m
:small_blue_diamond:3: 5-13m
:small_blue_diamond:4: 6-17m
:small_blue_diamond:5: 8-21m
:small_blue_diamond:6: 10-25m
:small_blue_diamond:7: 11-30m
:small_blue_diamond:8: 13-34m
:small_blue_diamond:9: 15-38m
:small_blue_diamond:10: 16-42m
:small_blue_diamond:11: 18-45m
:small_blue_diamond:12: 20-50m
:small_blue_diamond:13: 21-54m
:small_blue_diamond:14: 23-58m
:small_blue_diamond:15: 25-63m
etc.
We don't recommend more than maybe a 6 second slow down, but you do you, Boo.

Slowmode goes into effect when a user uses the \`?punish\` command. Two users doing punishments at the same time can mess with each other's slow mode.
`
    );
  }
}

/**
 * slowcorner @user (time)
 */
export const slowCornerForUser = {
  regex: /^slow\s?corner\s+(<@|\d+)/i,
  tag_bot: true,
  execute: async function(message, bot) {
    // Get user ID
    let mentioned_user_id = getFirstUserIdFromMentionOnRespond(message);
    if (!mentioned_user_id) {
      message.reply('Who? I didn\'t catch who you want me to slow down.');
    } else {
      let sender = message.author; // User object

      // Check consent
      let seconds = getDigits(message); // How many seconds is the message for?
      if (await topHasConsentByID(bot, sender.id, mentioned_user_id) || (seconds !== false && seconds[0] === '0')) {
        // If there's consent OR the message is 0
        // You should be able to clear your set slowdown even if there's no consent
        if (!seconds) {
          message.reply('Did you forget to tell me how many seconds to slow them for?');
        } else {
          // User it's for
          let user_id = getFirstUserIdFromMentionOnRespond(message);
          // Get the brain key
          let slowdown = ensureBrainKeyExists('slowdown', user_id, sender.id);
          if (seconds[0] === 0) {
            // If you're clearing the slowdown
            seconds = slowdown.seconds;
            // No matter the message, delete the record.
            // We created it earlier even if it was empty.
            delete brain.data.slowdown[user_id][sender.id];
            await brain.write();
            if (seconds) {
              // Seconds existed, delete them
              message.reply(`Your slowdown for <@${user_id}> was ${seconds}s, and has been cleared.`);
            } else {
              // Nothing was set
              message.reply('Your didn\'t have a slowdown set for that user to clear.');
            }
          } else {
            // If you're setting a slowdown
            brain.data.slowdown[user_id][sender.id] = {
              'set_by': sender.id,
              'seconds': seconds[0],
              'self': (user_id === sender.id)
            };
            await brain.write();
            message.reply(`A slowdown has been added to <@${user_id}> of ${seconds[0]} seconds for when they have to count in the corner.`);
          }
        }
      } else {
        message.react('🚫');
        message.reply('You do not have consent to rate limit that user.');
      }
    }
  }
}

/**
 * slow my corner (time)
 */
export const slowMyCorner = {
  regex: /^slow my corner/i,
  tag_bot: true,
  execute: async function(message) {
    let digits_1 = message.content.match(/corner \d+/);
    if (!digits_1) {
      message.reply('Did you forget to tell me how many seconds to slow you for?');
    } else {
      let seconds = digits_1[0].match(/\d+/);
      // get user id
      let sender = message.author; // User object

      // add to brain
      ensureBrainKeyExists('slowdown', sender.id, sender.id);
      if (seconds[0] === 0) {
        delete brain.data.slowdown[sender.id][sender.id];
        message.reply('Your slowdown has been removed.');
        await brain.write();
      } else {
        brain.data.slowdown[sender.id][sender.id] = {
          'set_by': sender.id,
          'seconds': seconds[0],
          'self': true
        };
        await brain.write();
        message.reply(`I've added a slowdown to you of ${seconds[0]} for when you count in the corner.`);
      }
    }
  }
}

/**
 * check slowdowns
 */
export const checkSlowdowns = {
  regex: /^check slowdowns?$/i,
  tag_bot: true,
  execute: async function(message) {
    let sender_id = message.author.id;
    let guild = message.guild;

    let all_slowdowns = ensureBrainKeyExists('slowdown');

    let slowdowns_set_by_user = {};
    for (let user_id in all_slowdowns) {
      if (all_slowdowns[user_id][sender_id]) {
        slowdowns_set_by_user[user_id] = all_slowdowns[user_id][sender_id];
      }
    }

    if (_.isEmpty(slowdowns_set_by_user)) {
      message.reply("You haven't set any slowdowns on others.");
    } else {
      let slowdowns_report = "Slowdowns you've set on others:\n";

      for (let user_id in slowdowns_set_by_user) {
        try {
          let member = await guild.members.fetch(user_id);
          let displayName = member.displayName;
          let seconds = slowdowns_set_by_user[user_id].seconds;
          let self = slowdowns_set_by_user[user_id].self;

          slowdowns_report += `${displayName}: **${seconds}s**${self ? ' (self-set)' : ''}\n`;
        } catch (error) {
          // user not in guild anymore
          // console.error(`Error fetching member with ID ${user_id}: ${error}`);
        }
      }

      message.reply(slowdowns_report);
    }
  }
}

function getDigits(message) {
  let digits = message.content.match(/(>|corner)\s+\d+/);
  if (!digits) {
    return false;
  } else {
    return digits[0].match(/\d+/);
  }
}
