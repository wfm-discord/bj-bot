// plusplus.mjs

// Description:
//   Give or take away points. Keeps track and even prints out graphs.
//
//   This is a port over from https://github.com/ajacksified/hubot-plusplus
//   Some things are the same, some things are changed.
//
// Dependencies:
//   "underscore": ">= 1.0.0"
//   "clark": "0.0.6"
//
// Configuration:
//   HUBOT_PLUSPLUS_KEYWORD: the keyword that will make hubot give the
//   score for a name and the reasons. For example you can set this to
//   "score|karma" so hubot will answer to both keywords.
//   If not provided will default to 'score'.
//
//   HUBOT_PLUSPLUS_REASON_CONJUNCTIONS: a pipe separated list of conjunctions to
//   be used when specifying reasons. The default value is
//   "for|because|cause|cuz|as", so it can be used like:
//   "foo++ for being awesome" or "foo++ cuz they are awesome".
//
// Commands:
//   <name>++ [<reason>] - Increment score for a name (for a reason)
//   <name>-- [<reason>] - Decrement score for a name (for a reason)
//   hubot score <name> - Display the score for a name and some of the reasons
//   hubot top <amount> - Display the top scoring <amount>
//   hubot bottom <amount> - Display the bottom scoring <amount>
//   hubot erase <name> [<reason>] - Remove the score for a name (for a reason)


import ScoreKeeper from '../utils/classes/scoreKeeper.mjs';
import _ from 'lodash';
import clark from 'clark';

const scoreKeeper = new ScoreKeeper();
const scoreKeyword = process.env.HUBOT_PLUSPLUS_KEYWORD || 'score';
const reasonsKeyword = process.env.HUBOT_PLUSPLUS_REASONS || 'raisins';
const reasonConjunctions = process.env.HUBOT_PLUSPLUS_CONJUNCTIONS || 'for|because|cause|cuz|as';

export const plusPlus = {
  regex: new RegExp(
    `^([\\s\\w'@.\\-:\\u3040-\\u30FF\\uFF01-\\uFF60\\u4E00-\\u9FA0]*)(\\s*\\+\\+|\\s*--|\\s*—)(?:\\s+(?:${reasonConjunctions})\\s+(.+))?$`,
    'i'
  ),
  tag_bot: false,
  execute: async function(msg) {
    // Extracting variables from the message using the regex
    const match = msg.content.match(this.regex);

    const [dummy, name, operator, reason] = match;
    const from = msg.author.username.toLowerCase();
    const room = msg.channel.id;

    // Sanitize reason
    const trimmedReason = reason?.trim().toLowerCase();

    let sanitizedName = name;
    if (sanitizedName) {
      if (sanitizedName.charAt(0) === ':') {
        sanitizedName = sanitizedName.replace(/(^\s*@)|([,\s]*$)/g, '').trim().toLowerCase();
      } else {
        sanitizedName = sanitizedName.replace(/(^\s*@)|([,:\s]*$)/g, '').trim().toLowerCase();
      }
    }

    // Check whether a name was specified. Use MRU if not
    if (!sanitizedName || sanitizedName === '') {
      let lastReason;
      [sanitizedName, lastReason] = scoreKeeper.last(room);
    }

    // Perform the upvote or downvote and get the new score
    let score, reasonScore;
    if (operator === '++') {
      [score, reasonScore] = await scoreKeeper.add(sanitizedName, from, room, trimmedReason);
    } else {
      [score, reasonScore] = await scoreKeeper.subtract(sanitizedName, from, room, trimmedReason);
    }

    // If we got a score, display all the things and fire off events!
    if (score !== null) {
      let message;
      if (trimmedReason) {
        if (reasonScore === 1 || reasonScore === -1) {
          message = score === 1 || score === -1
            ? `${sanitizedName} has ${score} point for ${trimmedReason}.`
            : `${sanitizedName} has ${score} points, ${reasonScore} of which is for ${trimmedReason}.`;
        } else {
          message = `${sanitizedName} has ${score} points, ${reasonScore} of which are for ${trimmedReason}.`;
        }
      } else {
        message = score === 1
          ? `${sanitizedName} has ${score} point`
          : `${sanitizedName} has ${score} points`;
      }

      msg.channel.send(message);
    }
  }
};

export const eraseCommand = {
  regex: new RegExp('(?:erase )([\\s\\w\'@.\\-:\\u3040-\\u30FF\\uFF01-\\uFF60\\u4E00-\\u9FA0]*)(?:\\s+(?:for|because|cause|cuz)\\s+(.+))?$', 'i'),
  tag_bot: true,
  execute: async function(msg) {
    const match = msg.content.match(this.regex);
    if (!match) return;

    const [__, name, reason] = match;
    const from = msg.author.username.toLowerCase();
    const user = msg.author;
    const room = msg.channel.id;
    const trimmedReason = reason?.trim().toLowerCase();

    let sanitizedName = name;
    if (sanitizedName) {
      if (sanitizedName.charAt(0) === ':') {
        sanitizedName = sanitizedName.replace(/(^\s*@)|([,\s]*$)/g, '').trim().toLowerCase();
      } else {
        sanitizedName = sanitizedName.replace(/(^\s*@)|([,:\s]*$)/g, '').trim().toLowerCase();
      }
    }

    const isAdmin = msg.member.roles.cache.some(role => role.name === 'plusplus-admin' || role.name === 'admin');

    if (isAdmin) {
      const erased = scoreKeeper.erase(sanitizedName, from, room, trimmedReason);
      if (erased) {
        const message = trimmedReason
          ? `Erased the following reason from ${sanitizedName}: ${trimmedReason}`
          : `Erased points for ${sanitizedName}`;
        msg.channel.send(message);
      }
    } else {
      msg.reply("Sorry, you don't have authorization to do that.");
    }
  }
};

export const scoreCommand = {
  regex: new RegExp(`(?:${scoreKeyword}) (for\\s)?(.*)`, 'i'),
  tag_bot: true,
  execute: async function(msg) {
    const match = msg.content.match(this.regex);
    if (!match) return;

    let name = match[2].trim().toLowerCase();

    if (name) {
      if (name.charAt(0) === ':') {
        name = name.replace(/(^\\s*@)|([,\\s]*$)/g, '');
      } else {
        name = name.replace(/(^\\s*@)|([,:\\s]*$)/g, '');
      }
    }

    const score = scoreKeeper.scoreForUser(name);
    const reasons = scoreKeeper.reasonsForUser(name);

    const reasonString = typeof reasons === 'object' && Object.keys(reasons).length > 0
      ? `${name} has ${score} points. Here are some ${reasonsKeyword}:` +
      _.reduce(reasons, (memo, val, key) => {
        memo += `\n${key}: ${val} points`;
        return memo;
      }, '')
      : `${name} has ${score} points.`;

    msg.channel.send(reasonString);
  }
};

export const topBottomCommand = {
  regex: /(top|bottom) (\d+)/i,
  tag_bot: true,
  execute: async function(msg) {
    const match = msg.content.match(this.regex);
    if (!match) return;

    const amount = parseInt(match[2], 10) || 10;
    const message = [];

    const tops = scoreKeeper[match[1]](amount);

    if (tops.length > 0) {
      for (let i = 0; i < tops.length; i++) {
        message.push(`${i + 1}. ${tops[i].name} : ${tops[i].score}`);
      }
    } else {
      message.push("No scores to keep track of yet!");
    }

    if (match[1] === 'top') {
      const graphSize = Math.min(tops.length, Math.min(amount, 20));
      message.splice(0, 0, clark(_.first(_.pluck(tops, 'score'), graphSize)));
    }

    msg.channel.send(message.join('\n'));
  }
};
