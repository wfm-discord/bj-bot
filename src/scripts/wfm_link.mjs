// Description:
// embed a task when it's posted


import {wfmUtils} from '../utils/wfm.mjs';
import {EmbedBuilder} from 'discord.js';
import {punishMassPings} from '../utils/security.mjs';

/**
 * wfm link
 */
export const wfm_link = {
  regex: /https:\/\/writeforme.org\/task\//i,
  tag_bot: false,
  execute: async function(message) {
    let matches = [...message.content.matchAll(/https:\/\/writeforme.org\/task\/[a-fA-F0-9]+(\?accessCode=[\w]+)?/ig)];
    let urls = matches.map(match => match[0]);

    for (const url of urls) {
      const { taskId, accessCode } = wfmUtils.getInfoFromUrl(url);
      const details = await wfmUtils.getTaskDetails(taskId, accessCode);
      if (details.statusText === 'Unauthorized') {
        message.reply('No preview available for passworded tasks.');
        return;
      }
      let taskDetails = await details.json();

      if (await punishMassPings(message, taskDetails.name, taskDetails.text, url)) return;

      // Check if the channel or its parent category is marked as NSFW
      const isNSFW = message.channel.nsfw || (message.channel.parent && message.channel.parent.nsfw);
      const blindFlight = taskDetails.blindFlight;

      // Create an embed message
      const embed = new EmbedBuilder()
        .setColor(0xC83F17)
        .setTimestamp()
        .setFooter({text: 'Task ID: ' + taskDetails._id})
        .setURL(url);

      if (isNSFW) {
        embed.setTitle(taskDetails.name)
          .setDescription(taskDetails.text);
      } else {
        embed.setTitle('||' + taskDetails.name + '||')
          .setDescription('||' + taskDetails.text + '||')
          .addFields({
            name: 'Why is this spoilered?',
            value: 'Because this isn\'t a NSFW room and there\'s no promise the task is SFW text.'
          });
      }

      // Adding totalLines and punishmentLines based on blindFlight
      if (!blindFlight) {
        embed.addFields(
          { name: 'Total Lines', value: String(taskDetails.totalLines), inline: true },
          { name: 'Punishment Lines', value: String(taskDetails.punishmentLines), inline: true }
        );
      } else {
        embed.addFields(
          { name: 'Total Lines', value: 'Until you are done.', inline: true },
          { name: 'Punishment Lines', value: '?', inline: true }
        );
      }

      // send the embed to the same channel
      message.channel.send({embeds: [embed]});
    }
  }
}
