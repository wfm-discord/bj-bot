// Description:
//   give the corner of shame to a user who asks for it
//
// Commands:
//   bot shame me
//   bot shame @user

import { EmbedBuilder } from 'discord.js';
import { getRandomMsg } from '../utils/failed_count_msg.mjs';

export const shame_me_hard = {
  regex: /^shame me hard/i,
  tag_bot: true,
  execute: async function (message, bot) {
    let channel = message.channel;
    let task = await getRandomMsg(message.author, message.guild);
    if (!task) return channel.send("You don't have any count messages set.");
    let embed = new EmbedBuilder()
      .setColor('#0099ff')
      .setTitle('SHAME ON YOU!')
      .addFields({name: "Here's your task.", value: task.text})
    let content = `Heads up <@${message.author.id}>! You've got a special message from <@${task.set_by}>.`;
    channel.send({content: content, embeds: [embed]});

    const shame = await bot.shame.forUser(message.author.id);
    await shame.start({
      source: 'shame_me'
    });
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_me = {
  regex: /^shame me$/i,
  tag_bot: true,
  execute: async function (message, bot) {
    const shame = await bot.shame.forUser(message.author.id);
    await shame.start({
      source: 'shame_me'
    });
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_you = {
  regex: /^shame (on )?you/i,
  tag_bot: true,
  execute: async function (message, bot) {
    message.reply('Nice try. Go to the corner for that!');
    const shame = await bot.shame.forUser(message.author.id);
    await shame.start({
      source: 'shame_me'
    });
    message.channel.send(`SHAME ON <@${message.author.id}>!`);
  }
}

export const shame_user = {
  regex: /shame <@/i,
  tag_bot: true,
  execute: async function (message) {
    message.reply('Due to recent improvements and not wanting to maintain ' +
      'them in both places, this command is no more! It has ceased to be. ' +
      'It has gone to meet its maker! It has run down the curtain and joined ' +
      'the bleedin\' choir invisible!! \n\n. . . ahem.\n\n Anyway, use `/shame`');
  }
}
