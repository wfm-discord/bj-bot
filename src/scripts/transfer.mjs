// Description:
//   transfer shame to another user
//
// Commands:
//   ?transfer @user

import {topHasConsent} from 'bot-commons-consent/src/utils/consentUtils.mjs';
import {backfires} from '../utils/backfire.mjs';

const shame_room = process.env.SHAME_ROOM;

export const transfer_shame = {
  regex: /^\?transfer /i,
  tag_bot: false,
  execute: async function (message, bot) {
    let mentions = message.mentions;
    let targetUser = mentions.users.first(); // User object

    if (!targetUser) {
      await message.react('❌');
      message.reply('Who?');
      return;
    }

    let sender = message.author;
    const shame = await bot.shame.forUser(sender.id);

    if (shame.transferBackfire) {
      message.reply(
        'Aww, that\'s cute. That blew up in your face the last time' +
        ' you tried it. Why did you think this time would be better?'
      );
      return;
    }

    // did the transfer backfire?
    if (await backfires(message.member, targetUser)) {
      await shame.backfireShameTransfer(targetUser.id);
      await message.react('💣');
      await message.reply(
          `# Backfire!\n${''
          }${sender} tried to transfer their shame to ${targetUser} but it ${''
          }backfired. Their shame has been doubled instead! 😈`
      );
      return;
    }

    let guild = message.guild;
    // Does the sender even have the shame role?
    let guildMember = await guild.members.fetch(sender.id);
    if (!guildMember.roles.cache.has(process.env.SHAME_ROLE_ID)) {
      // User does not have the shame role, fail here
      await message.react('🚫');
      message.reply("You can't transfer a role you don't have.");
      return;
    }

    // Is the sender allowed to transfer their shame?
    const source = shame.source;
    switch (source) {
      case 'cheating_on_lines':
        message.reply('You know how they say that cheaters never prosper? ' +
          'They never transfer shame either. Cheaters keep their shame to themselves! ' +
          'Wait, or count your way out.');
        return;
      case 'mass_ping':
        message.reply('Ha. Fuck you. You don\'t get to transfer ' +
          'shame away when you tried to ping everyone.');
        return;
    }

    // Is the target user already shamed?
    let targetMember = await guild.members.fetch(targetUser.id);
    if (targetMember.roles.cache.has(process.env.SHAME_ROLE_ID)) {
      // Target has the shame role, fail here
      await message.react('🚫');
      message.reply("You can't transfer shame to someone who already has it.");
      return;
    }

    // Consent check!
    if (!await topHasConsent(bot, sender, targetUser)) {
      // Target does not consent, stop here
      await message.react('🚫');
      message.reply('You do not have consent to transfer shame to that user.');
      return
    }

    // Finally... transfer the SHAME
    await shame.transferShame(targetUser.id);

    const channel = message.guild.channels.cache.get(shame_room);
    // Send in shame room
    channel.send(`<@${guildMember.id}> has passed on their `
        + `Corner of Shame to you <@${targetMember.id}>.\n`
        + 'Type `?punish` to get started. Enjoy!');
    message.delete();
  }
}
