// Description:
//    Track user WFM profiles
//
// Commands:
//    set wfm
//    wfm @user
//    wfm?
//    wfm profile?
import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import { getFirstUserIdFromMentionOnHear } from 'bot-commons-utils/src/utils/users.mjs';
import { wfmUtils } from '../utils/wfm.mjs';
import { MessageStacker } from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';
import {punishMassPings} from '../utils/security.mjs';


/**
 * wfm?
 */
export const wfm_info = {
  regex: /^wfm\??$/i,
  tag_bot: false,
  execute: async function (message) {
    message.channel.send('I remember WFM profiles! \nYou can set your profile using '
      + '`set wfm LINK HERE` \n'
      + 'You can retrieve anyone\'s set profile with `wfm @user`\n'
      + 'Ask `wfm profile?` if you need help finding your profile link in WFM.\n'
      + 'If you ever want to remove your profile link you can use `set wfm` without any link and it will erase your entry'
    );
  }
}

/**
 * wfm profile?
 */
export const wfm_profile_info = {
  regex: /^wfm profile\??$/i,
  tag_bot: false,
  execute: async function (message) {
    message.channel.send(`You can't just go to "show my profile" to get your profile link in WFM. Here are some ways you can find your profile link:
  * Use the "User Search" in the left hamburger menu of the site.
  * Make a task and go to it. Click on your name.
  * Complete a task and find your name on the list of people who have completed it and click on it.
  * Make a post somewhere in the forums and click on your name on your post.

Ask if you need more help. =)`
    );
  }
}

/**
 * set wfm (link)
 */
export const set_wfm = {
  regex: /^set wfm\s?/i,
  tag_bot: false,
  execute: async function (message) {
    ensureBrainKeyExists('wfm_profiles');
    if (message.content.replace(/^set wfm\s?/i, '') === '') {
      delete brain.data.wfm_profiles[message.author.id];
      message.reply('I\'ve removed your profile link.');
    } else {
      let link = message.content.replace(/^set wfm /i, '');
      link = link.trim();
      if (link.match(/LINK/i)) {
        message.reply('You need to replace `LINK` with your WFM profile link. Use `wfm profile` if you need help finding your link.');
      } else if (link === 'https://writeforme.org/profile/') {
        message.reply('I\'m sorry that\'s the page everyone views their own profiles on. Use `wfm profile` if you need help finding your link.');
      } else if (await punishMassPings(message, link)) {
        // handled by punishMassPings
      } else if (!link.startsWith('https://writeforme.org/')) {
        message.reply('That didn\'t look like a valid link. ' +
          'Please make sure it starts with "https://writeforme.org/"');
      } else {
        brain.data.wfm_profiles[message.author.id] = link;
        await brain.write();
        await wfmUtils.createUserProfileEmbed('I will remember your profile is '
        + link
        + '\nAnyone can call it up using "wfm <@'
        + message.author.id
        + '>"', message, message.author.id);
      }
    }
  }
}

/**
 * wfm @user
 */
export const wfm_user = {
  regex: /^wfm <@/i,
  tag_bot: false,
  execute: async function (message) {
    let mentioned_user_id = getFirstUserIdFromMentionOnHear(message);
    await wfmUtils.createUserProfileEmbed('', message, mentioned_user_id);
  }
}

/**
 * wfm dir / directory
 */
export const wfm_dir = {
  regex: /^wfm dir(ectory)?\??$/i,
  tag_bot: false,
  execute: async function (message) {
    message.reply('Please wait while I gather and alphabetize the directory.');
    let list = ensureBrainKeyExists('wfm_profiles');
    const stacker = new MessageStacker(message);

    let profiles = [];

    // Fetch guild members concurrently
    const fetchPromises = Object.keys(list).map(async (user_id) => {
      try {
        let guildMember = await message.guild.members.fetch(user_id);
        profiles.push({ name: guildMember.displayName, profile: list[user_id] });
      } catch (error) {
        // I don't feel like showing unknown members anymore
        // stacker.appendOrSend(`Unknown member: ${list[user_id]}\n`);
      }
    });

    await Promise.all(fetchPromises);

    profiles.sort((a, b) => a.name.localeCompare(b.name));
    for (let profile of profiles) {
      await stacker.appendOrSend(`${profile.name}: ${profile.profile}\n`);
    }

    await stacker.finalize('', 'Directory is empty.');
  }
}


/**
 * wfm cleanup
 * Clean up URL mistakes that happened when users did things the code didn't trap for
 */
export const wfm_cleanup = {
  regex: /^wfm cleanup/i,
  tag_bot: false,
  execute: async function (message) {
    message.channel.send('Cleaning up wfm links...');
    let list = ensureBrainKeyExists('wfm_profiles');

    for (let user_id in list) {
      let url = list[user_id];
      if (url.match(/link/i)) {
        url = url.replace(/\s?link\s?\n?\s?/i, '');
        brain.data.wfm_profiles[user_id] = url;
        await brain.write();
      }
      if (url.match(/Set wfm /i)) {
        url = url.replace(/Set wfm /i, '');
        brain.data.wfm_profiles[user_id] = url;
        await brain.write();
      }
      if (url.match(/\s/)) {
        url = url.replace(/\s/, '');
        brain.data.wfm_profiles[user_id] = url;
        await brain.write();
      }
    }
  }
}
