// Description:
//    Tops can add additional consequences for messing up the count
//
// Commands:
//    hubot add/set {one-time} count msg  @user text
//    hubot remove count msg {id}
//    hubot see count msg
//

import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {getFirstUserIdFromMentionOnRespond} from 'bot-commons-utils/src/utils/users.mjs';
import {clearUserMessages, findMessageById} from '../utils/failed_count_msg.mjs';


/**
 * help count message
 */
export const failedCountHelp = {
  regex: /^(help\s?)?count(ing)?\s?(message|msg)s?\??$/i,
  tag_bot: false,
  execute: function(message) {
    message.reply('**Counting Messages (Punishments)**\n' +
      'Add some text that will display with the "fruiting" message. ' +
      'If more than one is added, something random will display. The original purpose of this ' +
      'is to add additional punishments when a user messes up the count, but it could be used ' +
      'for other messages as well such as encouragement, beratement, etc.\n' +
      '\n' +
      '*Requires consent to add messages.*\n' +
      '\n' +
      ':small_blue_diamond:`/failed_count_message_add` Add a message. ' +
      'It can be optionally one-time, or overwrite all existing messages.' +
      'If there\'s more than one added, the bot will choose one randomly\n' +
      '\n' +
      ':small_blue_diamond:`/failed_count_message_see` See either all you\'ve ' +
      'set for a user, or all you\'ve set for all users. Will provide IDs for removing messages from ' +
      'the list. Users cannot look up what messages have been set for them.\n' +
      '\n' +
      ':small_blue_diamond:`/failed_count_message_remove` Remove messages you set, either by ID or by user.\n'
    );
  }
}


/**
 * add/override count message (id)
 */
export const failedCountAdd = {
  regex: /^(add|override)\s?(one[-\s]?time )?count(ing)?\s?(message|msg)\s?<@/i,
  tag_bot: true,
  execute: async function(message) {
    await message.reply('Due to how complex this command is, ' +
      'it\'s been moved to a slash command only. Sorry.\n' +
      'You can use `/failed_count_message_add` to add messages now.');
  }
}


/**
 * remove count message (id)
 */
export const failedCountRemv = {
  regex: /^remove\s?count(ing)?\s?(message|msg)s? /i,
  tag_bot: true,
  execute: async function (message) {
    // Get @user from mentions
    let mentioned_user_id = getFirstUserIdFromMentionOnRespond(message);
    let sender = message.author; // User object

    if (mentioned_user_id) {
      await ensureBrainKeyExistsPromise('count_fail_msgs', mentioned_user_id);
      // A user was given!
      let delete_count = clearUserMessages(mentioned_user_id, sender.id)

      if (delete_count > 0) {
        message.reply(`${delete_count} messages cleared for <@${mentioned_user_id}>.`);
      } else {
        message.reply('No messages found to clear.');
      }
    } else {
      // Look for an ID
      let match = message.content.match(/remove\s?count(ing)?\s?(message|msg)s? \d+/is);
      let matched_id = match[0].replace(/remove\s?count(ing)?\s?(message|msg)s? /is, '');
      let found_message = findMessageById(matched_id);
      if (found_message) {
        if (sender.id === found_message.set_by) {
          // Delete found_message; I don't know why this doesn't work
          delete brain.data.count_fail_msgs[found_message.given_to][matched_id]; // Works
          await brain.write();
          await message.react('✅');
        } else {
          message.reply("Wait. You weren't the one who set that message! " +
            `Only <@${found_message.set_by}> can remove ${matched_id}!`);
        }
      } else {
        message.reply('I couldn\'t find that message.');
      }
    }
  }
}


/**
 * see count messages @user
 */
export const failedCountSee = {
  regex: /^see\s?count(ing)?\s?(message|msg)s?/i,
  tag_bot: true,
  execute: async function (message) {
    await message.reply('Due to how spammy this command is, ' +
      'it\'s been moved to a slash command only so it can be ephemeral. Sorry.\n' +
      'You can use `/failed_count_message_see` to see messages now.');
  }
}
