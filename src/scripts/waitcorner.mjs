// Description:
//    Updates your corner wait time to 2 hours from time of command
//
// Commands:
//    ?waitcorner

import {getGuildMemberByUserID} from 'bot-commons-utils/src/utils/users.mjs';

/**
 * ?waitcorner
 */
export const waitcorner = {
  regex: /^\?waitcorner/,
  tag_bot: false,
  execute: async function (message, bot) {
    let sender = message.author; // User object
    const shame = await bot.shame.forUser(message.author.id);
    if (shame.isShamed()) {
      shame.expiresInMs = 7200000; // 2 hours
      await shame.save();
      message.delete();
      message.channel.send(`<@${sender.id}> has chosen to wait for their corner of shame to end.`);
    } else {
      let guildMember = await getGuildMemberByUserID(message.guild, message.author);
      if (!guildMember.roles.cache.has(process.env.SHAME_ROLE_ID)) {
        message.reply('What? You don\'t have that role.');
      } else {
        // is user has the role but no shame record... make one!
        await shame.start({
          expiresInMs: 7200000
        });
      }
    }
  }
}
