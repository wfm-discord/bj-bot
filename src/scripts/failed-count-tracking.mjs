// Commands:
//    slow modify?
//    slow modifier?
//    bot slow opt in
//    bot slow opt out
//    bot my slowdown

import { brain, ensureBrainKeyExistsPromise } from 'bot-commons-utils/src/utils/brain.mjs';
import { Slowdown } from '../utils/classes/Slowdown.mjs';


/**
 * slow modify? / slow modifier?
 */
export const slowModifier = {
  regex: /^slow\s?modif(ier|y)\??/i,
  tag_bot: false,
  execute: function(message) {
    message.reply('Every time you miscount you\'ll type one second '
      + 'slower in the corner of shame. This slowdown isn\'t permanent — you\'ll lose '
      + 'one second every week. **These seconds add on to any slowdown set by another '
      + 'user!** Everyone is opted into this system by default, but you can opt out '
      + 'if you prefer. You can also also DM Bad Janet if you don\'t want people '
      + 'people to know you\'re a huge chicken. 🐔'
      + '\n'
      + '\n `@BJ my slow down` to see your current modifier.'
      + '\n `@BJ slow opt out` to opt out of this system.'
      + '\n `@BJ slow opt in` to opt back into this system.'
    );
  }
}

/**
 * hubot slow opt out
 */
export const slowOptOut = {
  regex: /^slow\s?opt\s?out/i,
  tag_bot: true,
  execute: async function(message) {
    let author = message.author;

    if (!brain.data.count_fail_opt_out[author.id]) {
      brain.data.count_fail_opt_out[author.id] = {
        'id': author,
        'opt_out': new Date()
      }

      await brain.write();
      await message.react('✅');
      message.reply('You\'re now opted out of the slowdown. 🐔');
    } else {
      await message.react('🚫')
      message.reply('You\'re already opted out of the slowdown. 🐔');
    }
  }
}

/**
 * hubot slow opt in
 */
export const slowOptIn = {
  regex: /^slow\s?opt\s?in/i,
  tag_bot: true,
  execute: async function(message) {
    let author = message.author;

    if (brain.data.count_fail_opt_out[author.id]) {
      delete brain.data.count_fail_opt_out[author.id];

      await brain.write();
      await message.react('✅');
      message.reply('You\'re now opted back into the slowdown.');
    } else {
      await message.react('🚫')
      message.reply('You\'re already opted into the slowdown.');
    }
  }
}

/**
 * hubot my slowdown
 */
export const mySlowDown = {
  regex: /^my\s?slow\s?down/i,
  tag_bot: true,
  execute: async function(message) {
    let author = message.author;
    await ensureBrainKeyExistsPromise('slowdown', author.id);

    let countSlowdown = 0;
    let userSlowdown;
    let setter;

    await ensureBrainKeyExistsPromise('count_fail_opt_out');
    await ensureBrainKeyExistsPromise('count_fail_tracking');

    // Set the reply for the miscounting part of the string
    let countReply;
    const slowdownClass = new Slowdown(author, message.guild);
    if (brain.data.count_fail_tracking[author.id]) {
      countSlowdown = await slowdownClass.getCountModifierSeconds();
      if (countSlowdown === 0) {
        countReply = 'Your first miscount is on the house.'
      } else {
        countReply = `${countSlowdown} seconds are from miscounting.`;
      }
    } else {
      countReply = 'You haven\'t miscounted yet.';
    }

    // Find the highest slowdown set by another user
    userSlowdown = await slowdownClass.getSlowRecordSeconds();
    const setterId = await slowdownClass.getSlowRecordSetterId();
    if (setterId !== null) {
      setter = await message.guild.members.fetch(setterId);
      setter = setter.displayName;
    } else {
      setter = '';
    }

    // Set the reply for slowdowns set by other users
    let userReply;
    if (userSlowdown > 0) {
      userReply = `You can thank **${setter}** for the other ${userSlowdown} 💖`;
    } else {
      userReply = 'No one\'s slowed you down yet';
    }

    // If the user has slowdown from miscounting or another user
    let reply;
    if (countSlowdown > 0 || userSlowdown > 0) {
      // Create the full reply
      reply = `Your total slowdown is **${+countSlowdown + +userSlowdown}** seconds.`
        + `\n${countReply}`
        + `\n${userReply}`;
    } else {
      reply = 'You don\'t have any slowdown. Good for you!';
    }

    // Send the completed reply based on if the user opted out or not
    if (brain.data.count_fail_opt_out[author.id]) {
      message.reply('You have opted out of the slowdown.\nYou can opt back in with `slow opt in`.');
    } else {
      message.reply(reply);
    }
  }
}
