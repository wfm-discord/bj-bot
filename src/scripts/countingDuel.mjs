// /src/scripts/countingDuel.mjs
import {MessageCollector} from 'discord.js';
import {delay, deleteAllUnpinnedMessages} from 'bot-commons-utils/src/utils/messageUtils.mjs';

export const countingDuel = {
  regex: /^\?countingDuel/i,
  tag_bot: false,
  execute: async function (message) {
    const channelID = process.env.COUNTING_DUEL_CHANNEL_ID;
    if (message.channel.id !== channelID) {
      return;
    }

    const player1 = message.author;
    const player2 = message.mentions.users.first();
    if (!player2) {
      return message.reply('Please mention another player to start the duel.');
    }

    if (player2.bot) {
      return message.reply('You cannot duel with a bot. Please mention a real player.');
    }

    const INITIAL_TIME_LIMIT = 60000; // 60 seconds
    let count = 1;
    let toPlay = player1;
    let notToPlay = player2;
    let gameStatus = true;
    let gameEnded = false; // Flag to ensure the game ends only once

    await message.channel.send(`**Counting-Duel** between ${player1} and ${player2} is started!\n${player1} Please start with the first number within 60 seconds:`);

    const initialFilter = msg => msg.author.id === player1.id;
    const initialCollector = new MessageCollector(message.channel, { filter: initialFilter, time: INITIAL_TIME_LIMIT });

    initialCollector.on('collect', async msg => {
      if (gameEnded) return;

      const number = parseInt(msg.content);
      if (isNaN(number) || number !== 1) {
        await message.channel.send(`That is not a valid starting number, ${player1}. Please start with the first number.`);
        initialCollector.stop();
        return;
      }

      await msg.react(count % 2 === 0 ? '✅' : '☑️');
      count += 1;
      // Switch which player is expected to play next
      [toPlay, notToPlay] = [player2, player1];
      initialCollector.stop();

      const filter = msg => [player1.id, player2.id].includes(msg.author.id);
      const collector = new MessageCollector(message.channel, { filter, time: 2500 });

      collector.on('collect', async msg => {
        if (gameEnded) return;

        if (msg.author.id !== toPlay.id) return;

        const number = parseInt(msg.content);
        if (isNaN(number) || number !== count) {
          if (!gameEnded) {
            gameEnded = true;
            await endGame(message, `Wrong number ${toPlay}!\n**Game Over!!!** ${notToPlay} is the **WINNER!**`);
          }

          gameStatus = false;
          collector.stop();
          return;
        }

        await msg.react(count % 2 === 0 ? '✅' : '☑️');
        // Switch which player is expected to play next
        [toPlay, notToPlay] = [notToPlay, toPlay];
        count += 1;
        collector.resetTimer();
      });

      collector.on('end', async collected => {
        if (gameStatus && !gameEnded) {
          gameEnded = true;
          await endGame(message, `Too late ${toPlay}!\n**Game Over!!!** ${notToPlay} is the **WINNER!**`);
        }
      });
    });

    initialCollector.on('end', async collected => {
      if (!gameEnded && collected.size === 0 && gameStatus) {
        gameEnded = true;
        await endGame(message, `${player1} did not start the game in time. **Game Over!!!** ${player2} is the **WINNER!**`);
      }
    });
  }
}

async function endGame(message, text) {
  await message.channel.send(text);
  await message.channel.send('This channel will clear in 30 seconds.');
  // Delete all messages that are not pinned
  await delay(30000); // 30 second delay
  await deleteAllUnpinnedMessages(message.channel);
}
