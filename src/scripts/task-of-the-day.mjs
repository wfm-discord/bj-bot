import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {MessageStacker} from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';
import {postRandomTask} from '../utils/task-of-the-day.mjs';
import { wfmUtils } from '../utils/wfm.mjs';

/**
 * Adds tasks to the brain.
 * You can add multiple tasks using "add task" as the splitter.
 * Feel free to add line breaks between tasks
 * for easy review but it's not necessary.
 *
 * Each task should be added using correct JSON
 */
/**
 * add task (ID)
 */
export const addTask = {
  regex: /^add task [.\s\S]+/i,
  tag_bot: true, // Assuming this means it responds directly to bot commands
  execute: async function(message) {
    const adminRoom = process.env.DAILY_TASK_ADMIN_ROOM;
    if (adminRoom && adminRoom !== message.channel.id) {
      message.reply("You can't add tasks here.");
      return;
    }

    ensureBrainKeyExists('taskoftheday');
    let brainData = brain.data;
    let inputMessage = message.content;
    let tasks = inputMessage.split('add task ').slice(1); // Skip the command part

    for (const taskString of tasks) {
      try {
        let task = JSON.parse(taskString.trim());
        const { taskId, accessCode } = wfmUtils.getInfoFromUrl(task.url); // Utilize the utility function to parse the URL

        // Fetch task details from WFM API
        const detailsResponse = await wfmUtils.getTaskDetails(taskId, accessCode);
        if (detailsResponse.ok) {
          const taskDetails = await detailsResponse.json();
          // Extend the task object with additional details from WFM
          task = {
            ...task,
            name: taskDetails.name,
            text: taskDetails.text,
            totalLines: taskDetails.totalLines,
            punishmentLines: taskDetails.punishmentLines,
            blindFlight: taskDetails.blindFlight,
          };

          if (brainData.taskoftheday[taskId]) {
            // If this task already exists, update it
            brainData.taskoftheday[taskId] = { ...brainData.taskoftheday[taskId], ...task };
            message.reply(`Task ${taskId} updated with additional details.`);
          } else {
            // If this task doesn't exist, add it
            brainData.taskoftheday[taskId] = task;
            message.reply(`Task ${taskId} added with additional details.`);
          }
          await brain.write();
        } else {
          message.reply(`Failed to fetch details for task ${taskId} from WFM.`);
        }
      } catch (error) {
        console.error(error); // Log the error for debugging
        message.reply("Error processing task. Ensure it's valid JSON and includes a valid URL.");
      }
    }
  },
};



/**
 * remove task (ID)
 * You can use `list tasks` to get task IDs for removal.
 */
export const removeTask = {
  regex: /^remove task [\d|a-z|A-Z]+/i,
  tag_bot: true,
  execute: async function(message) {
    const adminRoom = process.env.DAILY_TASK_ADMIN_ROOM;
    if (adminRoom && adminRoom !== message.channel.id) {
      message.reply("You can't remove tasks here.");
      return;
    }

    ensureBrainKeyExists('taskoftheday');
    let brainData = brain.data;
    let inputMessage = message.content;
    let taskId = inputMessage.split('remove task ')[1].trim();

    if (brainData.taskoftheday[taskId]) {
      delete brainData.taskoftheday[taskId];
      message.reply('Task removed.');
    } else {
      message.reply(`Could not find task ${taskId} to remove.`);
    }

    await brain.write();
  },
};



/**
 * list tasks
 * List the tasks currently in the robot brain into the room.
 * IDs are listed so that you can remove tasks if you need to.
 */
export const listTasks = {
  regex: /^list tasks/i,
  tag_bot: true,
  execute: async function(message) {
    const adminRoom = process.env.DAILY_TASK_ADMIN_ROOM;
    if (adminRoom && adminRoom !== message.channel.id) {
      message.reply("You can't list tasks here.");
      return;
    }

    ensureBrainKeyExists('taskoftheday');
    let brainData = brain.data;
    const stacker = new MessageStacker(message, 'Tasks:\n');

    for (const key of Object.keys(brainData.taskoftheday)) {
      let task = brainData.taskoftheday[key];
      await stacker.appendOrSend(`* ${key}: ${task.url}\n`);
    }

    await stacker.finalize('', 'No tasks. Please add some!');
  },
};




/**
 * post task
 * This adds the ability to post a task at any time.
 * It DOES NOT reset the cron job, so if you do it an hour before
 *  the task is scheduled, it will post a new task on schedule.
 *
 * This is mostly for testing.
 */
export const postTask = {
  regex: /^post task/i,
  tag_bot: true,
  execute: async function(message) {
    const adminRoom = process.env.DAILY_TASK_ADMIN_ROOM;
    if (adminRoom && adminRoom !== message.channel.id) {
      message.reply("You can't do that here.");
      return;
    }

    await postRandomTask();
  },
};
