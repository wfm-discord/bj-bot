// Description:
//   Asks a question every day
//
// Commands:
//   @bot add question
//   @bot remove question
//   @bot list questions
//   @bot ask question

import {bot} from '../services/bot.mjs';
import cron from 'node-cron';
import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import _ from 'lodash';

const run_time = process.env.QOTD_CRON || "1 0 0 * * *"; //12:01am every day
const admin_room = process.env.QOTD_ADMIN_ROOM;

cron.schedule(run_time, async () => {
  await askQuestion();
});

/**
 * Adds questions to the brain.
 * You can add multiple questions using "add question" as the splitter.
 * Feel free to add line breaks between questions
 *  for easy review, but it's not necessary.
 */
export const addQuestion = {
  regex:/add question [.\s\S]+/im,
  tag_bot:true,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't add questions here.");
      return;
    }
    const qotd = await ensureBrainKeyExistsPromise('qotd');

    //"add question" is the splitter
    let questions = message.content.split("add question ");

    // Brain is an object not an array, so I needed to make something up
    // for the index. Timestamp was the easiest thing.
    let index = (new Date()).getTime();

    for (let key in questions) {
      if (key > 0) {
        //skip first key
        index++;
        //prevent possible collisions if adding questions too fast
        index = preventCollisions(index, qotd);
        qotd[index] = questions[key].trim();
      }
    }
    await brain.write();
    message.reply("Question(s) added.");
  }
}

/**
 * Removes a question using its ID.
 * You need to use `list questions` to get question IDs for removal.
 */
export const removeQuestion = {
  regex:/remove question \d+/i,
  tag_bot:true,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't remove questions here.");
      return;
    }
    const qotd = await ensureBrainKeyExistsPromise('qotd');
    let id = message.content.match(/\d+$/)[0];
    if (qotd[id]) {
      delete qotd[id];
      message.reply("Question removed.");
    } else {
      message.reply("Could not find requested question to remove.");
    }
    await brain.write();
  }
}

/**
 * List the questions currently in the brain into the room.
 * IDs are listed so that you can remove questions if you need to.
 */
export const listQuestions = {
  regex:/list questions/i,
  tag_bot:true,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't list questions here.");
      return;
    }
    const qotd = await ensureBrainKeyExistsPromise('qotd');

    var list = "Questions: \n";
    var temp_list = "";
    for (let key in qotd) {
      temp_list = list;
      list = list + `${key}: ${qotd[key]} \n`;
      if (list.length > 2000) {
        message.reply(temp_list);
        list = "";
      }
    }
    if (list === '') {
      message.reply("No questions. Please add some!");
    } else {
      message.reply(list);
    }
  }
}

/**
 * This adds the ability to ask a question at any time.
 * It DOES NOT reset the cron job, so if you ask it an hour before
 *  the question is scheduled, it will ask a new question on schedule.
 *
 * This is mostly so that if we run out of questions we can reload it
 *  then ask a new question rather than leave it at no question for the day
 *  or have to ask manually.
 */
export const askQuestionScript = {
  regex:/ask question/i,
  tag_bot:true,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't do that here.");
      return;
    }
    await askQuestion();
  }
}

/**
 * Function to actually ask the question.
 * Needed so I can ask on a schedule
 *  and also on command without duplicate code.
 */
async function askQuestion() {
  const qotd = await ensureBrainKeyExistsPromise('qotd');
  const randomQuestionId = _.sample(Object.keys(qotd));

  const qotdChannel = await bot.client.channels.fetch(process.env.QOTD_ROOM);
  if (!(randomQuestionId)) {
    await qotdChannel.send("No questions left. Someone should ask one!");
    return;
  }
  const question = qotd[randomQuestionId];
  await qotdChannel.send(`**QOTD: ${question}**`);
  delete qotd[randomQuestionId];
  await brain.write();
}

/**
 * Checks to see if an ID already exists in the brain
 * Increments if it exists and loops
 *   until it finds something that doesn't exist.
 * @param id int
 * @param brain object
 * @return id
 */
function preventCollisions(id, brain) {
  if (brain[id]) {
    id++;
    return preventCollisions(id, brain);
  }
  return id;
}
