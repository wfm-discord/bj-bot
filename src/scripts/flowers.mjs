import { EmbedBuilder } from 'discord.js';
import _ from 'lodash';
import {ensureBrainKeyExistsPromise, brain} from 'bot-commons-utils/src/utils/brain.mjs';
import {MessageStacker} from 'bot-commons-utils/src/utils/classes/MessageStacker.mjs';

const admin_room = process.env.BOT_ADMIN_ROOM;

const flowers = [
  'https://cdn.discordapp.com/attachments/928079339482923010/928256427204698162/1e0e705d-5d7e-4d66-b097-154e0d57de20_1.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256427640897536/flower-shop-always-on-my-mind-65-167398.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256427921899520/Deluxe_PurpleFlowerBouquet.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256428119035934/sku4601408.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256428756586506/Deluxe_PinkFlowerBouquet.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256428962091058/flower-bouquet-delivery-in-athens.png',
  'https://cdn.discordapp.com/attachments/928079339482923010/928256429327024138/Diamond-Embroidery-Beautiful-Flower-Bouquet-Picture-of-Rhinestone-Full-Square-Diamond-Mosaic-Floral-Series-Bedroom-Decor.png',
]

export const sendFlowers = {
  regex: /^\?flowers/i,
  tag_bot: false,
  execute: async function (message, bot) {
    let content = message.content;

    // Get front half of the message
    let front_half = content.match(/^\?flowers <@\d+>\s?/i);

    if (!front_half) {
      message.reply('Flowers command not formed correctly. The command is `?flowers @usertag Optional message`');
    } else {
      let mentions = message.mentions;
      let to_user = mentions.users.first(); // User object
      let card = content.replace(front_half, '');

      let sender = message.author; // User object
      let card_message = `**<@${sender.id}> has sent you flowers!**`;
      if (card) {
        card = card.replace(/\n/g, '\n> ');
        card_message = card_message + `\nThe card says: \n> ${card}`;
      }

      let chosen_flowers = _.sample(flowers);

      let block_list = await ensureBrainKeyExistsPromise('block_list');

      let color = '';
      let status = '';
      if (block_list
        && block_list[to_user.id]
        && block_list[to_user.id][sender.id]
        && block_list[to_user.id][sender.id].id
        && block_list[to_user.id][sender.id].id === sender.id
      ) {
        status = 'BLOCKED';
        color = '#ff050d';
      } else if (block_list
        && block_list[to_user.id]
        && block_list[to_user.id].all
      ) {
        message.reply('I\'m sorry, the user you are trying to send flowers to is not accepting any flowers at this time.');
        status = 'Blocked All';
        color = '#000000';
      } else {
        try {
          const embededMessage = new EmbedBuilder()
            .setColor('#bfc0ff')
            .setTitle('You\'ve got flowers!')
            .setImage(chosen_flowers)
            .setDescription(card_message)
            .setTimestamp()
            .setFooter({
              text: 'If you would like information on blocking users, send me a message `block?`, \nor `block '
                + sender.id
                + '` to block this user.'
            });
          await to_user.send({ embeds: [embededMessage] });

          status = 'Sent';
          color = '#06d622';
        } catch (error) {
          if (error.code === 50007) {
            message.reply('I\'m sorry, I cannot send messages to this user.');
            status = 'Failed - Cannot send messages to this user';
            color = '#ff050d';
          } else {
            console.error(error);
            message.reply('An error occurred while trying to send flowers.');
            status = 'Failed - Error';
            color = '#ff050d';
          }
        }
      }

      let cardLine = '';
      if (card) {
        cardLine = `Card: \n> ${card}`;
      } else {
        cardLine = 'No card message attached';
      }

      // Copy moderation channel
      const embededMessage = new EmbedBuilder()
        .setColor(color)
        .setTitle(`Flowers Sent by ${message.author.tag}`)
        .setDescription(`
        User: <@${message.author.id}>
        Original message: ${message.content}
        To: <@${to_user.id}>
        ${cardLine}`)
        .setTimestamp()
        .setFooter({
          text: `Status: ${status}`
      });
      await bot.logsChannel.send({embeds: [embededMessage]})

      message.delete();
    }
  }
}


/**
 * block?
 */
export const blockHelp ={
  regex: /^block\?/i,
  tag_bot: false,
  execute: async function(message) {
    message.reply('If someone is abusing the flowers command you can '
      + 'block them using block commands. Blocking specific users is **silent**, and '
      + 'does not tell the target they\'ve been blocked. Blocking flowers from all users '
      + 'will let anyone who tries to send you some know you aren\'t accepting them.'
      + '\n'
      + '\n`block (userID)` Block a specific user.'
      + '\n`block all` Stop accepting flowers entirely.'
      + '\n`unblock (userID)` Unblock a specific user.'
      + '\n`unblock all` Start accepting flowers again.'
      + '\n`block list` See everyone you\'ve blocked.'
    );
  }
}


/**
 * block audit
 */
export const blockAudit ={
  regex: /^block audit$/i,
  tag_bot: false,
  execute: async function(message) {
    if (admin_room && admin_room !== message.channel.id) {
      message.reply("You can't do that here.");
    } else {
      const list = await ensureBrainKeyExistsPromise('block_list');
      const stacker = new MessageStacker('');

      for (let blocker_id in list) {
        for (let blocked_id in list[blocker_id]) {
          if (blocked_id === 'all') {
            await stacker.appendOrSend(`<@${blocker_id}> has blocked **everyone**.\n`);
          } else {
            await stacker.appendOrSend(`<@${blocker_id}> has blocked <@${blocked_id}>.\n`);
          }
        }
      }
      await stacker.finalize();
    }
  }
}

  /**
   * block list
   */
export const blockList ={
  regex: /^block list/i,
  tag_bot: false,
  execute: async function(message) {
    if (message.guildId !== null) {
      message.reply("Sorry, this command can only be used in DMs.");
      message.delete();
      return;
    }

    let block_list = await ensureBrainKeyExistsPromise('block_list', message.author.id);
    let base_text = '';
    let user_list = '';
    let noOne = true;
    let split = false;

    // Add a message to the top if they blocked all flowers
    if (block_list.all) {
      base_text = '**You\'ve blocked all incoming flowers.**\nTo undo this, use `unblock all`. '
        + '\n'
        + '\n';
    }

    // List specific blocked users
    for (let key in block_list) {
      if (key !== 'all') {
        // Add the header for the list if it's not empty
        if (noOne) {
          base_text = base_text + 'You\'ve blocked these users specifically:'
          noOne = false;
        }

        // Split the message if it's over 2000 characters
        let test_message = base_text + user_list + `\n<@${block_list[key].id}>, to unblock them use \`unblock ${key}\``;
        if (test_message.length > 2000) {
          message.reply(base_text + user_list);
          base_text = '';
          user_list = '';
          split = true;
        }

        user_list += `\n<@${block_list[key].id}>, to unblock them use \`unblock ${key}\``
      }
    }

    if (noOne) {
      base_text = base_text + 'You haven\'t blocked any specific users yet. DM me `block?` for details about block users.';
      message.reply(base_text);
    } else if (!split) {
      message.reply(base_text + user_list);
    }
  }
}

  /**
   * block (user)
   */
  export const blockUser ={
    regex: /^block \d+/i,
    tag_bot: false,
    execute: async function(message) {
      if (message.guildId !== null) {
        message.reply("Sorry, this command can only be used in DMs.");
        return;
      }

      let block_list = await ensureBrainKeyExistsPromise('block_list', message.author.id);
      let text = message.content;
      let blocked_user_id = text.match(/\d+/i);
      blocked_user_id = blocked_user_id[0];

      if (block_list[blocked_user_id]) {
        message.reply(`<@${blocked_user_id}> is already blocked from sending you flowers.`);
      } else {
        block_list[blocked_user_id] = {
          'id': blocked_user_id,
          'time': new Date()
        }
        message.reply(`<@${blocked_user_id}> is now blocked from sending you flowers.`);
      }
      await brain.write();
    }
  }

  /**
   * block all
   */
  export const name ={
    regex: /^block all/i,
    tag_bot: false,
    execute: async function(message) {
      let block_list = await ensureBrainKeyExistsPromise('block_list', message.author.id);

      if (block_list.all) {
        message.reply('You\'ve already blocked everyone.');
      } else {
        block_list.all = {
          'time': new Date()
        }
        message.reply('Blocked everyone from sending you flowers.');
      }
      await brain.write();
    }
  }

/**
 * unblock (user)
 */
export const unblockUser ={
  regex: /^unblock \d+/i,
  tag_bot: false,
  execute: async function(message) {
    let block_list = await ensureBrainKeyExistsPromise('block_list', message.author.id);
    let text = message.content;
    let blocked_user_id = text.match(/\d+/i);
    blocked_user_id = blocked_user_id[0];

    if (block_list[blocked_user_id]) {
      delete block_list[blocked_user_id];
      message.reply(`<@${blocked_user_id}> can send you flowers again.`);
    } else {
      message.reply(`<@${blocked_user_id}> is already unblocked.`);
    }
    await brain.write();
  }
}


/**
 * unblock all
 */
export const unblockAll ={
  regex: /^unblock all/i,
  tag_bot: false,
  execute: async function(message) {
    let block_list = await ensureBrainKeyExistsPromise('block_list', message.author.id);

    if (block_list.all) {
      delete block_list.all;
      message.reply('You are now accepting flowers from users you haven\'t specifically blocked.');
    } else {
      message.reply('You are already accepting flowers from users you haven\'t specifically blocked.');
    }
    await brain.write();
  }
}
