// Description:
//    Listens for when someone messes up the counting game and
//    publicly shames them.
//
// Dependencies:
//    discord.js
//
// Configuration:
//    environment variable FRUITING_ROOM={room_id}
//
// Commands:
//    none

import _ from 'lodash';
import { EmbedBuilder } from 'discord.js';
import { getRandomMsg } from '../utils/failed_count_msg.mjs';
import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {bot} from '../services/bot.mjs';

const where_to_fruit = process.env.FRUITING_ROOM;
const counting_bot = '510016054391734273'; // counting bot ID

const titles = [
  'Bring on the fruit',
  'Pass the bananas',
  'Someone\'s going to need to go to the store and buy more fruit.',
  'Time to make some fruit salad on their face.',
  "I'm not mad, I'm just disappointed.",
  "I hope everyone's throwing arm is warmed up",
  "Two comes after one, three comes after two. Come on, it's not that hard!",
  'I think someone needs to go back to school.',
  'Someone is spending some time in the corner to practice their counting.',
  "This is why we can't have nice things.",
  'Oh come on. You can do better than that!',
  "You've dashed all my hopes and dreams.",
  'I believed in you and look at you now.',
  'Counting mishap: unleash the fruity fury!',
  'Numbers gone wild!',
  'Oops, wrong number! Fruit party time!',
  'Error 404: Correct number not found. Deploying fruit!',
  'When life gives you counting errors, make fruit rain!',
  'Someone skipped counting class!',
  'Fruit for thought: let\'s try counting again!',
  'Oops, back to counting school you go!',
  'Did you learn to count from a fruit basket?',
  'Try again, rookie!',
  'Who knew counting could be so challenging?',
  'Well, that escalated quickly... in the wrong direction!',
  'When counting goes awry: try, try again!',
  'The not-so-simple art of counting!',
  'Counting chaos: let\'s give it another shot!',
  'A numbers game gone wrong!',
  'Count again, my friend: practice makes perfect!',
  'Numbers not your strong suit, huh?',
  'Were you counting with your fingers or toes?',
  'We\'re gonna need a bigger abacus!',
  'Counting crisis: who let the amateur in?',
  'Nice try, but that\'s not how it works!',
  'One, two, whoops! Maybe stick to words?',
  'And the award for "Best Counting Fail" goes to...',
  'Don\'t worry, counting isn\'t for everyone!',
  'Maybe counting sheep is more your speed?',
  'That\'s an interesting way to count... said no one ever!',
];

export const ruined_it = {
  regex: /@.*\sRUINED IT AT \*\*\d*\*\*/g,
  tag_bot: false,
  execute: async function (message) {
    if (!testViability(message)) {
      return;
    }
    let mentions = message.mentions;
    let user = mentions.users.first(); // User object

    message.channel.send(`SHAME ON <@${user.id}>! Head over to <#928783575627751505> to practice your counting and remove your new Corner of Shame role.\n`
      + 'If you do nothing your role of shame will fall off on its own... eventually. :smiling_imp:'
    );
    let bolded_number = getBoldedNumber(message);
    let description = `**<@${user.id}>** messed up the counting game at ${bolded_number}!`;

    await shameForBadCounting(message, user, bolded_number, description);
  }
}

export const used_guild_save = {
  regex: /@.* You have used \*\*\d*\*\* guild save!/gi,
  tag_bot: false,
  execute: async function(message) {
    if (!testViability(message)) {
      return;
    }
    let mentions = message.mentions;
    let user = mentions.users.first(); // User object

    message.channel.send(`SHAME ON <@${user.id}>! Why? Because you messed up the count!! `
      + 'Users voted and donated their saves *just so you could mess up*; the least you can do is count your way out of the corner. \n'
      + 'Head over to <#928783575627751505> to practice your counting and remove your new Corner of Shame role.\n'
      + 'If you do nothing your role of shame will fall off on its own... eventually. :smiling_imp:'
    );
    let description = `**<@${user.id}>** messed up counting and used up a guild save!`;

    let bolded_number = getBoldedNumber(message);
    await shameForBadCounting(message, user, bolded_number, description);
  }
}

export const used_your_save = {
  regex: /@.* You have used \*\*\d*\*\* of your saves/gi,
  tag_bot: false,
  execute: async function (message) {
    if (!testViability(message)) {
      return; // stop
    }
    let mentions = message.mentions;
    let user = mentions.users.first(); // User object

    message.channel.send(`SHAME ON <@${user.id}>! Why? Because you messed up the count!! `
      + '***Counting-bot saves can\'t save you from your SHAME.*** \n'
      + 'Head over to <#928783575627751505> to practice your counting and remove your new Corner of Shame role.\n'
      + 'If you do nothing your role of shame will fall off on its own... eventually. :smiling_imp:'
    );
    let description = `**<@${user.id}>** messed up counting and used up one of their saves!`;

    let bolded_number = getBoldedNumber(message);

    await shameForBadCounting(message, user, bolded_number, description);
  }
}

// Helper function to get bolded number
function getBoldedNumber(message) {
  let bolded_number = '';
  try {
    // Match all bolded numbers
    let number_matches = message.content.match(/\*\*[\d,]*\*\*/ig);

    if (!number_matches) {
      console.error(`There was an issue parsing the message: ${message.content}`);
    } else {
      if (message.content.includes('RUINED IT AT')) {
        // If the message includes "RUINED IT AT", get the first bolded number
        bolded_number = number_matches[0].replace(/\*/g, '').trim();
      } else {
        // Otherwise, get the last bolded number
        bolded_number = number_matches[number_matches.length - 1].replace(/\*/g, '').trim();
      }
    }
  } catch (error) {
    console.error(`Error while parsing the next number: ${error}`);
  }
  return bolded_number;
}

function testViability(message) {
  if (!where_to_fruit) {
    console.error("There's nowhere to throw any fruit.");
    return 0;
  }
  if (message.author.id !== counting_bot) {
    console.error(`OH snap, user ${message.author.id} attempted to trigger these commands! D=`);
    return 0;
  }
  return 1;
}

async function shameForBadCounting(message, user, bolded_number, description) {
  // Track counting failures
  let count = ensureBrainKeyExists('count_fail_tracking');
  if (!count[user.id]) {
    count[user.id] = 0;
  }
  count[user.id]++;
  await brain.write();

  let extra_message = await getRandomMsg(user, message.guild);

  const exampleEmbed = new EmbedBuilder()
    .setColor('#0099ff')
    .setTitle(_.sample(titles))
    .setURL('https://discord.com/channels/' + message.guild.id + '/' + message.channel.id + '/' + message.id)
    .setDescription(description)
    .setImage(user.avatarURL())
    .setTimestamp()
  ;

  const channel = message.guild.channels.cache.get(where_to_fruit);

  if (extra_message) {
    exampleEmbed.addFields(
      { name: '\u200B', value: '\u200B' },
      {name: 'Special Message:', value: extra_message.text},
      { name: '\u200B', value: '\u200B' },
    )
    let content = `Heads up <@${user.id}>! You've got a special message from <@${extra_message.set_by}>.`;
    channel.send({content: content, embeds: [exampleEmbed] })
      .then((message) => {
        message.react('🍅'); // Tomato
        message.react('🍑'); // Peach
        message.react('🥝'); // Kiwi
        message.react('🫐'); // Blueberries
        message.react('🍌'); // Banana
        message.react('🥭'); // Mango
        message.react('🍒'); // Cherries
        message.react('🍓'); // Strawberry
        message.react('🍇'); // Grapes
      });
    // Remove one-time messages from the brain
    if (extra_message.is_one_time) {
      delete brain.data.count_fail_msgs[user.id][extra_message.id];
      await brain.write();
    }
  } else {
    channel.send({embeds: [exampleEmbed] })
      .then((message) => {
        message.react('🍅'); // Tomato
        message.react('🍑'); // Peach
        message.react('🥝'); // Kiwi
        message.react('🫐'); // Blueberries
        message.react('🍌'); // Banana
        message.react('🥭'); // Mango
        message.react('🍒'); // Cherries
        message.react('🍓'); // Strawberry
        message.react('🍇'); // Grapes
      });
  }

  // Save shame timer and data
  const max = 3600000; // 1 hour in milliseconds
  const min = 18000000; // 5 hours in milliseconds
  let timer = Math.floor(Math.random() * (max - min + 1) + min);
  let number = bolded_number.replaceAll('*', '');
  number = number.replaceAll(',', '');

  const shame = await bot.shame.forUser(user.id);
  await shame.start({
    source: 'counting',
    failedAtNumber: number,
    expiresInMs: timer,
  });
}
