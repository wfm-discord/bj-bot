// src/services.bot.mjs
import {Client, Guild, TextChannel} from 'discord.js';
import {ConsentRepository} from 'bot-commons-consent';

export class Bot {
    /** @var {Client<true>} */
    client;

    /** @var {Guild} */
    guild;

    /** @var {string} */
    botRoot;

    /** @var {TextChannel} */
    logsChannel;

    /** @var {ConsentRepository} */
    consent;

    /** @var {GagRepository} */
    gag;

    /** @var {GroundingRepository} */
    grounding;

    /** @var {LinesRepository} */
    lines;

    /** @var {MathRepository} */
    math;

    /** @var {ShameRepository} */
    shame;

    /** @var {StatisticsRepository} */
    stats;
}

/** Container for accessing global services. */
export const bot = new Bot();
