// src/services/statistics/math.mjs
import {brain, ensureBrainKeyExists, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {TrackingBase} from './statisticsBase.mjs';

export class MathTracking extends TrackingBase {
  PRACTICE = 'practice';

  get #trackingData() {
    const trackingData = ensureBrainKeyExists('tracking', 'math');
    return trackingData[this.userId];
  }

  set #trackingData(data) {
    const trackingData = ensureBrainKeyExists('tracking', 'math');
    trackingData[this.userId] = data;
  }

  async #initTrackingData() {
    this.#trackingData = await ensureBrainKeyExistsPromise('tracking', 'math', this.userId);
    if (!this.#trackingData.operation) {
      this.#trackingData.operation = {};
    }
    // NOTE: "all" was added because the assignment is all.
    // It's only tracked on assigned problems, not completed problems etc.
    const operations = [
      'addition',
      'subtraction',
      'multiplication',
      'division',
      'algebra',
      'mixed',
      'mean',
      'median',
      'mode',
      'mean-median-mode',
      'factorial',
      'all'
    ];
    operations.forEach(operation => {
      if (!this.#trackingData.operation[operation]) {
        this.#trackingData.operation[operation] = {
          problemsStarted: 0,
          problemsCompleted: 0,
          problemsCorrect: 0,
          problemsIncorrect: 0,
        };
      }
    });
    ['practice', 'test', 'all'].forEach(mode => {
      if (!this.#trackingData[mode]) {
        this.#trackingData[mode] = {
          assignmentsStarted: 0,
          assignmentsCancelled: 0,
          assignmentsCompleted: 0,
          problemsAssigned: 0,
          problemsStarted: 0,
          problemsCompleted: 0,
          problemsCorrect: 0,
          problemsIncorrect: 0,
          timeSpent: 0
        };
      }
    });
    await brain.write();
  }

  async assignProblems(operation, mode, problemsAssigned) {
    await this.#initTrackingData();
    this.#trackingData[mode].problemsAssigned += problemsAssigned;
    this.#trackingData.all.problemsAssigned += problemsAssigned;
    await brain.write();
  }

  async startProblem(operation, mode) {
    await this.#initTrackingData();
    this.#trackingData.operation[operation].problemsStarted++;
    this.#trackingData[mode].problemsStarted++;
    this.#trackingData.all.problemsStarted++;
    await brain.write();
  }

  async cancelledAssignment(mode, startTime) {
    await this.#initTrackingData();
    this.#trackingData[mode].assignmentsCancelled++;
    this.#trackingData.all.assignmentsCancelled++;
    this.#updateTime(mode, startTime);
    await brain.write();
  }

  async startAssignment(mode) {
    await this.#initTrackingData();
    this.#trackingData.all.assignmentsStarted++;
    this.#trackingData[mode].assignmentsStarted++;
    await brain.write();
  }

  async problemCorrect(operation, mode) {
    await this.#initTrackingData();
    this.#trackingData.operation[operation].problemsCorrect++;
    this.#trackingData.operation[operation].problemsCompleted++;

    this.#trackingData[mode].problemsCorrect++;
    this.#trackingData[mode].problemsCompleted++;

    this.#trackingData.all.problemsCorrect++;
    this.#trackingData.all.problemsCompleted++;
  }

  async problemIncorrect(operation, mode) {
    await this.#initTrackingData();
    this.#trackingData.operation[operation].problemsIncorrect++;
    this.#trackingData[mode].problemsIncorrect++;
    this.#trackingData.all.problemsIncorrect++;
    // Problem isn't completed if it's incorrect in practice mode
    if (mode !== this.PRACTICE) {
      this.#trackingData.operation[operation].problemsCompleted++;
      this.#trackingData[mode].problemsCompleted++;
      this.#trackingData.all.problemsCompleted++;
    }
  }

  async completedAssignment(mode, startTime) {
    await this.#initTrackingData();
    this.#trackingData[mode].completed++;
    this.#trackingData.all.completed++;
    this.#updateTime(mode, startTime);
    await brain.write();
  }

  //
  // - Private Functions -
  //

  #updateTime(mode, startTime) {
    if (startTime) {
      const timeRecorded = Date.now() - startTime;
      this.#trackingData.all.timeSpent += timeRecorded;
      this.#trackingData[mode].timeSpent += timeRecorded;
    }
  }

  //
  // - Booleans -
  //

  /**
   * @returns {boolean}
   */
  hasTracking() {
    return !!this.#trackingData;
  }

  //
  // - Getters -
  //

  getModeData(mode) {
    return this.#trackingData[mode];
  }

  getOperationData(operation) {
    return this.#trackingData.operation[operation];
  }

  get percentage() {
    return this.#trackingData.all.problemsAssigned > 0
      ? (this.#trackingData.all.problemsCorrect / this.#trackingData.all.problemsAssigned) * 100
      : 0;
  }
}
