// src/services/statistics/grounding.mjs
import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {TrackingBase} from './statisticsBase.mjs';

export class GroundingTracking extends TrackingBase {
  #trackingData;

  async update({minutesCompleted = 0, correctClicks = 0, incorrectClicks = 0, missedClicks = 0}) {
    this.#trackingData = await ensureBrainKeyExistsPromise('tracking', 'grounding', this.userId);

    this.#trackingData.minutesCompleted = this.#trackingData.minutesCompleted || 0;
    this.#trackingData.correctClicks = this.#trackingData.correctClicks || 0;
    this.#trackingData.incorrectClicks = this.#trackingData.incorrectClicks || 0;
    this.#trackingData.misses = this.#trackingData.misses || 0;

    // Update the grounding data
    this.#trackingData.minutesCompleted += minutesCompleted;
    this.#trackingData.correctClicks += correctClicks;
    this.#trackingData.incorrectClicks += incorrectClicks;
    this.#trackingData.misses += missedClicks;

    await brain.write();
  }
}
