// src/services/statistics/base.mjs
import {brain} from 'bot-commons-utils/src/utils/brain.mjs';

export class TrackingBase {
  #userId;
  constructor(userId) {
    this.#userId = userId;
  }

  get userId() {
    return this.#userId;
  }

  async save() {
    return brain.write();
  }
}
