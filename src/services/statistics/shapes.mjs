// src/services/statistics/shapes.mjs
import {brain, ensureBrainKeyExistsPromise} from 'bot-commons-utils/src/utils/brain.mjs';
import {TrackingBase} from './statisticsBase.mjs';

export class ShapeCountTracking extends TrackingBase {
  #trackingData;

  async update({isSuccess = false, timeTaken, difficulty, size, movement, color, guesses = 0}) {
    this.#trackingData = await ensureBrainKeyExistsPromise('tracking', 'shape_count', this.userId);

    // Initialize or update specific stats
    this.#trackingData.all = this.#trackingData.all || this.#createNewShapeStatsObject();
    this.#trackingData.difficultyStats = this.#trackingData.difficultyStats || {};
    this.#trackingData.sizeStats = this.#trackingData.sizeStats || {};
    this.#trackingData.movementStats = this.#trackingData.movementStats || {};
    this.#trackingData.colorStats = this.#trackingData.colorStats || {};

    // Ensure stats objects exist for new categories
    this.#trackingData.difficultyStats[difficulty] = this.#trackingData.difficultyStats[difficulty] || this.#createNewShapeStatsObject();
    this.#trackingData.sizeStats[size] = this.#trackingData.sizeStats[size] || this.#createNewShapeStatsObject();
    this.#trackingData.movementStats[movement] = this.#trackingData.movementStats[movement] || this.#createNewShapeStatsObject();
    this.#trackingData.colorStats[color] = this.#trackingData.colorStats[color] || this.#createNewShapeStatsObject();

    // Update stats objects
    this.#updateShapeStatsObject(this.#trackingData.all, isSuccess, timeTaken, guesses);
    this.#updateShapeStatsObject(this.#trackingData.difficultyStats[difficulty], isSuccess, timeTaken, guesses);
    this.#updateShapeStatsObject(this.#trackingData.sizeStats[size], isSuccess, timeTaken, guesses);
    this.#updateShapeStatsObject(this.#trackingData.movementStats[movement], isSuccess, timeTaken, guesses);
    this.#updateShapeStatsObject(this.#trackingData.colorStats[color], isSuccess, timeTaken, guesses);

    await brain.write();
  }

  #createNewShapeStatsObject() {
    return {
      totalGuesses: 0,
      successfulGuesses: 0,
      failedGuesses: 0,
      timeTaken: 0,
      averageTime: 0
    };
  }

  #updateShapeStatsObject(statsObject, isSuccess, timeTaken, guesses) {
    statsObject.totalGuesses = (statsObject.totalGuesses || 0) + guesses;
    if (isSuccess) {
      statsObject.successfulGuesses = (statsObject.successfulGuesses || 0) + 1;
      if (timeTaken !== null) {
        statsObject.timeTaken = (statsObject.timeTaken || 0) + timeTaken;
        statsObject.averageTime = statsObject.timeTaken / statsObject.successfulGuesses;
      }
    } else {
      statsObject.failedGuesses = (statsObject.failedGuesses || 0) + 1;
    }
  }
}
