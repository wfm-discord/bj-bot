// src/services/math.mjs
import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {AssignmentBase} from './assignmentBase.mjs';
import {bot} from './bot.mjs';
import { timeUtils } from 'bot-commons-utils/src/utils/time.mjs';
import { Addition } from './math/addition.mjs';
import { Algebra } from './math/algebra.mjs';
import { Division } from './math/division.mjs';
import { Factorial } from './math/factorial.mjs';
import { MeanMedianMode } from './math/meanMedianMode.mjs';
import { MixedOperation } from './math/mixedOperation.mjs';
import { Multiplication } from './math/multiplication.mjs';
import { Subtraction } from './math/subtraction.mjs';


export class MathRepository {
  /** Get the math manager for a user.
   *
   * @param {Snowflake} userId the target user's Discord user ID
   * @returns {Promise<MathAssignment>}
   */
  async forUser(userId) {
    const tracking = await bot.stats.mathForUser(userId);
    return new MathAssignment(userId, tracking);
  }
}

export class MathAssignment extends AssignmentBase {
  #tracking;

  constructor(userId, tracking) {
    super(userId);
    this.#tracking = tracking;
  }

  get #math() {
    const math = ensureBrainKeyExists('mathProblems');
    return math[this.userId];
  }

  set #math(data) {
    const math = ensureBrainKeyExists('mathProblems');
    math[this.userId] = data;
  }

  async assignMath({
    setByUserId,
    problemsTodo,
    channelIdWhereAssigned,
    operation,
    testMode,
    setting = "general",
    spectatorSetting = 'off'
  }) {
    // create channel to do math in
    const channel = await this.createAssignmentChannel('problems', setByUserId, spectatorSetting);
    const member = await bot.guild.members.fetch(this.userId);

    let timesTables = {};

    if (setting === 'times_table') {
      for (let i = 1; i <= 9; i++) {
        for (let j = 1; j <= 9; j++) {
          let ij = i + '_' + j;
          timesTables[ij] = `${i} * ${j}`;
        }
      }
      operation = 'multiplication';
    }

    this.#math = {
      username: member.displayName, // saved so I can use it if they leave the server
      problemsTodo: problemsTodo,
      problemsCompleted: 0,
      correct: 0,
      mistakes: 0,
      mistakesOnProblem: 0,
      problemAttempts: 0,
      channelId: channel.id,
      channelIdWhereAssigned,
      setByUserId,
      setTime: Date.now(),
      startTime: null,
      lastTime: null,
      started: false,
      currentProblem: null,
      currentAnswer: null,
      currentOperation: operation,
      operation: operation,
      testing: testMode ? 'test' : 'practice',
      setting: setting,
      timesTablesProblemsRemaining: timesTables,
      firstProblemStartTime: null,
      wrongTestProblems: [],
      timeTaken: null,
      timeRecorded: null,
      delayToStart: null
    };

    await this.#tracking.assignProblems(operation, this.#math.testing, +problemsTodo);

    await brain.write();

    await channel.send(this.#getProblemAssignmentMessage(problemsTodo));
  }

  async clearMath() {
    delete brain.data.mathProblems[this.userId];
    await brain.write();
  }

  async cancelMath() {
    await this.#tracking.cancelledAssignment(this.testing, this.#math.firstProblemStartTime);
    // delete channel
    setTimeout(async () => {
      await this.cancelMathNow();
    }, 10000);
  }

  async cancelMathNow() {
    await this.deleteAssignmentChannel();
    await this.clearMath();
  }


  async startMathAssignment() {
    this.#math.started = true;
    this.#math.firstProblemStartTime = Date.now();
    this.updateTimestamp();

    // Update tracking info
    await this.#tracking.startAssignment(this.testMode);
    await this.#tracking.startProblem(this.#math.operation, this.testMode);

    // Write brain
    await brain.write();
  }

  async handleAnswer(userAnswerStr) {
    let userAnswer;

    // Handle algebra problems where the answer can be in the form "x=30" or "30"
    if (this.#math.currentOperation === 'algebra') {
      const match = userAnswerStr.trim().match(/^x\s*=\s*(-?\d+(\.\d+)?)$/);
      if (match) {
        userAnswer = parseFloat(match[1]);
      } else {
        userAnswer = parseFloat(userAnswerStr.trim());
      }
    } else {
      userAnswer = parseFloat(userAnswerStr.trim());
    }

    // Ensure this.#math.currentAnswer is a number
    const currentAnswer = parseFloat(this.#math.currentAnswer);

    // Define a small tolerance for floating-point comparison
    const epsilon = 1e-10;

    const isAnswerCorrect = Math.abs(userAnswer - currentAnswer) < epsilon;

    // Only increment problems completed if the answer is correct or in test mode
    if (isAnswerCorrect || this.isTest()) {
      this.#math.problemsCompleted += 1;
    }

    if (isAnswerCorrect) {
      this.#math.correct++;
      await this.#tracking.problemCorrect(this.#math.currentOperation, this.testMode);
    }

    if (!isAnswerCorrect) {
      // Always update the mistakes when the answer is wrong
      this.#math.mistakes++;
      this.#math.mistake_on_problem++;
      await this.#tracking.problemIncorrect(this.#math.currentOperation, this.testMode);
    }

    if (this.isTest() && !isAnswerCorrect) {
      // Store the wrong answer, the entered answer, and the actual answer
      this.#math.wrongTestProblems.push({
        problem: this.#math.currentProblem,
        enteredAnswer: userAnswerStr,
        correctAnswer: this.#math.currentAnswer
      });
    }

    await brain.write();
  }

  async handleCompletion() {
    await this.#tracking.completedAssignment(this.testMode, this.firstProblemStartTime);
    this.#math.timeTaken = Date.now() - this.#math.setTime;
    this.#math.timeRecorded = Date.now() - this.#math.firstProblemStartTime;
    this.#math.delayToStart = this.#math.timeRecorded - this.#math.timeTaken;
    await brain.write();
  }

  #getProblemAssignmentMessage(problemsTodo) {
    return `${this.#mathTestMessage()}<@${this.userId}> you must solve ${problemsTodo} problems. ` +
      'You can turn this assignment in incomplete by typing "cancel".\n' +
      '**When you are ready to start, send "start"**';
  }

  async getNewProblem() {
    let newProblem;
    if (this.isTimesTables()) {
      newProblem = await this.#getRandomTimesTableProblem();
      this.#math.currentOperation = 'multiplication';
    } else {
      newProblem = this.#generateNewProblem();
      this.#math.currentOperation = newProblem.operation;
    }
    this.#math.currentProblem = newProblem.problem;
    this.#math.currentAnswer = newProblem.answer;
    this.#math.mistake_on_problem = 0;
    await brain.write();
    return newProblem;
  }

  async sendCancellationMessage() {
    const channel = this.channelWhereAssigned;

    // Calculate problems completed and those left
    let completedProblems = this.#math.problemsCompleted;
    let totalProblems = this.#math.problemsTodo;
    let correctAnswers = this.#math.correct;

    let reply_msg = `<@${this.userId}> turned in their math ${this.#math.testing} early! ` +
      `Out of **${totalProblems}** problems, they completed **${completedProblems}** problems, ` +
      `getting **${correctAnswers}** right and **${this.#math.mistakes}** wrong.\n`;

    // If there are any incorrect answers, show them
    reply_msg += this.wrongProblemsMessage();
    if (this.#math.wrongTestProblems && this.#math.wrongTestProblems.length > 0) {
      let wrong_test_problems = this.#math.wrongTestProblems;
      reply_msg += '**Here are the problems you got wrong:**\n';
      for (const wrongProblem of wrong_test_problems) {
        reply_msg += `* Problem: ${wrongProblem.problem} | Your answer: ${wrongProblem.enteredAnswer} | Correct answer: ${wrongProblem.correctAnswer}\n`;
      }
    }

    await channel.send(reply_msg);
  }

  async sendCompletionMessage() {
    let reply_msg = '';

    if (this.#math.setByUserId !== this.userId) {
      reply_msg += `<@${this.userId}> completed their math assignment for <@${this.#math.setByUserId}>. `;
    } else {
      reply_msg += `<@${this.userId}> completed their math assignment.`;
    }

    reply_msg += ` They made **${this.#math.mistakes}** ${this.#math.mistakes === 1 ? 'mistake' : 'mistakes'} ` +
      `and it took them **${timeUtils.msToTime(this.#math.timeTaken)}** from time assigned, ` +
      `or **${timeUtils.msToTime(this.#math.timeRecorded)}** from time started.\n\n`;

    if (this.#math.delayToStart > (5 * 60 * 1000)) {
      reply_msg += `It took **${timeUtils.msToTime(this.#math.delayToStart)}** to get started. ` +
        'Can you explain to us what took you so long to get started??\n\n';
    }

    // If in test mode, show the incorrect answers
    if (this.isTest()) {
      // Send the grade
      reply_msg += `Your grade is: **${this.letterGrade}** (${this.percentageGrade.toFixed(2)}%)\n\n`;

      // If there are any incorrect answers, show them
      reply_msg += this.wrongProblemsMessage;
    }
    try {
      const channel = this.channelWhereAssigned;
      await channel.send(reply_msg);
    } catch (error) {
      let botNoiseChannel = bot.client.channels.cache.get(process.env.BOTNOISE_CHANNEL_ID);
      botNoiseChannel.send("An error occurred while sending a message to the intended channel. Here's the message content:\n\n" + reply_msg);
    }  }

  updateTimestamp() {
    this.#math.lastTime = Date.now();
  }


  //
  // - Public Static Functions -
  //


  //
  // - Private Functions -
  //

  #generateNewProblem() {
    const operationsMap = {
      addition: new Addition(),
      subtraction: new Subtraction(),
      multiplication: new Multiplication(),
      division: new Division(),
      algebra: new Algebra(),
      'mean-median-mode': new MeanMedianMode(),
      mixed: new MixedOperation(),
      factorial: new Factorial()
    };

    let selectedOperation = this.#math.operation;
    let operationInstance = selectedOperation !== 'all'
      ? operationsMap[selectedOperation]
      : operationsMap[Object.keys(operationsMap)[Math.floor(Math.random() * Object.keys(operationsMap).length)]];

    if (operationInstance) {
      return operationInstance.generateProblem();
    }

    return { problem: '', answer: 0, operation: '' };
  }

  async #getRandomTimesTableProblem() {
    const timesTablesRemaining = this.#math?.timesTablesProblemsRemaining;
    const keys = Object.keys(timesTablesRemaining);

    if (keys.length === 0) {
      throw new Error('No remaining problems.');
    }

    const randomIndex = Math.floor(Math.random() * keys.length);
    const selectedProblemStr = keys[randomIndex];
    const [i, j] = timesTablesRemaining[selectedProblemStr].split(' * ').map(Number);

    let problem = timesTablesRemaining[selectedProblemStr];
    // Remove the selected problem from the object
    delete timesTablesRemaining[selectedProblemStr];
    await brain.write();

    return {
      problem: problem,
      answer: i * j,
      operation: 'multiplication'
    };
  }

  #mathTestMessage() {
    if (this.isTest()) {
      return '# Time for a Math Test!\n' +
        '* You should complete this exam without reference to notes, books, or other resources. ' +
        'You should not use a calculator (one will not be necessary), ' +
        'but may use scrap paper to do calculations if you wish.\n' +
        '* **If you do use a calculator, make sure to fess up to it when your test is turned in.** ' +
        'Many students have needed additional lessons in long division.\n' +
        '* **Eyes on your own paper.** Looking at another person\'s paper is ' +
        'reason to assume cheating and your paper will be taken.\n' +
        '* All Cell phones and electronic devices must be OFF and put away and ' +
        'hats removed.\n' +
        '* **Make sure your answer is correct before submitting it.** ' +
        'You will not be able to edit your answers later.\n' +
        '* You will find out what answers you got correct or incorrect at the end of the test.\n' +
        '## :warning: Turning this in early (sending "cancel") will affect your overall grade!\n\n';
    }
    return '';
  }


  //
  // - Booleans -
  //

  /**
   * @return boolean
   */
  hasProblems() {
    return !!this.#math?.problemsTodo;
  }

  /**
   * @return boolean
   */
  isComplete() {
    const problemsCompleted = this.#math.problemsCompleted;
    let problemsTodo = this.#math.problemsTodo;

    // Check if problemsTodo is undefined or not a number
    if (problemsTodo === undefined || problemsTodo === null || isNaN(problemsTodo)) {
      // Handle the case where problemsTodo is not set or invalid
      problemsTodo = 0;
    }

    return problemsCompleted >= problemsTodo;
  }

  /**
   * @return boolean
   */
  isStarted() {
    return !!this.#math.started;
  }

  /**
   * @return boolean
   */
  isTest() {
    return this.#math.testing === 'test';
  }

  /**
   * @return boolean
   */
  isTimesTables() {
    return this.#math?.setting === 'times_table';
  }

  shouldShowDivisionMessage() {
    if (this.#math.currentOperation === 'division') {
      return true;
    }

    if (this.#math.currentOperation === 'mixed') {
      return this.#math.currentProblem.includes('/');
    }

    return false;
  }


  //
  // - Getters/Setters -
  //

  /**
   * Gets the channel ID of the math assignment.
   *
   * @returns {Snowflake} The ID of the Discord channel associated with the math assignment.
   */
  get channelId() {
    return this.#math.channelId;
  }

  /**
   * Gets the channel ID of where math was assigned.
   *
   * @returns {Snowflake} The ID of the Discord channel where math was assigned
   */
  get channelIdWhereAssigned() {
    return this.#math.channelIdWhereAssigned;
  }

  get currentAnswer() {
    return this.#math.currentAnswer;
  }

  /**
   * @return string
   */
  get divisionMessage() {
    return 'Round to two decimals! Use US notation with \'.\' denoting the decimal.';
  }

  /**
   * @return number
   */
  get percentageGrade() {
    return (this.#math.correct / this.#math.problemsTodo) * 100;
  }

  /**
   * @return string
   */
  get letterGrade() {
    return AssignmentBase.calculateLetterGrade(this.percentageGrade);
  }

  get problemNumber() {
    return this.#math.problemsCompleted + 1;
  }

  get setTime() {
    return this.#math.setTime;
  }

  /**
   * @return string
   */
  get testing() {
    return this.#math.testing;
  }

  /**
   * @return string
   */
  get testMode() {
    return this.#math.testing;
  }

  get timeTaken() {
    return this.#math.timeTaken;
  }

  get firstProblemStartTime() {
    return this.#math.firstProblemStartTime;
  }

  /**
   * @return string
   */
  get wrongProblemsMessage() {
    let message = '';
    const { wrongTestProblems } = this.#math;
    if (wrongTestProblems && wrongTestProblems.length > 0) {
      message += '**Here are the problems you got wrong:**\n';
      wrongTestProblems.forEach(({ problem, enteredAnswer, correctAnswer }) => {
        message += `* Problem: ${problem} | Your answer: ${enteredAnswer} | Correct answer: ${correctAnswer}\n`;
      });
    }
    return message;
  }
}
