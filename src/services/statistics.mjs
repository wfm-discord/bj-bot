// src/services/statistics.mjs

import {MathTracking} from './statistics/math.mjs';
import {GroundingTracking} from './statistics/grounding.mjs';
import {ShapeCountTracking} from './statistics/shapes.mjs';

export class StatisticsRepository {
  async shapeCountForUser(userId) {
    return new ShapeCountTracking(userId);
  }

  async groundingForUser(userId) {
    return new GroundingTracking(userId);
  }

  async mathForUser(userId) {
    return new MathTracking(userId);
  }
}
