// src/services/math/factorial.mjs

export class Factorial {
  generateProblem() {
    const num = Math.floor(Math.random() * 7) + 3; // Random number between 3 and 9
    const problem = `What is ${num} factorial ( ${num}! )?`;
    const answer = this.#factorial(num);
    return { problem, answer, operation: 'factorial' };
  }

  #factorial(n) {
    if (n === 0 || n === 1) return 1;
    return n * this.#factorial(n - 1);
  }
}
