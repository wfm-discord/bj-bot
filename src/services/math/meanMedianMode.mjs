import { OperationBase } from './operationBase.mjs';

export class MeanMedianMode extends OperationBase {
  /**
   * @returns {{problem: string, answer, operation: string}}
   */
  generateProblem() {
    let data;
    const operationType = ['mean', 'median', 'mode'][Math.floor(Math.random() * 3)];

    if (operationType === 'mode') {
      data = this.#generateDataWithControlledMode();
    } else {
      data = this.#generateDataWithControlledMeanAndMedian();
    }

    let problem, answer;

    if (operationType === 'mean') {
      const sum = data.reduce((a, b) => a + b, 0);
      answer = sum / data.length;
      problem = `Find the mean of the following data set: [${this.#shuffleArray(data).join(', ')}]`;
    } else if (operationType === 'median') {
      data.sort((a, b) => a - b);
      const mid = Math.floor(data.length / 2);
      answer = data.length % 2 !== 0 ? data[mid] : (data[mid - 1] + data[mid]) / 2;
      problem = `Find the median of the following data set: [${this.#shuffleArray(data).join(', ')}]`;
    } else {
      const frequency = {};
      data.forEach(num => frequency[num] = (frequency[num] || 0) + 1);
      const mode = Object.keys(frequency).reduce((a, b) => frequency[a] > frequency[b] ? a : b);
      answer = Number(mode);
      problem = `Find the mode of the following data set: [${this.#shuffleArray(data).join(', ')}]`;
    }

    return { problem, answer, operation: operationType };
  }

  #generateDataWithControlledMeanAndMedian() {
    let data;
    while (true) {
      data = Array.from({ length: 10 }, () => Math.floor(Math.random() * 100) + 1);

      // Check mean
      const sum = data.reduce((a, b) => a + b, 0);
      const mean = sum / data.length;

      // Check median
      const sortedData = [...data].sort((a, b) => a - b);
      const mid = Math.floor(sortedData.length / 2);
      const median = sortedData.length % 2 !== 0 ? sortedData[mid] : (sortedData[mid - 1] + sortedData[mid]) / 2;

      if (
        (Number.isInteger(mean) || (mean * 100) % 1 === 0 || (mean * 100) % 25 === 0) &&
        (Number.isInteger(median) || (median * 100) % 1 === 0 || (median * 100) % 25 === 0)
      ) {
        break;
      }
    }
    return data;
  }

  #generateDataWithControlledMode() {
    // Generate initial data set of random numbers
    const data = Array.from({ length: 10 }, () => Math.floor(Math.random() * 100) + 1);

    // Randomly select numbers from the data set to be duplicated
    const num1 = data[Math.floor(Math.random() * data.length)];
    const num2 = data[Math.floor(Math.random() * data.length)];
    const num3 = data[Math.floor(Math.random() * data.length)];

    // Randomly decide how many times to duplicate each number
    const num1DupCount = Math.floor(Math.random() * 3) + 1; // Add 1 to 3 duplicates
    const num2DupCount = Math.floor(Math.random() * 4) + 1; // Add 1 to 4 duplicates
    const num3DupCount = Math.floor(Math.random() * 5) + 1; // Add 1 to 5 duplicates

    // Add the duplicates to the data set
    for (let i = 0; i < num1DupCount; i++) {
      data.push(num1);
    }
    for (let i = 0; i < num2DupCount; i++) {
      data.push(num2);
    }
    for (let i = 0; i < num3DupCount; i++) {
      data.push(num3);
    }

    // Calculate frequency of each number
    const frequency = {};
    data.forEach(num => frequency[num] = (frequency[num] || 0) + 1);

    // Find the maximum frequency
    const maxFrequency = Math.max(...Object.values(frequency));

    // Identify numbers that have the same max frequency as the most frequent number
    const tiedModes = Object.keys(frequency).filter(num => frequency[num] === maxFrequency);

    // If there's more than one mode, add another occurrence of a randomly selected mode candidate
    if (tiedModes.length > 1) {
      const randomIndex = Math.floor(Math.random() * tiedModes.length);
      const selectedModeCandidate = parseInt(tiedModes[randomIndex], 10);
      data.push(selectedModeCandidate);
    }

    return this.#shuffleArray(data);
  }

  #shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }
}
