export class OperationBase {
  generateProblem() {
    throw new Error('generateProblem() must be implemented');
  }
}
