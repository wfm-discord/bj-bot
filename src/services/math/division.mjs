import { OperationBase } from './operationBase.mjs';

export class Division extends OperationBase {
  /**
   * @returns {{problem: string, answer: number, operation: string}}
   */
  generateProblem() {
    const num1 = Math.floor(Math.random() * 100) + 1;
    const num2 = Math.floor(Math.random() * 100) + 1;
    const problem = `${num1} / ${num2}`;
    const answer = Math.round((num1 / num2) * 100) / 100; // Rounding to 2 decimal places
    return { problem, answer, operation: 'division' };
  }
}
