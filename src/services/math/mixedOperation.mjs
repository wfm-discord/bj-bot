import { OperationBase } from './operationBase.mjs';
import {evaluate} from 'mathjs';

export class MixedOperation extends OperationBase {
  /**
   * @returns {{problem: string, answer: number, operation: string}}
   */
  generateProblem() {
    let numInts = Math.floor(Math.random() * 3) + 3;
    let operations = ['+', '-', '*', '/'];
    let problemParts = [];

    for (let i = 0; i < numInts; i++) {
      problemParts.push(Math.floor(Math.random() * 100) + 1);
      if (i < numInts - 1) {
        problemParts.push(operations[Math.floor(Math.random() * operations.length)]);
      }
    }
    const problem = problemParts.join(' ');
    const answer = Math.round(evaluate(problem) * 100) / 100;
    return { problem, answer, operation: 'mixed' };
  }
}
