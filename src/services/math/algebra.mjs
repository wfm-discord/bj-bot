import { OperationBase } from './operationBase.mjs';

export class Algebra extends OperationBase {
  /**
   * @returns {{problem: string, answer: number, operation: string}}
   */
  generateProblem() {
    const variable = 'x';
    const coefficient = Math.floor(Math.random() * 100) + 1;

    // Generate the answer such that it is positive about 2/3 of the time and negative 1/3 of the time
    const answer = (Math.random() < 0.33)
      ? Math.floor(Math.random() * 80) - 100  // Negative answer
      : Math.floor(Math.random() * 80) + 1;   // Positive answer

    const constant = Math.floor(Math.random() * 1000) + 1;
    const c = coefficient * answer + constant;

    const problem = `${coefficient}${variable} + ${constant} = ${c}`;

    return { problem, answer, operation: 'algebra' };
  }
}
