import { OperationBase } from './operationBase.mjs';

export class Addition extends OperationBase {
  /**
   * @returns {{problem: string, answer: number, operation: string}}
   */
  generateProblem() {
    const num1 = Math.floor(Math.random() * 100) + 1;
    const num2 = Math.floor(Math.random() * 100) + 1;
    const problem = `${num1} + ${num2}`;
    const answer = num1 + num2;
    return { problem, answer, operation: 'addition' };
  }
}
