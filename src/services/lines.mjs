// src/services/lines.mjs
import randomWords from 'random-words';
import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {bot} from './bot.mjs';
import {AssignmentBase} from './assignmentBase.mjs';
import fetch from 'node-fetch';
import {loremIpsum} from "lorem-ipsum";
import { promises as fs } from 'node:fs';

export const charMap = {
  ' ': '\u{200B} ',
  'a': 'а', // Cyrillic 'а'
  'c': 'с', // Cyrillic 'с'
  'e': 'е', // Cyrillic 'е'
  'i': 'і', // Ukrainian 'і'
  'o': 'ο', // Greek 'ο'
  // 'p': 'ρ', // Greek 'ρ' - Too obviously different
  'x': 'х', // Cyrillic 'х'
  'y': 'у', // Cyrillic 'у'
  'A': 'Α', // Greek uppercase 'Α'
  'B': 'Β', // Greek uppercase 'Β'
  'C': 'С', // Cyrillic uppercase 'С'
  'E': 'Ε', // Greek uppercase 'Ε'
  'H': 'Н', // Cyrillic uppercase 'Н'
  'I': 'І', // Ukrainian/Cyrillic 'І'
  'J': 'Ј', // Cyrillic uppercase 'Ј'
  'K': 'Κ', // Greek uppercase 'Κ'
  'M': 'Μ', // Greek uppercase 'Μ'
  'N': 'Ν', // Greek uppercase 'Ν'
  'O': 'О', // Cyrillic uppercase 'О'
  'P': 'Ρ', // Greek uppercase 'Ρ'
  'S': 'Ѕ', // Cyrillic uppercase 'Ѕ'
  'T': 'Τ', // Greek uppercase 'Τ'
  'X': 'Х', // Cyrillic uppercase 'Х'
  'Y': 'Υ', // Greek uppercase 'Υ'
  'Z': 'Ζ', // Greek uppercase 'Ζ'
};

const randomTypeWord = {
  'quotes': 'Quotes!',
  'sentences': 'Sentences!',
  'random_words': 'Random Words!',
  'lorem_ipsum': 'Lorem Ipsum!',
};

export class LinesRepository {
  /** Get the lines manager for a user.
   *
   * @param {Snowflake} userId the target user's Discord user ID
   * @returns {Promise<Lines>}
   */
  async forUser(userId) {
    return new Lines(userId);
  }
}

export class Lines extends AssignmentBase {
  get #lines() {
    const lines = ensureBrainKeyExists('lines');
    return lines[this.userId];
  }

  set #lines(data) {
    const lines = ensureBrainKeyExists('lines');
    lines[this.userId] = data;
  }

  async clearLines() {
    delete brain.data.lines[this.userId];
    await brain.write();
  }

  async assignLines({
    setByUserId,
    linesTodo,
    line = null,
    randomLineType = null,
    channelIdWhereAssigned,
    hideNumberCompleted,
    randomCase,
    penaltyLines,
    spectatorSetting = 'off'
  }) {
    // create channel to do lines in
    const channel = await this.createAssignmentChannel('lines', setByUserId, spectatorSetting);

    const member = await bot.guild.members.fetch(this.userId);

    this.#lines = {
      username: member.displayName, // saved so I can use it if they leave the server
      linesTodo,
      linesCompleted: 0,
      mistakes: 0,
      mistakesOnLine: 0,
      lineAttempts: 0,
      randomWords: !!randomLineType || !line,
      randomLineType,
      line,
      channelId: channel.id,
      channelIdWhereAssigned,
      setByUserId,
      startTime: null,
      lastTime: null,
      currentCpm: 0,
      hasCheated: false,
      hideNumberCompleted,
      started: false,
      randomCase: (randomCase === null) ? 'normal' : randomCase,
      penaltyLines,
    }


    await brain.write();

    // send starting message to new channel
    let startingMessage = `<@${this.userId}> you must write ${linesTodo}`;
    if (!line) {
      startingMessage += ` random lines.\n\n`;
    } else {
      startingMessage += ` lines.\n\n`;
    }

    startingMessage += 'When you\'re ready to write your lines, write "start".\n'+
     'You can cancel the task at any time by typing "cancel".';
    await channel.send(startingMessage);
  }

  async startLines() {
    this.#lines.started = true;
    this.#lines.startTime = Date.now();
    this.#lines.lastTime = Date.now();
    await this.save();
  }

  async cancelLines() {
    // delete channel
    setTimeout(async () => {
      await this.cancelLinesNow();
    }, 10000);
  }

  async cancelLinesNow() {
    // delete channel
    await this.deleteAssignmentChannel();
    // clear lines
    await this.clearLines();
  }

  async completeLines() {
    // currently the same as cancelling lines
    await this.cancelLines();
  }

  async addLinesToDo(quantity) {
    // Ensure that quantity can be converted to a number
    const numericQuantity = +quantity;
    if (isNaN(numericQuantity)) {
      console.error(`Expected a numeric value for quantity, received: ${quantity}`);
      return;
    }

    // Ensure this.#lines.linesTodo is a number
    if (typeof this.#lines.linesTodo !== 'number') {
      console.error(`this.#lines.linesTodo is not a number, current value: ${this.#lines.linesTodo}`);
      this.#lines.linesTodo = 0; // Reset to 0 or handle accordingly
    }

    // Perform the addition
    this.#lines.linesTodo += numericQuantity;

    // Save the updated value
    await this.save();
  }

  async setUpNextLine() {
    if (this.#lines.randomLineType && this.#lines.randomLineType === 'quotes') {
      const response = await fetch('https://zenquotes.io/api/random');
      const data = await response.json();
      const quote = data[0]?.q.trim();
      const author = data[0]?.a.trim();
      this.#lines.line = `"${quote}" (${author})`;

    } else if (this.#lines.randomLineType && this.#lines.randomLineType === 'sentences') {
      // Read the JSON file
      const data = await fs.readFile(new URL('../../resources/sentences.json', import.meta.url), { encoding: 'utf8' });
      // Parse the JSON data
      const json = JSON.parse(data);
      const sentences = json.sentences;
      // Select a random sentence
      const randomIndex = Math.floor(Math.random() * sentences.length);
      this.#lines.line = sentences[randomIndex];

    } else if (this.#lines.randomLineType && this.#lines.randomLineType === 'lorem_ipsum') {
      const loremLine = loremIpsum();
      if (loremLine) {
        this.#lines.line = loremLine;
      } else {
        console.error('loremIpsum() returned an invalid line.');
        this.#lines.line = 'I lucked out because the lorem ipsum API didn\'t give me a line.';
      }
    } else if (this.isRandomWords() || (this.#lines.randomLineType && this.#lines.randomLineType === 'random_words')) {
      this.#lines.line = this.#getRandomWordLine();
    }

    if (this.isRandomCase()) {
      this.#lines.line = this.#randomizeLineCase();
    } else if (this.isRandomCaseBuggy()) {
      // Do nothing because that's how "buggy" works.
      // The line may be normal, or have an already randomized case
      // but nothing happens here because something else randomizes the line case.
    }
    this.#lines.mistakesOnLine = 0;
    await this.save();
  }

  async completeOneLine() {
    this.#didLine();
    this.#lines.linesCompleted++;
    await this.save();
  }

  async madeMistakeOnLine() {
    this.#didLine();
    if (this.isRandomCase() || this.isRandomCaseBuggy()) {
      this.#lines.line = this.#randomizeLineCase(0.6);
    }
    this.#lines.mistakes++;
    this.#lines.mistakesOnLine++;
    this.#lines.linesTodo += this.#lines.penaltyLines;
    await this.save();
  }


  async handleCheating(message) {
    this.#lines.hasCheated = true;

    const shame = await bot.shame.forUser(this.userId);
    await shame.start({
      source: 'cheating_on_lines'
    });

    await this.channelWhereAssigned.send(message);

    await this.save();
  }


  //
  // - Public Static Functions -
  //

  antiCheatFilter(line) {
    return '\u{200B}' + line.split('').map(c => charMap[c] || c).join('') + '\u{200B}';
  }


  //
  // - Private Functions -
  //

  #calculateCPM() {
    // Use hasStarted to check if the user has begun typing
    if (!this.hasStarted()) {
      return 0; // User hasn't started, so CPM is 0
    }

    const messageLength = this.#lines.line.length;
    const currentTime = Date.now();
    const timeSinceLastLine = currentTime - this.#lines.lastTime;
    const minutesPassed = timeSinceLastLine / 60000;
    const calculatedCPM = messageLength / minutesPassed;
    return calculatedCPM;
  }

  /**
   * Updates the fields common to both completing a line correctly and making a mistake on a line
   */
  #didLine() {
    if (!this.#lines.started) {
      this.#lines.started = true;
    }
    this.#lines.currentCpm += this.#calculateCPM();
    this.#lines.lineAttempts++;
    this.#lines.lastTime = Date.now();
  }

  #getRandomWordLine(minWords = 10, maxWords = 13, commaChance = 0.5) {
    // List of forbidden words and the replacement word
    const forbiddenWords = ['slave'];
    const replacementWord = 'helper'; // You can change this to any other appropriate word

    // Generate random words
    const words = randomWords({ min: minWords, max: maxWords });

    // Filter out forbidden words
    const filteredWords = this.#filterWords(words, forbiddenWords, replacementWord);

    // Join words to form a sentence
    let randomSentence = filteredWords.join(' ');

    // Add commas randomly
    const randNum = Math.random();
    if (randNum < commaChance) {
      const randomIndex = Math.floor(Math.random() * (filteredWords.length - 1));
      randomSentence = randomSentence.replace(filteredWords[randomIndex], filteredWords[randomIndex] + ',');
    }

    // Capitalize the first letter
    randomSentence = randomSentence.charAt(0).toUpperCase() + randomSentence.slice(1);

    // Decide if the sentence should be a question or a statement
    const shouldBeQuestion = Math.random();
    if (shouldBeQuestion < 0.3) {
      randomSentence += '?';
    } else {
      randomSentence += '.';
    }

    return randomSentence;
  }

  // Function to replace forbidden words with a different word
  #filterWords(wordList, forbiddenWords, replacementWord) {
    return wordList.map(word => forbiddenWords.includes(word) ? replacementWord : word);
  }

  #randomizeLineCase(probability = 0.5) {
    let line = this.#lines.line;
    if (!line) {
      line = "Why is the line gone? Where did it go? Don't worry though, I've set this default.";
    }
    let result = '';
    for (let i = 0; i < line.length; i++) {
      if (Math.random() < probability) {
        result += line[i].toUpperCase();
      } else {
        result += line[i].toLowerCase();
      }
    }
    return result
  }

  /**
   * Normalizes the quotes in the given string.
   * @param {string} str - The string to normalize.
   * @return {string} - The normalized string.
   */
  #normalizeQuotes(str) {
    // Normalize single quotes
    str = str.replace(/[’´`‘]/g, "'");

    // Normalize double quotes
    str = str.replace(/[“”]/g, "\"");

    return str;
  }


  //
  // - Booleans -
  //

  /**
   * @return boolean
   */
  hasCheated() {
    return !!this.#lines?.hasCheated;
  }
  /**
   * @return boolean
   */
  hideNumberCompleted() {
    return !!this.#lines?.hideNumberCompleted;
  }

  /**
   * @return boolean
   */
  hasLines() {
    return !!this.#lines?.linesTodo;
  }

  /**
   * @return boolean
   */
  hasLinesToStillDo() {
    return this.#lines.linesCompleted < this.#lines.linesTodo;
  }

  /**
   * @return boolean
   */
  hasStarted() {
    return !!this.#lines.startTime;
  }

  /**
   * @return boolean
   */
  isCopyPasting(givenLine) {
    const cheatRegex = new RegExp(Object.values(charMap).join('|'), 'gu');
    const hiddenCharRegex = /\u{200B}/gu;
    return cheatRegex.test(givenLine) || hiddenCharRegex.test(givenLine);
  }

  /**
   * @return boolean
   */
  isNotRandomCase() {
    return this.#lines?.randomCase === 'normal';
  }

  /**
   * @return boolean
   */
  isRandomCase() {
    return this.#lines?.randomCase === 'random';
  }

  /**
   * @return boolean
   */
  isRandomCaseBuggy() {
    return this.#lines?.randomCase === 'buggy';
  }

  /**
   * @return boolean
   */
  isRandomWords() {
    return this.#lines?.randomWords;
  }

  /**
   * Checks if the given content matches the current line,
   * normalizing single quotes and right single quotation marks.
   * @param {string} givenContent - The content to check.
   * @return {boolean} - True if the content matches the line, false otherwise.
   */
  isTypedCorrectly(givenContent) {
    const normalizedGivenContent = this.#normalizeQuotes(givenContent);
    const normalizedExpectedLine = this.#normalizeQuotes(this.#lines.line);
    return normalizedGivenContent === normalizedExpectedLine;
  }

  /**
   * @return boolean
   */
  isTypingUnusuallyFast(givenLine) {
    if (givenLine.length < 5) {
      // just don't bother on really short lines
      return false;
    }
    return this.#calculateCPM(givenLine) > 1000;
  }


  //
  // - Getters -
  //

  /**
   * Gets the channel ID of the line assignment.
   *
   * @returns {Snowflake} The ID of the Discord channel associated with the line assignment.
   */
  get channelId() {
    return this.#lines.channelId;
  }

  /**
   * Gets the channel ID of where lines were assigned.
   *
   * @returns {Snowflake} The ID of the Discord channel where lines were assigned
   */
  get channelIdWhereAssigned() {
    return this.#lines.channelIdWhereAssigned;
  }

  get displayLine() {
    return this.antiCheatFilter(this.#lines.line);
  }

  get finalWPM() {
    // Check for line attempts to calculate WPM, ensuring session has started
    if (!this.hasStarted() || this.#lines.lineAttempts <= 1) {
      return 0; // No valid WPM calculation possible
    }

    const averageCpm = this.#lines.currentCpm / (this.#lines.lineAttempts - 1);
    const wpm = averageCpm / 5; // Assuming an average of 5 characters per word
    return Math.round((wpm + Number.EPSILON) * 100) / 100;
  }

  get lastTime() {
    return this.#lines.lastTime;
  }

  get line() {
    return this.#lines.line;
  }

  set line(line) {
    this.#lines.line = line;
  }

  get linesAssignedMessage() {
    let line = '';
    if (this.#lines.randomLineType && randomTypeWord[this.#lines.randomLineType]) {
      line = randomTypeWord[this.#lines.randomLineType];
    } else if (this.isRandomWords()) {
      line = 'Random Words!';
    } else {
      line = this.displayLine;
    }

    return `\nLines to complete: ${this.qtyToDo}\n` +
      `Line to write: ${line}\n` +
      '\nA new channel has been created just for you to type your lines in.';
  }

  get linesCompleted() {
    return this.#lines.linesCompleted;
  }

  get linesToDo() {
    return this.#lines.linesTodo;
  }

  get mistakesOnLine() {
    return this.#lines.mistakesOnLine;
  }

  get qtyToDo() {
    return this.#lines.linesTodo;
  }

  get setByUserId() {
    return this.#lines.setByUserId;
  }

  get startTime() {
    return this.#lines.startTime;
  }

  get totalMistakes() {
    return this.#lines.mistakes;
  }

  get username() {
    return this.#lines.username;
  }
}
