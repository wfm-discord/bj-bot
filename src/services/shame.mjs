// src/services/shame.mjs
import {bot} from './bot.mjs';
import {brain, ensureBrainKeyExists} from 'bot-commons-utils/src/utils/brain.mjs';
import {DiscordAPIError, RESTJSONErrorCodes} from "discord.js";

const {SHAME_ROLE_ID} = process.env;

const DEFAULT_EXPIRES_MS = 24 * 60 * 60 * 1000;
const DEFAULT_COUNTING_TARGET = 100;

export class ShameRepository {
  /** Get the shame manager for a user.
   *
   * @param {Snowflake} userId the target user's Discord user ID
   * @returns {Promise<Shame>}
   */
  async forUser(userId) {
    return new Shame(userId);
  }

  async *allUsers() {
    const shame = ensureBrainKeyExists('shame');
    for (let userId of Object.keys(shame)) {
      yield new Shame(userId);
    }
  }

  /**
   *
   * @returns {AsyncGenerator<Shame, void, *>}
   */
  async *expiredUsers() {
    const now = new Date().getTime();
    const shame = ensureBrainKeyExists('shame');
    for (const userId of Object.keys(shame)) {
      if (shame[userId].expiresAt <= now) {
        yield new Shame(userId);
      }
    }
  }
}

export class Shame {
  #userId;

  /**
   * Use {@link ShameRepository#forUser} instead.
   * @param {Snowflake} userId
   * @internal
   */
  constructor(userId) {
    this.#userId = userId;
  }


  get #shame() {
    const shame = ensureBrainKeyExists('shame');
    return shame[this.#userId]
  }

  set #shame(data) {
    const shame = ensureBrainKeyExists('shame');
    shame[this.#userId] = data;
  }


  /** Gives this user the shame role and records parameters for punishment.
   *
   * @param {string} source why the user is being shamed.
   *        Must be one of the {@code SOURCE_} constants.
   * @param {string?} auditReason an explanation for the Discord audit log
   * @param {Snowflake?} givenByUserId the user who ordered this shaming
   * @param {Snowflake?} givenInChannelId the channel where the person identified
   *        by givenByUserId issued the command to order this shaming
   * @param {int?} failedAtNumber what number they failed the counting at
   * @param {int?} expiresInMs milliseconds until the role should fall off
   * @param {int?} countingTarget the number they have to count to.
   *        If the user failed counting and hasn't paid the penalty for that
   *        yet, it will be applied as a minimum to this value.
   * @param {int?} oneTimeSlowdown a slowdown (in seconds) to apply to the
   *        channel while the user is counting out this shaming.
   *        Overrides the user's default slowdown for this shaming only.
   * @param {int?} mistakePenalty how much to add to the counting target
   *        as a penalty when the user makes a mistake while counting out.
   *        Defaults to no penalty.
   * @returns {Promise<void>}
   */
  async start({
    source,
    auditReason,
    givenByUserId,
    givenInChannelId,
    failedAtNumber,
    expiresInMs = DEFAULT_EXPIRES_MS,
    countingTarget = DEFAULT_COUNTING_TARGET,
    oneTimeSlowdown,
    mistakePenalty,
  }) {
    const lastFail = ensureBrainKeyExists('count_fail_tracking_number');

    if (failedAtNumber) {
      lastFail[this.#userId] = Math.max(
        failedAtNumber,
        lastFail[this.#userId] ?? 0,
      );
    }

    // Some code sets it as null, so make sure it's set
    if (!countingTarget) {
      countingTarget = DEFAULT_COUNTING_TARGET;
    }

    // If there's already an existing shame counting target: add to it
    // If there isn't, but there is a counting failure,
    //   calculate what the count should actually be and take the higher number
    let usingFailedNumber = false;
    if (this.#shame?.countingTarget) {
      countingTarget += this.#shame.countingTarget;
    } else if (lastFail[this.#userId]) {
      const punishment = calculatePunishment(lastFail[this.#userId]);
      if (punishment > countingTarget) {
        countingTarget = punishment;
        usingFailedNumber = true;
      }
    }

    // If there are existing mistake and slowdowns, keep them or add to them
    if (this.#shame?.mistakePenalty) {
      mistakePenalty += this.#shame?.mistakePenalty;
    }
    if (this.#shame?.oneTimeSlowdown) {
      oneTimeSlowdown += this.#shame?.oneTimeSlowdown;
    }

    // If a source is given but the existing source can't be transferred, keep the non-transferable source
    // Or if no source was given and one exists
    if ((source && !this.canTransfer()) || (!source && this.#shame.source)) {
      source = this.#shame.source
    }

    this.#shame = {
      source,
      givenByUserId,
      givenInChannelId,
      expiresAt: new Date().getTime() + expiresInMs,
      count:  this.#shame?.count || 0,
      counting: this.#shame?.counting || false,
      counted: this.#shame?.counted || false, // so I can tell if someone started counting and stopped
      violations: this.#shame?.violations || 0,
      usingFailedNumber: usingFailedNumber,
      countingTarget: countingTarget,
      oneTimeSlowdown: oneTimeSlowdown,
      mistakePenalty: mistakePenalty,
      transferred: this.#shame?.transferred || false,
      transferBackfire: this.#shame?.transferBackfire || false,
    };

    await brain.write();

    await bot.guild.members.addRole({
      user: this.#userId,
      role: SHAME_ROLE_ID,
      reason: auditReason,
    });
  }

  async finishedCounting() {
    // Only remove the fail tracking if this shame actually used the failed number to set itself
    if (this.#shame.usingFailedNumber && brain.data.count_fail_tracking_number) {
      delete brain.data.count_fail_tracking_number[this.#userId];
    }

    return this.clear("Counted out successfully.");
  }

  /** Remove the shame role and clear this user's shame information.
   *
   * This does _not_ clear the number at which they last failed counting, and
   * therefore their minimum target count. If you want to do that, use
   * {@link finishedCounting()} instead.
   *
   * @param {?string} auditReason the reason for the Discord audit log
   * @returns {Promise<void>}
   */
  async clear(auditReason) {
    delete brain.data.shame[this.#userId];
    await brain.write();

    let logMessage = (
      `Shame cleared for <@${this.#userId}>`
      + (auditReason ? ` with reason "${auditReason}"` : '')
    );

    try {
      await bot.guild.members.removeRole({
        user: this.#userId,
        role: SHAME_ROLE_ID,
        reason: auditReason,
      });
    } catch (caught) {
      if (
        caught instanceof DiscordAPIError
        && caught.code === RESTJSONErrorCodes.UnknownMember
      ) {
        logMessage += ", but they're not currently in the server."
      } else {
        throw caught;
      }
    }

    await bot.logsChannel.send(logMessage);
  }

  async transferShame(targetUserId, auditReason = 'Shame transferred') {
    const targetShame = new Shame(targetUserId);

    // Copying the shame record to the target user
    const shameData = this.#shame;
    if (!shameData) {
      // Something has to have really gone wrong
      throw new Error('No shame data to transfer.');
    }
    targetShame.#shame = {...shameData};
    targetShame.#shame.transferred = true;
    targetShame.#shame.counted = false;
    targetShame.#shame.violations = 0; // reset violations for new person
    targetShame.stopCounting(); // reset counting status

    // Clearing the shame from the source user
    await this.clear('Shame transferred to another user.');

    // Applying the shame to the target user
    await brain.write();

    try {
      await bot.guild.members.addRole({
        user: targetUserId,
        role: SHAME_ROLE_ID,
        reason: auditReason,
      });

      await bot.guild.members.removeRole({
        user: this.#userId,
        role: SHAME_ROLE_ID,
        reason: auditReason,
      });
    } catch (error) {
      console.error('Error transferring shame role:', error);
      throw error;
    }

    await bot.logsChannel.send(`Shame transferred from <@${this.#userId}> to <@${targetUserId}>.`);
  }

  async backfireShameTransfer(targetUserId) {
    if (this.isShamed()) {
      this.#shame.transferBackfire = true;
      this.#shame.countingTarget = this.#shame.countingTarget * 2;
    } else {
      await this.start({
        source: 'transfer_backfire',
        countingTarget: DEFAULT_COUNTING_TARGET * 2,
      });
      this.#shame.transferBackfire = true;
    }

    await brain.write();
    await bot.logsChannel.send(`Shame transfer from <@${this.#userId}> to <@${targetUserId}> backfired.`);
  }

  canTransfer() {
    return !this.#shame?.source ||
      (
        this.#shame?.source &&
        this.#shame?.source !== 'cheating_on_lines' &&
        this.#shame?.source !== 'mass_ping'
      )
  }

  /**
   * @return boolean
   */
  isShamed() {
    return !!this.#shame;
  }

  /**
   * @return boolean
   */
  isCounting() {
    return !!this.#shame?.counting;
  }

  /**
   * @return boolean
   */
  hasCounted() {
    return !!this.#shame?.counted;
  }

  /**
   * @return boolean
   */
  isWaiting() {
    return !!this.#shame?.waiting;
  }

  /**
   * Check if the shame status has expired.
   *
   * @returns {boolean} True if the shame status has expired, false otherwise.
   */
  isExpired() {
    const now = new Date().getTime();
    const expiresIn = this.#shame?.expiresAt;
    return expiresIn <= now;
  }

  /**
   * Checks if there is a valid last fail number for this user.
   * @returns {boolean} True if there is a valid last fail number, false otherwise.
   */
  hasValidLastFailNumber() {
    const lastFail = ensureBrainKeyExists('count_fail_tracking_number');
    const failNumber = lastFail[this.#userId];
    return failNumber > 0;
  }

  /**
   * Check if the shame expiration is within 2 hours from now.
   *
   * @returns {boolean} True if the expiration is within 2 hours, false otherwise.
   */
  isLessThanTwoHours() {
    const now = new Date().getTime();
    const expiresIn = this.#shame?.expiresAt;
    if (!expiresIn) {
      return false;
    }

    // 2 hours in milliseconds
    const twoHours = 2 * 60 * 60 * 1000;

    return (expiresIn - now) <= twoHours;
  }

  startCounting() {
    const shameData = this.#shame;
    if (!shameData) {
      // Something has to have really gone wrong
      throw new Error('No shame data!');
    }
    this.resetCount();
    this.#shame.counting = true;
    this.#shame.counted = true;
  }

  stopCounting() {
    const shameData = this.#shame;
    if (!shameData) {
      // Something has to have really gone wrong
      throw new Error('No shame data!');
    }
    this.resetCount();
    this.#shame.counting = false;
  }

  /**
   * Get the user ID associated with this Shame instance.
   *
   * @returns {Snowflake} The user's Discord ID.
   */
  getUserId() {
    return this.#userId;
  }

  incrementCount() {
    this.#shame.count++;
  }

  get count() {
    return this.#shame?.count;
  }

  resetCount() {
    this.#shame.count = 0;
  }

  incrementViolations() {
    this.#shame.violations++;
  }

  get violations() {
    return this.#shame?.violations || 0;
  }

  set violations(value) {
    this.#shame.violations = value;
  }

  /**
   * @return int the number this user needs to count up to in order to
   *       clear their shame
   */
  get countingTarget() {
    return this.#shame?.countingTarget;
  }

  set countingTarget(target) {
    this.#shame.countingTarget = target;
  }

  get failedCountNumber() {
    const lastFail = ensureBrainKeyExists('count_fail_tracking_number');
    return lastFail[this.#userId] ?? 0;
  }

  set failedCountNumber(value) {
    const lastFail = ensureBrainKeyExists('count_fail_tracking_number');
    lastFail[this.#userId] = value;
    brain.write();
  }


  /**
   * Gets the milliseconds until the shame expires.
   *
   * @returns {number} Milliseconds until expiration.
   */
  get expiresInMs() {
    return this.#shame?.expiresAt - new Date().getTime();
  }

  /**
   * Sets the expiration time in milliseconds.
   *
   * @param {number} ms Milliseconds until the shame should expire.
   */
  set expiresInMs(ms) {
    this.#shame.expiresAt = new Date().getTime() + ms;
  }

  get source() {
    return this.#shame?.source;
  }

  set source(newSource) {
    this.#shame.source = newSource;
  }

  get transferBackfire() {
    return this.#shame?.transferBackfire;
  }

  set transferBackfire(boolean) {
    this.#shame.transferBackfire = boolean;
  }

  /**
   * Gets the one-time slowdown duration.
   *
   * @returns {number} One-time slowdown in seconds.
   */
  get oneTimeSlowdown() {
    return this.#shame?.oneTimeSlowdown;
  }

  /**
   * Sets the one-time slowdown duration.
   *
   * @param {number} seconds Slowdown duration in seconds.
   */
  set oneTimeSlowdown(seconds) {
    this.#shame.oneTimeSlowdown = seconds;
  }

  /**
   * Gets the mistake penalty.
   *
   * @returns {number} Mistake penalty number.
   */
  get mistakePenalty() {
    return this.#shame?.mistakePenalty;
  }

  /**
   * Sets the mistake penalty.
   *
   * @param {number} penalty The penalty to be set for a mistake.
   */
  set mistakePenalty(penalty) {
    this.#shame.mistakePenalty = penalty;
  }

  async save() {
    return brain.write();
  }
}

export function calculatePunishment(failed_number) {
  if (failed_number <= 100) {
    return 100;
  } else if (failed_number <= 550) {
    return 100 + Math.ceil((failed_number-100) / 3);
  } else {
    return 250;
  }
}
