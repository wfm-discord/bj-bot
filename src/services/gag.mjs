// src/services/gag.mjs
import { bot } from './bot.mjs';
import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {DiscordAPIError, PermissionsBitField, RESTJSONErrorCodes} from 'discord.js';
import cron from 'node-cron';

const TIMEOUT_ROLE_ID = process.env.TIMEOUT_ROLE_ID;

export class GagRepository {
  /** Get the gag manager for a user.
   *
   * @param {Snowflake} userId the target user's Discord user ID
   * @returns {Promise<Gag>}
   */
  async forUser(userId) {
    return new Gag(userId);
  }

  async *allUsers() {
    const gag = ensureBrainKeyExists('gag');
    for (let userId of Object.keys(gag)) {
      yield new Gag(userId);
    }
  }
}

export class Gag {
  #userId;

  /**
   * Use {@link GagRepository#forUser} instead.
   * @param {Snowflake} userId
   * @internal
   */
  constructor(userId) {
    this.#userId = userId;
  }

  get #gag() {
    ensureBrainKeyExists('gag');
    return brain.data.gag[this.#userId];
  }

  set #gag(data) {
    const gag = ensureBrainKeyExists('gag');
    gag[this.#userId] = data;
  }


  /** Starts or overrides gag for this user.
   *
   * @param {Object} params The parameters for gag, including reason and minutes to do.
   * @returns {Promise<boolean>}
   */
  async newGag({gaggedByUser, reason, duration}) {

    // Convert duration to milliseconds
    duration = duration * 60 * 1000;

    this.#gag = {
      duration: duration,
      gagged_by: gaggedByUser.id,
      reason: reason,
      endTimestamp: null
    };

    return await this.startGag();
  }

  /**
   * Starts preexisting gag for this user.
   * @returns {Promise<boolean>}
   */
  async startGag() {
    // Time out the user
    const member = await bot.guild.members.fetch(this.#userId);
    if (member.permissions.has(PermissionsBitField.Flags.Administrator)) {
      await this.removeBrain();
      return false;
    }
    await member.timeout(this.#gag.duration, this.#gag.reason);

    // Get the timeout role
    try {
      await bot.guild.members.addRole({
        user: this.#userId,
        role: TIMEOUT_ROLE_ID,
        reason: "Gagged",
      });
    } catch (error) {
      console.error('Error adding gag role:', error);
      throw error;
    }

    this.#gag.endTimestamp = member.communicationDisabledUntilTimestamp;
    await brain.write();

    // Schedule to ungag the member when the timeout ends
    const endDate = new Date(this.#gag.endTimestamp);
    await this.gagMemberCron();
    return true;
  }

  /**
   * Ends gag role for this user.
   * NOTE: The timeout is not removed! This does not ungag an actively gagged user; the timeout expires on its own
   * @returns {Promise<void>}
   */
  async end() {
    await this.removeBrain();

    try {
      await bot.guild.members.removeRole({
        user: this.#userId,
        role: TIMEOUT_ROLE_ID,
        reason: "Gag over",
      });
    } catch (caught) {
      if (caught instanceof DiscordAPIError && caught.code === RESTJSONErrorCodes.UnknownMember) {
        console.error("User not in server, couldn't remove gaged role.", caught);
      } else {
        throw caught;
      }
    }
  }

  /**
   * Schedules the cron job to remove the user role and brain object
   * @returns {Promise<void>}
   */
  async gagMemberCron() {
    // Make sure the member still exists in the guild
    const member = await bot.guild.members.fetch(this.#userId);
    if (!member) {
      // remove brain object
      await this.removeBrain();
    }

    // Make sure the end timestamp is in the future
    const endDate = new Date(this.#gag.endTimestamp);
    if (endDate <= Date.now()) {
      // remove brain object and role
      await this.end();
    }

    // Set cron schedule to run once at the exact date and time
    const job = cron.schedule(`${endDate.getSeconds()} ${endDate.getMinutes()} ${endDate.getHours()} ${endDate.getDate()} ${endDate.getMonth() + 1} ${endDate.getDay()}`, async () => {
      try {
        await this.end();
      } catch (error) {
        console.error('Failed to ungag user: ', error);
      }
      // Stop the cron job after it has run once
      job.stop();
    });
  }

  /**
   * Removes brain object
   * @returns {Promise<void>}
   */
  async removeBrain() {
    delete brain.data.gag[this.#userId];
    await brain.write();
  }

  //
  // - Statics -
  //

  /**
   * Loops through all user gag objects and schedules cron jobs
   * @returns {Promise<void>}
   */
  static async scheduleAllGagCron() {
    const gags = await bot.gag.allUsers();

    // Loop through the 'gag' objects
    for (const gag in gags) {
      await gag.gagMemberCron();
    }
  }


  //
  // - Getters -
  //

  /**
   * @return number
   */
  get duration() {
    return this.#gag?.duration || 0;
  }

  /**
   * @return string
   */
  get reason() {
    return this.#gag.reason;
  }

  /** Retrieves the user ID associated with this Gag instance.
   *
   * @returns {Snowflake} The user's Discord ID.
   */
  get userId() {
    return this.#userId;
  }
}
