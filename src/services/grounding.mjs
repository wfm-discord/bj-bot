// src/services/grounding.mjs
import { bot } from './bot.mjs';
import { brain, ensureBrainKeyExists } from 'bot-commons-utils/src/utils/brain.mjs';
import {DiscordAPIError, EmbedBuilder, RESTJSONErrorCodes} from 'discord.js';

const { GROUNDED_ROLE_ID } = process.env;

export class GroundingRepository {
  /** Get the grounding manager for a user.
   *
   * @param {Snowflake} userId the target user's Discord user ID
   * @returns {Promise<Ground>}
   */
  async forUser(userId) {
    return new Ground(userId);
  }

  async *allUsers() {
    const grounding = ensureBrainKeyExists('grounding');
    for (let userId of Object.keys(grounding)) {
      yield new Ground(userId);
    }
  }
}

export class Ground {
  #userId;

  /**
   * Use {@link GroundingRepository#forUser} instead.
   * @param {Snowflake} userId
   * @internal
   */
  constructor(userId) {
    this.#userId = userId;
  }

  get #grounding() {
    const grounding = ensureBrainKeyExists('grounding');
    return grounding[this.#userId];
  }

  set #grounding(data) {
    const grounding = ensureBrainKeyExists('grounding');
    grounding[this.#userId] = data;
  }

  /** Starts or updates grounding for this user.
   *
   * @param {Object} params The parameters for grounding, including reason and minutes to do.
   * @returns {Promise<void>}
   */
  async start({
    minutesToDo = 0,
    minutesMinimum = 0,
    percentage = 0,
    mistakePenalty = 0,
    instructions = '',
    personalizedInstructions = false,
    secret = false,
  }) {
    // ensure positive numbers
    minutesToDo = Math.abs(minutesToDo);
    minutesMinimum = Math.abs(minutesMinimum);
    mistakePenalty = Math.abs(mistakePenalty);
    percentage = Math.abs(percentage);
    if (percentage > 100) {
      percentage = 100;
    }

    if (!this.isGrounded()) {
      // if not grounded just make the starting information
      this.#grounding = {
        minutesToDo: minutesToDo || 1,
        minutesMinimum,
        percentage,
        mistakePenalty,
        instructions,
        personalizedInstructions,
        secret,
        minutesDone: 0,
        previous: null,
      };
    } else {
      const currentGrounding = { ...this.#grounding };
      // something something circular error
      delete currentGrounding.previous;

      this.#grounding.previous = currentGrounding;
      this.#grounding.minutesToDo += minutesToDo;

      if (minutesMinimum > 0) {
        this.#grounding.minutesMinimum = Math.max(
          this.#grounding.minutesMinimum || 0,
          minutesMinimum
        );
      }
      if (percentage > 0) {
        this.#grounding.percentage = Math.max(
          this.#grounding.percentage || 0,
          percentage
        );
      }
      if (mistakePenalty > 0) {
        this.#grounding.mistakePenalty = Math.max(
          this.#grounding.mistakePenalty || 0,
          mistakePenalty
        );
      }

      if (secret && !this.#grounding.secret) {
        this.#grounding.secret = secret;
      }
    }

    await brain.write();

    try {
      await bot.guild.members.addRole({
        user: this.#userId,
        role: GROUNDED_ROLE_ID,
        reason: "Grounded",
      });
    } catch (error) {
      console.error('Error adding grounded role:', error);
      throw error;
    }
  }

  async overwrite({
    minutesToDo,
    minutesMinimum,
    percentage,
    mistakePenalty
  }) {
    if (percentage > 100) {
      percentage = 100;
    }
    const currentGrounding = { ...this.#grounding };
    // something something circular error
    delete currentGrounding.previous;

    this.#grounding.previous = currentGrounding;
    this.#grounding.mistakePenalty = mistakePenalty;

    if (typeof minutesToDo !== 'undefined') {
      this.#grounding.minutesToDo = minutesToDo;
    }
    // Check and update minutes_minimum only if provided
    if (typeof minutesMinimum !== 'undefined') {
      this.#grounding.minutesMinimum = minutesMinimum;
    }
    // Check and update minimum_percentage only if provided
    if (typeof percentage !== 'undefined') {
      this.#grounding.percentage = percentage;
    }
    // Check and update minimum_percentage only if provided
    if (typeof mistakePenalty !== 'undefined') {
      this.#grounding.mistakePenalty = mistakePenalty;
    }
    await brain.write();
  }

  /**
   * Ends grounding for this user.
   * @returns {Promise<void>}
   */
  async end() {
    delete brain.data.grounding[this.#userId];
    await brain.write();

    try {
      await bot.guild.members.removeRole({
        user: this.#userId,
        role: GROUNDED_ROLE_ID,
        reason: "Grounding over",
      });
    } catch (caught) {
      if (caught instanceof DiscordAPIError && caught.code === RESTJSONErrorCodes.UnknownMember) {
        console.error("User not in server, couldn't remove grounded role.", caught);
      } else {
        throw caught;
      }
    }
  }

  async handleMistakes(mistakes) {
    this.#grounding.mistakes += +mistakes;
    if (this.hasMistakePenalty()) {
      const penalty = mistakes * this.#grounding.mistakePenalty;
      this.#grounding.minutesToDo += penalty;
    }
    await brain.write();
  }

  async addMinutesCompleted(minutes) {
    this.#grounding.minutesDone += +minutes;
    await brain.write();
  }

  /** Updates the progress of the grounding.
   *
   * @param {number} minutesDone The minutes done towards grounding.
   */
  async updateProgress(minutesDone) {
    if (this.#grounding) {
      this.#grounding.minutesDone += minutesDone;
      await brain.write();
    }
  }

  //
  // - Booleans -
  //

  /**
   * @return boolean
   */
  hasMistakePenalty() {
    return this.#grounding.mistakePenalty > 0;
  }

  /** Checks if the user is currently grounded.
   *
   * @returns {boolean} True if the user is grounded, false otherwise.
   */
  isGrounded() {
    return !!this.#grounding;
  }

  isGroundingComplete() {
    return this.#grounding.minutesDone >= this.#grounding.minutesToDo;
  }

  /**
   * @return boolean
   */
  isMinutesMinimumOkay(minutes) {
    // test okay if minutes are less than 12
    return minutes <= 12;
  }

  /**
   * @return boolean
   */
  isSecret() {
    return !!this.#grounding?.secret;
  }

  /**
   * @return boolean
   */
  metMinimumPercentage(percentage) {
    return this.#grounding.percentage <= +percentage;
  }

  /**
   * @return boolean
   */
  metMinimumMinutes(minutes) {
    return this.#grounding.minutesMinimum <= +minutes;
  }


  //
  // - Getters -
  //

  getGroundingEmbed(overrideSecret = null) {
    const previousGrounding = this.#grounding.previous;
    const currentGrounding = this.#grounding;

    const embed = new EmbedBuilder()
      .setTitle('You\'re Grounded!')
      .setDescription(`<@${this.#userId}> has been grounded!`)
      .setColor('#0099ff')
      .setFooter({text: 'Type `/ground_me` or `/surprise_me` to serve out your grounding time using Grounding Bot.'});

    let currentSecret = currentGrounding.secret || (previousGrounding && previousGrounding.secret);
    if (overrideSecret !== null) {
      currentSecret = overrideSecret;
    }

    if (currentSecret) {
      embed.addFields({
        name: ':shushing_face: SECRET GROUNDING! :shushing_face:',
        value: 'The details of this grounding are a secret. Head to your grounding area and good luck!',
      });

      if (currentGrounding.instructions) {
        const name = currentGrounding.personalizedInstructions ? 'Personalized Instructions' : 'Instructions';
        embed.addFields({ name: name, value: currentGrounding.instructions});
      }
      return embed;
    }

    if (previousGrounding && previousGrounding.minutesToDo !== currentGrounding.minutesToDo) {
      embed.addFields({
        name: 'Minutes to Complete',
        value: `Was: ${previousGrounding.minutesToDo ? previousGrounding.minutesToDo : 0} minutes\nNow: ${currentGrounding.minutesToDo} minutes`,
      });
    } else {
      embed.addFields({
        name: 'Minutes to Complete',
        value: `${currentGrounding.minutesToDo} minutes`,
      });
    }

    if (previousGrounding && previousGrounding.minutesMinimum !== currentGrounding.minutesMinimum) {
      embed.addFields({
        name: 'Minimum Grounding Session',
        value: `Was: ${previousGrounding.minutesMinimum || '1 '} minutes\nNow: ${
          currentGrounding.minutesMinimum || '1 '
        } minutes`,
      });
    } else {
      embed.addFields({
        name: 'Minimum Grounding Session',
        value: `${currentGrounding.minutesMinimum || '1 '} minutes`,
      });
    }

    if (previousGrounding && previousGrounding.percentage !== currentGrounding.percentage) {
      embed.addFields({
        name: 'Percentage Minimum',
        value: `Was: ${
          previousGrounding.percentage || '0'
        }%\nNow: ${currentGrounding.percentage || '0'}%`,
      });
    } else {
      embed.addFields({
        name: 'Percentage Minimum',
        value: `${currentGrounding.percentage || '0'}%`,
      });
    }

    if (previousGrounding && previousGrounding.mistakePenalty !== currentGrounding.mistakePenalty) {
      embed.addFields({
        name: 'Mistake Penalty',
        value: `Was: ${previousGrounding.mistakePenalty || '0'
        }\nNow: ${currentGrounding.mistakePenalty || '0'}`,
      });
    } else {
      embed.addFields({
        name: 'Mistake Penalty',
        value: `${currentGrounding.mistakePenalty || '0'}`,
      });
    }

    if (currentGrounding.instructions) {
      const name = currentGrounding.personalizedInstructions ? 'Personalized Instructions' : 'Instructions';
      embed.addFields({ name: name, value: currentGrounding.instructions});
    }

    return embed;
  }

  get minimumMinutes() {
    return this.#grounding.minutesMinimum;
  }

  get minimumMinutesMessage() {
    return 'Sorry, the minimum amount of minutes to '
      + 'do at a time has to be 12 or less. '
      + 'The bot can\'t do more than 12 minutes at a time.';
  }

  get minimumPercentage() {
    return this.#grounding.percentage;
  }

  get minutesDone() {
    return this.#grounding.minutesDone;
  }

  get minutesToDo() {
    return this.#grounding.minutesToDo;
  }

  get mistakePenalty() {
    return this.#grounding.mistakePenalty;
  }

  get statusMessage() {
    const user_id = this.#userId;
    if (!this.isGrounded()) {
      return `<@${user_id}> is not currently grounded.`;
    }

    if (this.isSecret()) {
      return `<@${user_id}> is grounded, but the settings are a secret! :shushing_face:`;
    }

    const left = this.#grounding.minutesToDo - this.#grounding.minutesDone;
    const mistakePenalty = this.#grounding.mistakePenalty;
    return `<@${user_id}> is grounded!\n` +
      `* for ${this.#grounding.minutesToDo} minutes\n` +
      (
        this.#grounding.minutesMinimum ?
        `* with a minimum grounding session of ${this.#grounding.minutesMinimum} minutes, \n` :
        '* with no minimum grounding session, \n'
      ) +
      (
        this.#grounding.percentage ?
        `* and a minimum percentage of ${this.#grounding.percentage}%.\n` :
        '* and no minimum percentage.\n'
      ) + (
        this.#grounding.minutesDone ?
        `* ${this.#grounding.minutesDone} minutes have been completed.\n` :
        `* Nothing has been completed yet so there's still ${this.#grounding.minutesToDo} minutes to do.\n`
      ) + (
        left === this.#grounding.minutesToDo ? '' : `* ${left} minutes left.\n`
      ) + (
        mistakePenalty === 0 ? '' : `* ${mistakePenalty} minute penalty will be added for each mistake.`
      )
  }

  /** Retrieves the user ID associated with this Ground instance.
   *
   * @returns {Snowflake} The user's Discord ID.
   */
  get userId() {
    return this.#userId;
  }
}
