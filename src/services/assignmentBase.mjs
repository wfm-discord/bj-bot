// src/services/assignmentBase.mjs
import {brain} from 'bot-commons-utils/src/utils/brain.mjs';
import {bot} from './bot.mjs';
import {ChannelType, PermissionFlagsBits, TextChannel} from 'discord.js';

// used in Lines, and probably Math
export class AssignmentBase {
  #userId;

  constructor(userId) {
    this.#userId = userId;
  }

  get userId() {
    return this.#userId;
  }

  async save() {
    return brain.write();
  }

  async createAssignmentChannel(type, senderId, spectator = 'off') {
    const guild = bot.guild;

    const everyoneRole = guild.roles.everyone.id;

    // Default permissionOverwrites
    let permissionOverwrites = [
      {type: 'role', id: everyoneRole, deny: [
        PermissionFlagsBits.ViewChannel,
        PermissionFlagsBits.SendMessages,
        PermissionFlagsBits.CreatePublicThreads,
        PermissionFlagsBits.CreatePrivateThreads
        ]},
      {type: 'member', id: this.userId, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: senderId, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
      {type: 'member', id: process.env.DISCORD_CLIENT_ID, allow: [PermissionFlagsBits.ViewChannel, PermissionFlagsBits.SendMessages]},
    ];

    if (spectator === 'viewing' || spectator === 'free-talking') {
      const permissions = [PermissionFlagsBits.ViewChannel];
      if (spectator === 'free-talking') {
        permissions.push(PermissionFlagsBits.SendMessages);
      }
      permissionOverwrites.push(
        {type: 'role', id: process.env.REQUIRED_ROLE_1, allow: permissions},
        {type: 'role', id: process.env.REQUIRED_ROLE_2, allow: permissions},
        {type: 'role', id: process.env.REQUIRED_ROLE_3, allow: permissions},
      );
    }

    const member = await guild.members.fetch(this.userId);
    return await guild.channels.create({
      name: member.displayName + '-' + type,
      type: ChannelType.GuildText,
      permissionOverwrites: permissionOverwrites,
      parent: process.env.LINES_CATEGORY_ID,
    });
  }

  async deleteAssignmentChannel()
  {
    if (this.channel) {
      await this.channel.delete();
    }
  }

  /**
   * @return boolean
   */
  isUserTalking(userId) {
    return userId === this.userId;
  }

  /**
   * Gets the channel ID of the assignment.
   *
   * @returns {Snowflake} The ID of the Discord channel associated with the assignment.
   */
  get channelId() {
    throw new Error('This needs to be defined in the extended class.');
  }

  /**
   * Retrieves the Discord channel instance for the assignment from the guild's channel cache.
   *
   * @returns {TextChannel|null} The Discord channel object if found in the cache, or `null` if the channel
   *                         cannot be found (which might happen if the channel was deleted).
   */
  get channel() {
    const guild = bot.guild;
    return guild.channels.cache.get(this.channelId);
  }

  /**
   * Gets the channel ID of the assignment.
   *
   * @returns {Snowflake} The ID of the Discord channel associated with the assignment.
   */
  get channelIdWhereAssigned() {
    throw new Error('This needs to be defined in the extended class.');
  }

  /**
   * Retrieves the Discord channel instance for the assignment from the guild's channel cache.
   *
   * @returns {TextChannel|null} The Discord channel object if found in the cache, or `null` if the channel
   *                         cannot be found (which might happen if the channel was deleted).
   */
  get channelWhereAssigned() {
    const guild = bot.guild;
    return guild.channels.cache.get(this.channelIdWhereAssigned);
  }

  inAssignmentChannel(channelId) {
    return this.channelId === channelId;
  }

  /**
   * @return string
   */
  static calculateLetterGrade(percentage) {
    if (percentage >= 90) {
      return 'A';
    } else if (percentage >= 80) {
      return 'B';
    } else if (percentage >= 70) {
      return 'C';
    } else if (percentage >= 60) {
      return 'D';
    } else {
      return 'F';
    }
  }
}
